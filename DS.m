//
//  DS.m
//  CoinBook
//
//  Created by Vadim Re on 2012-11-03.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import "DS.h"
#import "Utility.h"
#define DATABASE_VERSION 8


@implementation DS

@synthesize  DatabasePath,DirPath, DocsDir;

#pragma mark - Property


NSFileManager *filemgr;
const char *dbpath;


-(void)setDatabasePath:(NSString *)newDatabasePath
{
    _databasePath = newDatabasePath;
}
-(NSString*)DatabasePath
{
    return _databasePath;
}
-(NSArray*)DirPath
{
    return _dirPaths;
}
-(void)setDirPath:(NSArray *)newDatabasePath
{
     _dirPaths = newDatabasePath;
}
-(NSString*)DocsDir
{
    return _docsDir;
}
-(NSString*)getSqlStatus
{
    return [self sqlStatus];
}


#pragma mark - Methods

-(void)dealloc
{
    //[dbpath release];
    [filemgr release];
    [super dealloc];
}
-(id)init
{
    self =[super init];
    
    
    // Get the documents directory
    _dirPaths = NSSearchPathForDirectoriesInDomains(  NSDocumentDirectory, NSUserDomainMask, YES);
    
    _docsDir = [_dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    _databasePath = [[NSString alloc] initWithString: [_docsDir stringByAppendingPathComponent: @"CoinBook.sqlite"]];
    
     filemgr = [NSFileManager defaultManager];
    dbpath = [_databasePath UTF8String];
    
    //[self checkDB];
       
    return self;
}


-(void)checkDB
{
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
        //////////////
        
        
        if ([filemgr fileExistsAtPath: _databasePath ] == NO)
        {
           //[self createTables];
        }
        else
        {
            ///////////////////////////////////////////////////////update table//////////////////////
            if([Utility getPreference:@"databaseversion" ] != nil )
            {
                
                if([Utility getPreferenceInt:@"databaseversion"] == 0)
                {
                    [self createTables];
                    
                    [Utility setPreferenceInt:@"databaseversion" PrefValue:DATABASE_VERSION ];
                }
                else if(!([Utility getPreferenceInt:@"databaseversion"] == DATABASE_VERSION ) )
                {
                    
                    [self updateTables];
                    [Utility setPreferenceInt:@"databaseversion" PrefValue:DATABASE_VERSION ];
                }
            }
            else
            {
                [self createTables];
                [Utility setPreferenceInt:@"databaseversion" PrefValue:DATABASE_VERSION ];
            }
           
        }
        
        
    }
    sqlite3_close(_contactDB);
}

-(void)commandSQL:(NSString*)SQLStatement
{
    NSString *updatedSQL = [[NSString alloc] initWithString:[SQLStatement stringByReplacingOccurrencesOfString:@"(null)" withString:@""]];
    _dirPaths = NSSearchPathForDirectoriesInDomains(  NSDocumentDirectory, NSUserDomainMask, YES);
    //NSLog(@"%@",SQLStatement );

    
   // NSLog(@"%@",updatedSQL );
    _docsDir = [_dirPaths objectAtIndex:0];
       
    // Build the path to the database file
    _databasePath = [[NSString alloc] initWithString: [_docsDir stringByAppendingPathComponent: @"CoinBook.sqlite"]];
    
     filemgr = [NSFileManager defaultManager];
    
    const char *dbpath = [_databasePath UTF8String];
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
        
              
        
        //get the date today
       
      
        const char *insert_stmt = [updatedSQL UTF8String];
        //NSLog(@"%s", insert_stmt);
        sqlite3_prepare_v2(_contactDB, insert_stmt, -1, &_statement,NULL);
        int result =sqlite3_step(_statement);
        //NSLog(@"result=%d", result);
         if ( result== SQLITE_DONE)
         {
             
             [self setSqlStatus: @"1"];
                 
         }
         else
         {
             [self setSqlStatus: @"0"];
         }
         
         sqlite3_finalize(_statement);
        sqlite3_close(_contactDB);
        
    }
   [updatedSQL release];

}

-(void)createTables
{
    _dirPaths = NSSearchPathForDirectoriesInDomains(  NSDocumentDirectory, NSUserDomainMask, YES);
    
    _docsDir = [_dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    _databasePath = [[NSString alloc] initWithString: [_docsDir stringByAppendingPathComponent: @"CoinBook.sqlite"]];
    
    // filemgr = [NSFileManager defaultManager];
    const char *dbpath = [_databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
        char *errMsg;
        const char *sql_stmt;
     
        sqlite3_stmt *statementChk;
        sqlite3_prepare_v2(_contactDB, "SELECT name FROM sqlite_master WHERE type='table' AND name='Collections';", -1, &statementChk, nil);
        
        bool boo = FALSE;
        
        if (sqlite3_step(statementChk) == SQLITE_ROW)
        {
            boo = TRUE;
        }
        sqlite3_finalize(statementChk);
        if (!boo)
        {
            sql_stmt = "CREATE TABLE 'Collections' ('collection_id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,'shelf_id' INTEGER,'name' TEXT,'description' TEXT,'year_start' TEXT,'year_finish' TEXT,'date_created' DATETIME DEFAULT (CURRENT_TIME) ,'coin_name' TEXT,'create_years' TEXT,'use_item_name' TEXT,'use_year_range' TEXT,'items_count' INTEGER DEFAULT (0) ,'status' INTEGER DEFAULT (0) ,'face_value' TEXT,'precious_metal' INTEGER DEFAULT (0) ,'weight' NUMERIC DEFAULT (0) ,'material' TEXT,'weight_unit' INTEGER DEFAULT (0) , 'country_code' TEXT , 'currency_code' TEXT )";

   
            if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                [self setSqlStatus:@"Failed to create table"];
            }
        }
         
        sqlite3_prepare_v2(_contactDB, "SELECT name FROM sqlite_master WHERE type='table' AND name='Items';", -1, &statementChk, nil);
                                        
        boo = FALSE;
        
        if (sqlite3_step(statementChk) == SQLITE_ROW)
        {
            boo = TRUE;
        }
        sqlite3_finalize(statementChk);
        if(!boo)
        {
            sql_stmt = "CREATE  TABLE 'Items' ('item_id' INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL ,'collection_id' INTEGER, 'name' TEXT,'description' TEXT,'price' NUMERIC,'value' TEXT,'year' INTEGER,'material' VARCHAR,'diameter' TEXT,'weight' NUMERIC DEFAULT (0), 'weight_unit' INTEGER DEFAULT 0,'grade' INTEGER DEFAULT 0,'face_value' NUMERIC,'date_created' DATETIME, 'status' INTEGER DEFAULT 0, 'precious_metal' INTEGER DEFAULT 0, 'purchase_date' DATETIME, 'country_code' TEXT DEFAULT US, 'currency_code' TEXT DEFAULT USD)";

            if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                [self setSqlStatus:@"Failed to create table"];
            }
            sql_stmt = "create index index_items_items_id on items(item_id)";
            if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                [self setSqlStatus:@"Failed to create table"];
            }
        }
        
        sql_stmt = "reindex index_items_items_id";
        if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
       
        sqlite3_prepare_v2(_contactDB, "SELECT name FROM sqlite_master WHERE type='table' AND name='collection_item';", -1, &statementChk, nil);
        
        boo = FALSE;
        
        if (sqlite3_step(statementChk) == SQLITE_ROW)
        {
            boo = TRUE;
        }
        sqlite3_finalize(statementChk);
        if(!boo)
        {
            sql_stmt= "CREATE  TABLE collection_item (collection_id INTEGER NOT NULL , item_id INTEGER NOT NULL )";
            if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                [self setSqlStatus:@"Failed to create table"];
            }
        }
        
        sqlite3_prepare_v2(_contactDB, "SELECT name FROM sqlite_master WHERE type='table' AND name='shelfs';", -1, &statementChk, nil);
        
        boo = FALSE;
        
        if (sqlite3_step(statementChk) == SQLITE_ROW)
        {
            boo = TRUE;
        }
        sqlite3_finalize(statementChk);
        if(!boo)
        {
            sql_stmt= "CREATE  TABLE 'shelfs' ('shelf_id' INTEGER PRIMARY KEY AUTOINCREMENT   NOT NULL , 'name' TEXT, 'date_created' DATETIME)";
            if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                [self setSqlStatus:@"Failed to create table"];
            }
        }
        

        
        
        
        
        sqlite3_prepare_v2(_contactDB, "SELECT name FROM sqlite_master WHERE type='table' AND name='country';", -1, &statementChk, nil);
        
        boo = FALSE;
        
        if (sqlite3_step(statementChk) == SQLITE_ROW)
        {
            boo = TRUE;
        }
        sqlite3_finalize(statementChk);
        if(!boo)
        {
            sql_stmt = "CREATE TABLE 'country' ('country_code' TEXT PRIMARY KEY  NOT NULL , 'country' TEXT)";
            if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                [self setSqlStatus:@"Failed to create table"];
            }
            [self populateCountry];

        }
        sqlite3_prepare_v2(_contactDB, "SELECT name FROM sqlite_master WHERE type='table' AND name='currency';", -1, &statementChk, nil);
        
        boo = FALSE;
        
        if (sqlite3_step(statementChk) == SQLITE_ROW)
        {
            boo = TRUE;
        }
        sqlite3_finalize(statementChk);
        if(!boo)
        {
            sql_stmt = "CREATE TABLE 'currency' ('currency_code' TEXT PRIMARY KEY  NOT NULL , 'currency' TEXT)";
            if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                [self setSqlStatus:@"Failed to create table"];
            }
            [self populateCurrency];
            
        }
    sql_stmt = "delete from items  where item_id not in (select item_id from  collection_item )  ";
    if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
    {
        [self setSqlStatus:@"Failed to create table"];
    }
    }

    sqlite3_finalize(_statement);
    sqlite3_close(_contactDB);



   // [filemgr release];
   // [self populateCountry];
    //[self populateCurrency];
}
-(void)updateTables
{
    if([Utility getPreference:@"coindatabaseversion"].length ==0 || [[Utility getPreference:@"coindatabaseversion"] intValue] < DATABASE_VERSION)
    {
        _dirPaths = NSSearchPathForDirectoriesInDomains(  NSDocumentDirectory, NSUserDomainMask, YES);
    
        _docsDir = [_dirPaths objectAtIndex:0];
    
        // Build the path to the database file
        _databasePath = [[NSString alloc] initWithString: [_docsDir stringByAppendingPathComponent: @"CoinBook.sqlite"]];
        if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
        {

        const char *sql_stmt;
        char *errMsg;
        sqlite3_stmt *statementChk;
        sqlite3_prepare_v2(_contactDB, "SELECT name FROM sqlite_master WHERE type='index' AND name='Index_country';", -1, &statementChk, nil);
        
        bool boo = FALSE;
        
        if (sqlite3_step(statementChk) == SQLITE_ROW)
        {
            boo = TRUE;
        }
        sqlite3_finalize(statementChk);
        if (!boo)
        {
            sql_stmt = "CREATE INDEX Index_country on country(country_code)";
            if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                [self setSqlStatus:@"0"];
            }

        }
        /////////////Add Status date changed
            
            
            sqlite3_prepare_v2(_contactDB, "SELECT name FROM sqlite_master WHERE type='table' AND name='Items';", -1, &statementChk, nil);
            
            boo = FALSE;
            
            if (sqlite3_step(statementChk) == SQLITE_ROW)
            {
                boo = TRUE;
            }
            sqlite3_finalize(statementChk);
            if(boo)
            {
                sql_stmt = "ALTER TABLE 'main'.'Items' ADD COLUMN 'status_changed_date' DATETIME";
                
                if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
                {
                    [self setSqlStatus:@"Failed to create table"];
                }
                sql_stmt = "create index index_items_items_id on items(item_id)";
                if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
                {
                    [self setSqlStatus:@"Failed to create table"];
                }
            }
            sqlite3_prepare_v2(_contactDB, "SELECT name FROM sqlite_master WHERE type='table' AND name='Items';", -1, &statementChk, nil);
            
            boo = FALSE;
            
            if (sqlite3_step(statementChk) == SQLITE_ROW)
            {
                boo = TRUE;
            }
            sqlite3_finalize(statementChk);
            if(boo)
            {
                sql_stmt = "ALTER TABLE 'Items' ADD COLUMN 'collection_id' INTEGER";
                
                if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
                {
                    [self setSqlStatus:@"Failed to create table"];
                }
                sql_stmt = "create index index_items_items_id on items(collection_id,item_id)";
                if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
                {
                    [self setSqlStatus:@"Failed to create table"];
                }
                sql_stmt = "update items set collection_id = (select collection_item.collection_id from collection_item where collection_item.item_id= items.item_id)";
                if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
                {
                    [self setSqlStatus:@"Failed to create table"];
                }
                
                
            }
            sqlite3_prepare_v2(_contactDB, "SELECT name FROM sqlite_master WHERE type='table' AND name='Items';", -1, &statementChk, nil);
            
            boo = FALSE;
            
            if (sqlite3_step(statementChk) == SQLITE_ROW)
            {
                boo = TRUE;
            }
            sqlite3_finalize(statementChk);
            if(boo)
            {
                sql_stmt = "ALTER TABLE 'main'.'Items' ADD COLUMN 'image_name' TEXT";
                
                
                if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
                {
                    [self setSqlStatus:@"Failed to create table"];
                }
                sql_stmt = "ALTER TABLE 'Items' ADD COLUMN 'image' TEXT";
                
                
                if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
                {
                    [self setSqlStatus:@"Failed to create table"];
                }
                
                
            }

                        
            
        /////////////////////index_collection_item
        sqlite3_prepare_v2(_contactDB, "SELECT name FROM sqlite_master WHERE type='index' AND name='index_collection_item';", -1, &statementChk, nil);
        
        boo = FALSE;
        
        if (sqlite3_step(statementChk) == SQLITE_ROW)
        {
            boo = TRUE;
        }
        sqlite3_finalize(statementChk);
        if (!boo)
        {
            sql_stmt = "CREATE INDEX index_collection_item on collection_item(collection_id, item_id)";
            if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                [self setSqlStatus:@"0"];
            }
            
        }
        /////////////////////index_collection_name
        sqlite3_prepare_v2(_contactDB, "SELECT name FROM sqlite_master WHERE type='index' AND name='index_collection_name';", -1, &statementChk, nil);
        
        boo = FALSE;
        
        if (sqlite3_step(statementChk) == SQLITE_ROW)
        {
            boo = TRUE;
        }
        sqlite3_finalize(statementChk);
        if (!boo)
        {
            sql_stmt = "CREATE INDEX index_collection_name on collections(name)";
            if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                [self setSqlStatus:@"0"];
            }
            
        }

        /////////////////////index_items_items_id
        sqlite3_prepare_v2(_contactDB, "SELECT name FROM sqlite_master WHERE type='index' AND name='index_items_items_id';", -1, &statementChk, nil);
        
        boo = FALSE;
        
        if (sqlite3_step(statementChk) == SQLITE_ROW)
        {
            boo = TRUE;
        }
        sqlite3_finalize(statementChk);
        if (!boo)
        {
            sql_stmt = "CREATE INDEX index_items_items_id on items(item_id)";
            if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                [self setSqlStatus:@"0"];
            }
            
        }
        /////////////////////index_items_name
        sqlite3_prepare_v2(_contactDB, "SELECT name FROM sqlite_master WHERE type='index' AND name='index_items_name';", -1, &statementChk, nil);
        
        boo = FALSE;
        
        if (sqlite3_step(statementChk) == SQLITE_ROW)
        {
            boo = TRUE;
        }
        sqlite3_finalize(statementChk);
        if (!boo)
        {
            sql_stmt = "CREATE INDEX index_items_name on items(name)";
            if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                [self setSqlStatus:@"0"];
            }
            
        }

        /////////////////////index_items_year
        sqlite3_prepare_v2(_contactDB, "SELECT name FROM sqlite_master WHERE type='index' AND name='index_items_year';", -1, &statementChk, nil);
        
        boo = FALSE;
        
        if (sqlite3_step(statementChk) == SQLITE_ROW)
        {
            boo = TRUE;
        }
        sqlite3_finalize(statementChk);
        if (!boo)
        {
            sql_stmt = "CREATE INDEX index_items_year on items(year)";
            if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                [self setSqlStatus:@"0"];
            }
            
        }

        
        /////////////////////index_currency
        sqlite3_prepare_v2(_contactDB, "SELECT name FROM sqlite_master WHERE type='index' AND name='index_currency';", -1, &statementChk, nil);
        
        boo = FALSE;
        
        if (sqlite3_step(statementChk) == SQLITE_ROW)
        {
            boo = TRUE;
        }
        sqlite3_finalize(statementChk);
        if (!boo)
        {
            sql_stmt = "CREATE INDEX index_currency on currency(currency_code)";
            if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                [self setSqlStatus:@"0"];
            }
            
        }

            /////////////////////index_collection_item
            sqlite3_prepare_v2(_contactDB, "SELECT name FROM sqlite_master WHERE type='index' AND name='index_collection_item';", -1, &statementChk, nil);
            
            boo = FALSE;
            
            if (sqlite3_step(statementChk) == SQLITE_ROW)
            {
                boo = TRUE;
            }
            sqlite3_finalize(statementChk);
            if (!boo)
            {
                sql_stmt = "CREATE INDEX index_currency on currency(currency_code)";
                if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
                {
                    [self setSqlStatus:@"0"];
                }
                
            }

        [Utility setPreference:@"coindatabaseversion" PrefValue:[NSString stringWithFormat:@"%d",DATABASE_VERSION]];
        sqlite3_finalize(_statement);
        sqlite3_close(_contactDB);
        }
    }
}
-(NSString*)getLastSeq:(NSString *)tableName
{
     
    
    _dirPaths = NSSearchPathForDirectoriesInDomains(  NSDocumentDirectory, NSUserDomainMask, YES);
    
    _docsDir = [_dirPaths objectAtIndex:0];
    NSString *sqlStatement = [[NSString alloc]initWithFormat:@"SELECT seq FROM sqlite_sequence where name='%@'" ,tableName ];
    NSString *ret =@"";
    //NSLog(@"%@" , sqlStatement);
       
     filemgr = [NSFileManager defaultManager];
    
    const char *dbpath = [_databasePath UTF8String];
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
        
         
        const char *insert_stmt = [sqlStatement UTF8String];
        
        if(sqlite3_prepare_v2(_contactDB, insert_stmt, -1, &_statement, NULL)== SQLITE_OK)
        {
            if (sqlite3_step(_statement) == SQLITE_ROW)
            {
                if(sqlite3_column_text(_statement, 0) != NULL)
                {
                    ret = [NSString stringWithUTF8String: (const char *) sqlite3_column_text(_statement, 0)];
                    [self setSqlStatus:@"1"];
                }
            }
            else
            {
                [self setSqlStatus: @"0"];
            }
        }
        sqlite3_finalize(_statement);
        sqlite3_close(_contactDB);
        
    }
    [sqlStatement release];
   // [filemgr release];
    
    return ret;

}

-(NSString*)getOneValue:(NSString*)sqlStatement
{
    NSString *ret = @"";
    _dirPaths = NSSearchPathForDirectoriesInDomains(  NSDocumentDirectory, NSUserDomainMask, YES);
   
    _docsDir = [_dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    _databasePath = [[NSString alloc] initWithString: [_docsDir stringByAppendingPathComponent: @"CoinBook.sqlite"]];
    
     filemgr = [NSFileManager defaultManager];
    
    const char *dbpath = [_databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
         const char *insert_stmt = [sqlStatement UTF8String];
        
        if(sqlite3_prepare_v2(_contactDB, insert_stmt, -1, &_statement, NULL)== SQLITE_OK)
        {
            if (sqlite3_step(_statement) == SQLITE_ROW)
            {
                if(sqlite3_column_text(_statement, 0) != NULL)
                {
                    ret = [NSString stringWithUTF8String: (const char *) sqlite3_column_text(_statement, 0)];
                   
                }
            }
            else
            {
                //[mainDelegate.DataBase setStatus: @"0"];
            }
        }
        sqlite3_finalize(_statement);
        sqlite3_close(_contactDB);
        
    }
    else
    {
        //[mainDelegate.DataBase setStatus: @"0"];
    }
    [sqlStatement release];
    //[filemgr release];
    return ret;
    
}
-(void)populateCountry
{
    
    _dirPaths = NSSearchPathForDirectoriesInDomains(  NSDocumentDirectory, NSUserDomainMask, YES);
    
    _docsDir = [_dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    _databasePath = [[NSString alloc] initWithString: [_docsDir stringByAppendingPathComponent: @"CoinBook.sqlite"]];
    
     filemgr = [NSFileManager defaultManager];
    const char *dbpath = [_databasePath UTF8String];
     //sqlite3_stmt *statementChk;
    
   
    {
        
    
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
        char *errMsg;
        const char *sql_stmt;
       // sql_stmt = "drop TABLE 'country'";
       // if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
       // {
      //      [self setSqlStatus:@"Failed to create table"];
       // }
       // sql_stmt = "CREATE TABLE 'country' ('country_code' TEXT PRIMARY KEY  NOT NULL , 'country' TEXT)";
        //if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        //{
        //    [self setSqlStatus:@"Failed to create table"];
        //}

    sql_stmt = "insert  into country (country, country_code)   values( 'Select Country','AA')";
        if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'AFGHANISTAN','AF')";
        if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'ALAND ISLANDS','AX')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'ALBANIA','AL')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'ALGERIA','DZ')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'AMERICAN SAMOA','AS')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'ANDORRA','AD')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'ANGOLA','AO')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'ANGUILLA','AI')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'ANTIGUA AND BARBUDA','AG')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'ARGENTINA','AR')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'ARMENIA','AM')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'ARUBA','AW')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'AUSTRALIA','AU')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'AUSTRIA','AT')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'AZERBAIJAN','AZ')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'BAHAMAS','BS')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'BAHRAIN','BH')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'BANGLADESH','BD')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'BARBADOS','BB')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'BELARUS','BY')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'BELGIUM','BE')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'BELIZE','BZ')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'BENIN','BJ')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'BERMUDA','BM')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'BHUTAN','BT')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'BOLIVIA','BO')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'BOSNIA AND HERZEGOVINA','BA')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'BOTSWANA','BW')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'BOUVET ISLAND','BV')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'BRAZIL','BR')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'BRITISH INDIAN OCEAN TERRITORY','IO')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'BRUNEI DARUSSALAM','BN')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'BULGARIA','BG')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'BURKINA FASO','BF')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'BURUNDI','BI')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'CAMBODIA','KH')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'CAMEROON','CM')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'CANADA','CA')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'CAPE VERDE','CV')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'CAYMAN ISLANDS','KY')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'CENTRAL AFRICAN REPUBLIC','CF')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'CHAD','TD')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'CHILE','CL')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'CHINA','CN')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'CHRISTMAS ISLAND','CX')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'COCOS (KEELING) ISLANDS','CC')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'COLOMBIA','CO')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'COMOROS','KM')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'CONGO','CG')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'CONGO, THE DEMOCRATIC REPUBLIC OF THE','CD')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'COOK ISLANDS','CK')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'COSTA RICA','CR')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'COTE D',IVOIRE','CI')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'CROATIA','HR')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'CUBA','CU')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'CYPRUS','CY')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'CZECH REPUBLIC','CZ')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'DENMARK','DK')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'DJIBOUTI','DJ')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'DOMINICA','DM')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'DOMINICAN REPUBLIC','DO')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'ECUADOR','EC')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'EGYPT','EG')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'EL SALVADOR','SV')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'EQUATORIAL GUINEA','GQ')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'ERITREA','ER')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'ESTONIA','EE')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'ETHIOPIA','ET')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'EUROPEAN UNION','EU')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'FALKLAND ISLANDS (MALVINAS) ','FK')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'FAROE ISLANDS','FO')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'FIJI','FJ')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'FINLAND','FI')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'FRANCE','FR')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'FRENCH GUIANA','GF')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'FRENCH POLYNESIA','PF')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'FRENCH SOUTHERN TERRITORIES','TF')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'GABON','GA')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'GAMBIA','GM')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'GEORGIA','GE')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'GERMANY','DE')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'GHANA','GH')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'GIBRALTAR','GI')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'GREECE','GR')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'GREENLAND','GL')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'GRENADA','GD')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'GUADELOUPE','GP')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'GUAM','GU')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'GUATEMALA','GT')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'GUERNSEY','GG')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'GUINEA','GN')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'GUINEA-BISSAU','GW')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'GUYANA','GY')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'HAITI','HT')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'HEARD ISLAND AND MCDONALD ISLANDS','HM')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'HOLY SEE (VATICAN CITY STATE)','VA')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'HONDURAS','HN')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'HONG KONG','HK')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'HUNGARY','HU')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'ICELAND','IS')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'INDIA','IN')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'INDONESIA','ID')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'IRAN, ISLAMIC REPUBLIC OF','IR')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'IRAQ','IQ')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'IRELAND','IE')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'ISLE OF MAN','IM')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'ISRAEL','IL')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'ITALY','IT')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'JAMAICA','JM')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'JAPAN','JP')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'JERSEY','JE')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'JORDAN','JO')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'KAZAKHSTAN','KZ')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'KENYA','KE')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'KIRIBATI','KI')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'KOREA, DEMOCRATIC PEOPLE',S REPUBLIC OF','KP')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'KOREA, REPUBLIC OF','KR')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'KUWAIT','KW')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'KYRGYZSTAN','KG')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'LAO PEOPLE',S DEMOCRATIC REPUBLIC','LA')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'LATVIA','LV')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'LEBANON','LB')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'LESOTHO','LS')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'LIBERIA','LR')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'LIBYAN ARAB JAMAHIRIYA','LY')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'LIECHTENSTEIN','LI')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'LITHUANIA','LT')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'LUXEMBOURG','LU')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'MACAO','MO')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'MACEDONIA','MK')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'MADAGASCAR','MG')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'MALAWI','MW')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'MALAYSIA','MY')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'MALDIVES','MV')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'MALI','ML')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'MALTA','MT')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'MARSHALL ISLANDS','MH')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'MARTINIQUE','MQ')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'MAURITANIA','MR')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'MAURITIUS','MU')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'MAYOTTE','YT')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        }
    sql_stmt = "insert  into country (country, country_code)   values( 'MEXICO','MX')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'MICRONESIA, FEDERATED STATES OF','FM')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'MOLDOVA, REPUBLIC OF','MD')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'MONACO','MC')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'MONGOLIA','MN')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'MONTENEGRO','ME')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'MONTSERRAT','MS')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'MOROCCO','MA')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'MOZAMBIQUE','MZ')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'MYANMAR','MM')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'NAMIBIA','NA')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'NAURU','NR')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'NEPAL','NP')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'NETHERLANDS','NL')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'NETHERLANDS ANTILLES','AN')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'NEW CALEDONIA','NC')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'NEW ZEALAND','NZ')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'NICARAGUA','NI')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'NIGER','NE')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'NIGERIA','NG')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'NIUE','NU')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'NORFOLK ISLAND','NF')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'NORTHERN MARIANA ISLANDS','MP')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'NORWAY','NO')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'OMAN','OM')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'PAKISTAN','PK')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'PALAU','PW')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'PANAMA','PA')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'PAPUA NEW GUINEA','PG')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'PARAGUAY','PY')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'PERU','PE')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'PHILIPPINES','PH')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'PITCAIRN','PN')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'POLAND','PL')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'PORTUGAL','PT')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'PUERTO RICO','PR')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'QATAR','QA')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'REUNION','RE')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'ROMANIA','RO')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'RUSSIAN FEDERATION','RU')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'RWANDA','RW')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SAINT HELENA','SH')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SAINT KITTS AND NEVIS','KN')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SAINT LUCIA','LC')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SAINT PIERRE AND MIQUELON','PM')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SAINT VINCENT AND THE GRENADINES','VC')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SAMOA','WS')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SAN MARINO','SM')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SAO TOME AND PRINCIPE','ST')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SAUDI ARABIA','SA')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SENEGAL','SN')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SERBIA','RS')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SEYCHELLES','SC')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SIERRA LEONE','SL')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SINGAPORE','SG')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SLOVAKIA','SK')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SLOVENIA','SI')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SOLOMON ISLANDS','SB')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SOMALIA','SO')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SOUTH AFRICA','ZA')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS','GS')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SPAIN','ES')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SRI LANKA','LK')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SUDAN','SD')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SURINAME','SR')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SVALBARD AND JAN MAYEN','SJ')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SWAZILAND','SZ')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SWEDEN','SE')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SWITZERLAND','CH')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'SYRIAN ARAB REPUBLIC','SY')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'TAIWAN','TW')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'TAJIKISTAN','TJ')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'TANZANIA, UNITED REPUBLIC OF','TZ')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'THAILAND','TH')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'TIMOR-LESTE','TL')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'TOGO','TG')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'TOKELAU','TK')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'TONGA','TO')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'TRINIDAD AND TOBAGO','TT')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'TUNISIA','TN')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'TURKEY','TR')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'TURKMENISTAN','TM')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'TURKS AND CAICOS ISLANDS','TC')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'TUVALU','TV')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'UGANDA','UG')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'UKRAINE','UA')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'UNITED ARAB EMIRATES','AE')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'UNITED KINGDOM','GB')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'UNITED STATES','US')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'UNITED STATES MINOR OUTLYING ISLANDS','UM')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'URUGUAY','UY')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'UZBEKISTAN','UZ')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'VANUATU','VU')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'VENEZUELA','VE')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'VIET NAM','VN')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'VIRGIN ISLANDS, BRITISH','VG')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'VIRGIN ISLANDS, U.S.','VI')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'WALLIS AND FUTUNA','WF')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'WESTERN SAHARA','EH')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'YEMEN','YE')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'ZAMBIA','ZM')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    sql_stmt = "insert  into country (country, country_code)   values( 'ZIMBABWE','ZW')"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            [self setSqlStatus:@"Failed to create table"];
        } 
    
    
    }
    }
    sqlite3_finalize(_statement);
    sqlite3_close(_contactDB);
  //  [filemgr release];

}
    -(void)populateCurrency
    {
        
        _dirPaths = NSSearchPathForDirectoriesInDomains(  NSDocumentDirectory, NSUserDomainMask, YES);
        
        _docsDir = [_dirPaths objectAtIndex:0];
        
        // Build the path to the database file
        _databasePath = [[NSString alloc] initWithString: [_docsDir stringByAppendingPathComponent: @"CoinBook.sqlite"]];
        
         filemgr = [NSFileManager defaultManager];
        const char *dbpath = [_databasePath UTF8String];
       
        {
            
            if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
            {
            char *errMsg;
            const char *sql_stmt;
            sql_stmt = "drop TABLE 'currency'";
            if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                [self setSqlStatus:@"Failed to create table"];
            }
            sql_stmt = "CREATE TABLE 'currency' ('currency_code' TEXT PRIMARY KEY  NOT NULL , 'currency' TEXT)";
            if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                [self setSqlStatus:@"Failed to create table"];
            }

            sql_stmt = "insert  into currency (currency_code, currency)   values('AED','Dirham')";
            if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {             [self setSqlStatus:@"Failed to create table"];         }
            
            sql_stmt = "insert  into currency (currency_code, currency)   values ('AFN','Afghani')";
            if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values('All','Lek')";
            if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'AMD' ,'Dram' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'ANG' ,'Netherlands Antillian Guilder' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'AOA' ,'Kwanza' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'ARS' ,'Argentine Peso' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'AUD' ,'Australian Dollar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'AWG' ,'Aruban Guilder' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'AZN' ,'Azerbaijanian Manat' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'BAM' ,'Convertible Mark' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'BBD' ,'Barbados Dollar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'BDT' ,'Taka' )";
            if (sqlite3_exec(_contactDB,sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'BGN' ,'BulgarianLev' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'BHD' ,'Bahraini Dinar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'BIF' ,'Burundi Franc' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'BMD' ,'Bermudian Dollar(customarily:BermudaDollar)' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'BND' ,'Brunei Dollar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'BOB' ,'Boliviano' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'BRL' ,'Brazilian Real' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'BSD' ,'Bahamian Dollar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'BTN' ,'Ngultrum' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'BWP' ,'Pula' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'BYR' ,'Belarussian Ruble' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'BZD' ,'Belize Dollar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'CAD' ,'Canadian Dollar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'CDF' ,'Franc Congolais' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'CHF' ,'Swiss Franc' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'CLP' ,'Chilean Peso' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'CNY' ,'Yuan Renminbi' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'COP' ,'Colombian Peso' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'CRC' ,'Costa Rican Colon' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'CUP' ,'Cuban Peso' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'CVE' ,'CapeVerde Escudo' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'CZK' ,'Czech Koruna' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'DJF' ,'Djibouti Franc' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'DKK' ,'Danish Krone' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'DOP' ,'Dominican Peso' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'DZD' ,'Algerian Dinar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'EEK' ,'Kroon' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'EGP' ,'Egyptian Pound' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'ERN' ,'Nakfa' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'ETB' ,'Ethopian Birr' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'EUR' ,'Euro' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'FJD' ,'Fiji Dollar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'FKP' ,'Falkland Islands Pound' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'GBP' ,'Pound Sterling' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'GEL' ,'Lari' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'GHS' ,'Cedi' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'GIP' ,'Gibraltar Pound' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'GMD' ,'Dalasi' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'GNF' ,'Guinea Franc' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'GTQ' ,'Quetzal' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'GYD' ,'Guyana Dollar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'GWP' ,'Guinea-BisauPeso' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'HKD' ,'Honk Kong Dollar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'HNL' ,'Lempira' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'HRK' ,'Kuna' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'HTG' ,'Gourde' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'HUF' ,'Forint' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'IDR' ,'Rupiah' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'ILS' ,'New Israeli Sheqel' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'INR' ,'Indian Rupee' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'IQD' ,'Iraqi Dinar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'IRR' ,'Iranian Rial' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'ISK' ,'Iceland Krona' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'JMD' ,'Jamaican Dollar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'JOD' ,'Jordanian Dinar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'JPY' ,'Yen' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'KES' ,'KenyanShilling' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'KGS' ,'Som' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'KHR' ,'Riel' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'KMF' ,'Comoro Franc' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'KPW' ,'North KoreanWon' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'KRW' ,'Won' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'KWD' ,'Kuwaiti Dinar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'KYD' ,'Cayman Islands Dollar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'KZT' ,'Tenge' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'LAK' ,'Kip' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'LBP' ,'Lebanese Pound' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'LKR' ,'SriLanka Rupee' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'LRD' ,'Liberian Dollar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'LSL' ,'Loti' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'LTL' ,'Lithuanian Litas' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'LVL' ,'Latvian Lats' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'LYD' ,'Libyan Dinar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'MAD' ,'Morrocan Dirham' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'MDL' ,'Moldovan Leu' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'MGA' ,'Malagasy Ariary' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'MKD' ,'Denar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'MMK' ,'Kyat' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'MNT' ,'Tugrik' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'MOP' ,'Pataca' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'MRO' ,'Ouguiya' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'MUR' ,'Mauritius Rupee' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'MVR' ,'Rufiyaa' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'MWK' ,'Kwacha' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'MXN' ,'Mexican Peso' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'MYR' ,'Malaysian Ringgit' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'MZN' ,'Metical' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'NAD' ,'Namibia Dollar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'NGN' ,'Naira' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'NIO' ,'Cordoba Oro' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'NOK' ,'Norwegian Krone' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'NPR' ,'Nepalese Rupee' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'NZD' ,'New Zealand Dollar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'OMR' ,'Rial Omani' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'PAB' ,'Balboa' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'PEN' ,'Nuevo Sol' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'PGK' ,'Kina' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'PHP' ,'Philippine Peso' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'PKR' ,'Pakistan Rupee' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'PLN' ,'Zloty' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'PYG' ,'Guarani' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'QAR' ,'Qatari Rial' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'RON' ,'New Leu' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'RSD' ,'Serbian Dinar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'EUR' ,'Vatican' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'EUR' ,'San Marino' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'RUB' ,'Russian Ruble' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'RWF' ,'Rwanda Franc' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'SAR' ,'Saudi Riyal' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'SBD' ,'Solomon Islands Dollar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'SCR' ,'Seychelles Rupee' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'SDG' ,'Sudanese Pound' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'SEK' ,'Swedish Krona' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'SGD' ,'Singapore Dollar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'SHP' ,'St.Helena Pound' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'SKK' ,'Slovak Koruna' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'SLL' ,'Leone' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'SOS' ,'Somali Shilling' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'SRD' ,'Suriname Dollar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'STD' ,'Dobra' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'SVC' ,'ElSalvador Colon' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'SYP' ,'Syrian Pound' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'SZL' ,'Lilangeni' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'THB' ,'Baht' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'TJS' ,'Somoni' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'TMM' ,'Manat' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'TND' ,'Tunisian Dinar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
           sql_stmt = "insert  into currency (currency_code, currency)   values( 'TOP' ,'Pa'anga' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
                sql_stmt = "insert  into currency (currency_code, currency)   values( 'TRY' ,'New Turkish Lira' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
                sql_stmt = "insert  into currency (currency_code, currency)   values( 'TTD' ,'Trinidadand Tobago Dollar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
                sql_stmt = "insert  into currency (currency_code, currency)   values( 'TWD' ,'New Taiwan Dollar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
                sql_stmt = "insert  into currency (currency_code, currency)   values( 'TZS' ,'Tanzanian Shilling' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
                sql_stmt = "insert  into currency (currency_code, currency)   values( 'UAH' ,'Hryvnia' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
                sql_stmt = "insert  into currency (currency_code, currency)   values( 'UGX' ,'Uganda Shilling' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
                sql_stmt = "insert  into currency (currency_code, currency)   values( 'USD' ,'US Dollar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
                sql_stmt = "insert  into currency (currency_code, currency)   values( 'UYU' ,'Peso Uruguayo' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
                sql_stmt = "insert  into currency (currency_code, currency)   values( 'UZS' ,'Uzbekistan Sum' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
                sql_stmt = "insert  into currency (currency_code, currency)   values( 'VEF' ,'Bolivar Fuerte' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
                sql_stmt = "insert  into currency (currency_code, currency)   values( 'VND' ,'Dong' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
                sql_stmt = "insert  into currency (currency_code, currency)   values( 'VUV' ,'Vatu' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
                sql_stmt = "insert  into currency (currency_code, currency)   values( 'WST' ,'Tala' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
                sql_stmt = "insert  into currency (currency_code, currency)   values( 'XAF' ,'CFA Franc' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
                sql_stmt = "insert  into currency (currency_code, currency)   values( 'XAG' ,'Silver' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         }
                sql_stmt = "insert  into currency (currency_code, currency)   values( 'XAU' ,'Gold' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         } 
                sql_stmt = "insert  into currency (currency_code, currency)   values( 'XCD' ,'East Carribean Dollar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         } 
                sql_stmt = "insert  into currency (currency_code, currency)   values( 'XDR' ,'SDR' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         } 
                sql_stmt = "insert  into currency (currency_code, currency)   values( 'XOF' ,'CFA Franc' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         } 
                sql_stmt = "insert  into currency (currency_code, currency)   values( 'XPD' ,'Palladium' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         } 
                sql_stmt = "insert  into currency (currency_code, currency)   values( 'XPF' ,'CFP Franc' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         } 
                sql_stmt = "insert  into currency (currency_code, currency)   values( 'XPT' ,'Platinum' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         } 
                sql_stmt = "insert  into currency (currency_code, currency)   values( 'YER' ,'YemeniRial' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         } 
                sql_stmt = "insert  into currency (currency_code, currency)   values( 'ZAR' ,'Rand' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         } 
                sql_stmt = "insert  into currency (currency_code, currency)   values( 'ZMK' ,'Kwacha' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         } 
                sql_stmt = "insert  into currency (currency_code, currency)   values( 'ZWR' ,'Zimbabwe Dollar' )"; if (sqlite3_exec(_contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)         {             [self setSqlStatus:@"Failed to create table"];         } 
                 

            
            
            
        }
        }
        sqlite3_finalize(_statement);
        sqlite3_close(_contactDB);
      //  [filemgr release];

    }


@end
