//
//  Lookup.m
//  CoinBook
//
//  Created by Vadim Re on 2013-01-21.
//  Copyright (c) 2013 Vadim Re. All rights reserved.
//

#import "Lookup.h"

#import "DS.h"
#import "AppDelegate.h"


@implementation Lookup
@synthesize key, value, ind;



AppDelegate *mainDelegate;

#pragma mark-Init
-(id)init
{
    self = [super init];
    key = [[NSString alloc]init];
    
    value = [[NSString alloc] init];
       mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    return self;
}

-(void)populateLookup:(NSString*)lookupName Content:(NSMutableArray*)content
{
    NSMutableArray *items = [[NSMutableArray alloc] init];
  // NSMutableArray *content = [NSMutableArray new];
   
    const char *sql = [[NSString  stringWithFormat:@" select SUBSTR(%@, 1,1) as  ind, *  from %@ order by ind" ,lookupName, lookupName] UTF8String];
 
    const char *dbpath = [ mainDelegate.DataBase.DatabasePath UTF8String];
    //sqlite3_stmt    *statement;
    
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
        // const char *query_stmt = [selectSQL UTF8String];
        
        if (sqlite3_prepare_v2(_contactDB, sql, -1, &_statement, NULL) == SQLITE_OK)
        {
            NSString  *pk ;
            NSMutableDictionary *row;
            BOOL first = true;
            while (sqlite3_step(_statement) == SQLITE_ROW)
            {
                
                Lookup *item = [[Lookup alloc] init  ];

                [item setValue: [NSString stringWithUTF8String: (const char *) sqlite3_column_text(_statement, 2)]];
                [item setKey:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 1)]];
                [item setInd:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 0)]];
                if(first)
                {
                    pk=item.ind;
                }
                if(![pk isEqualToString: item.ind])
                {
                    row = [[NSMutableDictionary alloc] init];
                    [row setValue:pk forKey:@"headerTitle"];
                    [row setValue:items forKey:@"item"];
                    [content addObject:row];
                    [row release];
                    pk=item.ind;
                    [items release];
                    items = [[NSMutableArray alloc] init];
                }
                first=FALSE;
                 [items addObject:item];                
                [item release];
                // [name release];
            }
            
            sqlite3_finalize(_statement);
        }
        sqlite3_close(_contactDB);
    }
    [items release];
    
  //  return content ;

}
@end
