//
//  AppDelegate.m
//  CoinBook
//
//  Created by Vadim Re on 2012-10-22.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import "AppDelegate.h"
#import "DS.h"
#import "CheckPassword.h"
#import "ViewController.h"
#import "ImportCSVViewController.h"
#import "Item.h"
#import "Utility.h"

@implementation AppDelegate
@synthesize navigationController = _navigationController;
@synthesize DataBase, Collection,ItemList,itemIndex, MyMarket, Item;
@synthesize DropdownId, AppOptions, CSVText, AppPath, DropDownKey, DropDownValue;
@synthesize ItemTemp, CollectionTemp, CoinUser,selectedRowIndexCollection,itemId;
@synthesize whereField, sortField,fileName, isPassword;


const int DROPDOWN_SETTINGS_COUNTRY=5;
const int DROPDOWN_SETTINGS_CURRENCY=6;
const int RETURN_TO_ALL_COLLECTION=3;
const int RETURN_TO_ONE_COLLECTION=4;
const int RETURN_TO_HELP=7;
const int COINS_MAX=200;
const int BOOKS_MAX= 100000;
const  NSString *Message_MAX_Coins = @"The maximum pockets for this book has been reached. Create another book.";
const NSString *DateFormat= @"dd-MM-yyyy";
const NSString *DateFormatOld= @"yyyy-MM-dd";
const int TAG_NUMBERKEYBOARD=205;
const int TAG_Settings = 100;


#pragma mark - Property

-(void)setDataBase:(DS *)newDataBase
{
    _dataBase = newDataBase;
}
-(DS*)DataBase
{
    return _dataBase;
}
-(void)setCollection:(Collection *)newCollection
{
    _collection = newCollection;
}
-(Collection*)Collection
{
    return _collection;
}

-(void)setItem:(Item *)ItemNewTemp
{
    _item = ItemNewTemp;
}
-(Item*)Item
{
    return _item;
}
 
#pragma mark - Methods

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    NSURL *url = (NSURL *)[launchOptions valueForKey:UIApplicationLaunchOptionsURLKey];
   
    self.window = [[[UIWindow alloc]  initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
   
    
    if (url != nil && [url isFileURL])
    {
        AppOptions = 1;
         NSError *outError;
        
        [self   setFileName:[[[NSString stringWithFormat:@"%@",url] lastPathComponent] stringByReplacingOccurrencesOfString:@"%20" withString:@" "]];
        NSLog(@"%@", fileName);
        [self setCSVText:[NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&outError]];
        // self.importCSV = [[ImportCSVViewController alloc] initWithNibName:@"ImportCSVViewController" bundle:nil];
        //[self.importCSV handleOpenURL:url];
    }
    else
    {
        
       [self setAppOptions: 0];
    // Override point for customization after application launch.
    // self.window.rootViewController = self.viewController;
  //  [self.window makeKeyAndVisible];
    
    }
//    if([Utility getPreference:@"coinIsPassword"].length ==0)
//      {
//          isPassword = NO;
//      }
//      else
//      {
//          NSString *isPSW = [Utility getPreference:@"coinIsPassword"];
//          if([isPSW isEqualToString:@"NO" ])
//              {
//                  isPassword = FALSE;
//              }
//              else
//              {
//                   isPassword = TRUE;
//              }
//      }
//    if(isPassword)
//    {
    self.checkPassword = [[[CheckPassword alloc] initWithNibName:@"CheckPassword" bundle:nil] autorelease];
   
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:self.checkPassword];
    self.window.rootViewController = self.navigationController;
//    }
    [self.window makeKeyAndVisible];
    
    return YES;
    
    
    
  }

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
-(void)dealloc
{
    [ItemTemp release];
    [super dealloc];
}
-(id)init
{
    self = [super init];
    _AppSettings = [[AppSettings alloc]init];
    _dataBase = [[DS alloc] init];
    [_dataBase createTables ];
    [_dataBase updateTables];
    _collection = [[Collection alloc]init];
    MyMarket =[[Market alloc]init];
    [self setDropdownId:0];
    CoinUser =[[User alloc]init];
    ItemList =[[NSMutableArray alloc]init];
    _item=[[Item alloc] init];
    fileName=[[NSString alloc] initWithString:@""];
    
    return self;
}

#pragma mark CSV

-(BOOL) application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication //annotation:(id)annotation
{
    if (url != nil && [url isFileURL])
    {
        //TODO replace following method
      //  [self.importCSV handleOpenURL:url];
      //  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
     //   NSString *documentsDirectory = [paths objectAtIndex:0];
        
       // NSString* theFileName = @"Penny roll(cerculaited)-6";//[[[NSString stringWithFormat:@"%@",url] lastPathComponent] stringByDeletingPathExtension];
      //  NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/Inbox/%@.csv", [theFileName stringByReplacingOccurrencesOfString:@"%20" withString:@" "]]]; //add
        NSString *fullPath =[url path];
       
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ([fileManager fileExistsAtPath:fullPath])
        {
            NSError *error;
            if (![fileManager removeItemAtPath:fullPath error:&error])
            {
                //NSLog(@"Error removing file: %@", error);
            };
        }
        
              
               
        
               
    }
    
    return YES;
}




@end
