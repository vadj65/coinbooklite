//
//  AddBookViewController.m
//  CoinBook
//
//  Created by Vadim Re on 2012-11-01.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import "AddBookViewController.h"
#import "CollectionListViewController.h"
#import "Item.h"
#import "AllItems.h"
#import <QuartzCore/QuartzCore.h>
#import "DropDownList.h"
#import "AppDelegate.h"
#import "Utility.h"

#define ALERT_UPDATE 1
#define ALERT_VALIDATE 2
#define ALERT_UPDATE_YEAR  3
#define ALERT_UPDATE_COUNTRY 4
#define ALERT_UPDATE_CURRENCY  5
#define ALERT_TAG_COUNTRY 1
#define ALERT_TAG_CURRENCY 2
#define ALERT_TAG_YEAR 6
#define ALERT_SAVE_COLLECTION @"Save collection first."
#define ALERT_UPDATE_SAVE_YEAR 7
#define TEXT_UPDATED @"Updated"
#define TEXT_Inserted @"Inserted"
#define TEXT_Failed @"Failed"
#define DROPDOWN_BOOK_COUNTRY 1
#define DROPDOWN_BOOK_CURRENCY 2


@interface AddBookViewController ()

@end

@implementation AddBookViewController

@synthesize txtBookName, txtDescription, lblStatus;








- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
        //[topNavigation setBackgroundColor:[UIColor blackColor]];
    }
    self.modalPresentationStyle = UIModalPresentationFullScreen;
    //self.viewRespectsSystemMinimumLayoutMargins = true;
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view autoresizingMask];
    [self setupTags];
    moveField = TRUE;
    
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideNumberKeyboard)];
    
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    
    [scroller addGestureRecognizer:tapGesture];
    [self.view setMultipleTouchEnabled:YES];
    [scroller setScrollEnabled:YES];
    [scroller setContentSize:CGSizeMake(320, scrView.frame.size.height)];

    [self populateScreen];
    [self registerForKeyboardNotifications];
      
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark- Buttons
- (IBAction)touchBookName:(id)sender
{
    
    UILabel *lbl= [[UILabel alloc]initWithFrame:CGRectMake(5,3,15,15)];
    lbl.textColor=[UIColor redColor];
    [lbl setText:@"Edit"];
    [lbl setBackgroundColor:[UIColor blackColor] ];
    
    [lbl sizeToFit];
    lbl.center = CGPointMake(160,25);
    [topNavigation addSubview:lbl];
    topNavigation.topItem.title = @"";
    [lbl release];
}


- (IBAction)onClickBack:(id)sender
{
   
    CollectionListViewController *vc = [[CollectionListViewController alloc] init];
        [self presentViewController: vc animated:YES completion:nil];
        [vc release];

   
}
- (IBAction)onClickBtnSendEmail:(id)sender
{
    
    if([mainDelegate.Collection CollectionId] > 0)
    {
       [mainDelegate setEmailReturn:1];
        MailViewController *vc = [[MailViewController alloc] init];
        [self presentViewController: vc animated:YES completion:nil];
        [vc release];
    }
    else
    {

        
        [Utility showAlertOneButton: ALERT_SAVE_COLLECTION alertTag:0];
    }

}

- (IBAction)onSwitchItemName:(id)sender {
}


    

- (IBAction)onClickSave:(id)sender {
    
    [self hideNumberKeyboard];
    if ([self validateFields])
    {
        //mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
     
        [mainDelegate.Collection setName: txtBookName.text];
        [mainDelegate.Collection setCoinName: txtCoinName.text];
        [mainDelegate.Collection setYearFinish: txtYearFinish.text];
        [mainDelegate.Collection setYearStart:txtYearStart.text];
        [mainDelegate.Collection setIsUseItemName:(int)sgItemName.selectedSegmentIndex];
        [mainDelegate.Collection setIsYearRange:swYearRange.on];
        [mainDelegate.Collection setDescription:txtDescription.text];
   
        [mainDelegate.Collection setMaterial:txtMaterials.text];
        [mainDelegate.Collection setFaceValue:txtFaceValue.text];

        [mainDelegate.Collection setWeight:[txtWeight.text floatValue]];
        [mainDelegate.Collection setPreciousMetal:(int)swPreciousMetal.selectedSegmentIndex];
        [mainDelegate.Collection setWeightUnit:(int)swWeightUnit.selectedSegmentIndex];
    
    
        if([mainDelegate.Collection CollectionId].length > 0)
        {
            if(swYearRange.on)
            {
               
            
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"All coins outside this year range will be deleted." message: @"" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *btnContinue = [UIAlertAction actionWithTitle:@"Continue" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                   // [self filterSold];
                } ];
                [alert addAction:btnContinue];
               
                UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil ];
                [alert addAction:cancel];
                //[alert setTag:ALERT_UPDATE_YEAR];
                
            

            }
            else
            {
                [self updateBook];
            }
           
        }
        else
        {
            [mainDelegate.Collection setDateCreated:[NSDate date]];
            [mainDelegate.Collection insertCollection];
        
            if([[mainDelegate.DataBase sqlStatus] isEqualToString:@"1"])
            {
                [self updateTopLabel:TEXT_Inserted];
                if (swYearRange.on)
                {
                    [self createItemByYear];
                }
            }
            else
            {
                [self updateTopLabel:TEXT_Failed];
            }
    
        }
       
       

        [formatter release];
        
        
    }
    else
    {
        [self showAlert:@"Validation"];
    }
    
    
}


-(void)createItemByYear
{
    if (txtYearFinish.text.length >0 && txtYearStart.text.length> 0)
    {
        
        UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleMedium];
         spinner.center =CGPointMake(160, 240);
         spinner.tag= 128;
         [self.view addSubview:spinner];
         
         [spinner startAnimating];
         [spinner release];
         

        
        Collection *execCollection =[[Collection alloc]init];
        [execCollection deleteCollectionYears:mainDelegate.Collection.CollectionId YearStart:txtYearStart.text YearFinish:txtYearFinish.text];
        [execCollection release];
        //int years = [txtYearFinish.text intValue] - [txtYearStart.text intValue] +1;
        Item *item;
        for (int currentYear = [txtYearStart.text intValue]; currentYear <=[txtYearFinish.text intValue]; currentYear = currentYear+1)
        {
            item =[[Item alloc]init];
         
            if([[item itemYearExist:currentYear] isEqualToString:@"0"])
            {
                switch (sgItemName.selectedSegmentIndex)
                {
                    case 0:
                        item.Name =@"";
                        break;
                    case 1:
                        item.Name = txtBookName.text;
                        break;
                    case 2:
                        item.Name = txtCoinName.text;
                        break;
                    
                    default:
                        break;
                }
            
                item.Year = [NSString stringWithFormat:@"%d", currentYear];
                [item setCountryCode:[mainDelegate.Collection CountryCode]];
                [item setCurrencyCode:[mainDelegate.Collection CurrencyCode]];
                
                [item setFaceValue:txtFaceValue.text.length==0?@"":txtFaceValue.text];
                [item setStatus:0];
            
                [item setWeight:[mainDelegate.Collection Weight] ];
                [item setPreciousMetal: [mainDelegate.Collection PreciousMetal]];
                [item setMaterial:[mainDelegate.Collection Material]];
                [item setWeightUnit:[mainDelegate.Collection WeightUnit]];
                [item setPrice:@"0"];
                [item setDescription:[mainDelegate.Collection Description]];
                [item setCollectionId:[[mainDelegate.Collection CollectionId] intValue]];
                [item insertItem];
            }
            [item release];
        }
        
        [mainDelegate.DataBase commandSQL:[NSString stringWithFormat:@"update collections set items_count =  (select count(*) from items where items.collection_id = collections.collection_id)  where collection_id = %d" , [[mainDelegate.Collection CollectionId] intValue]]];
        [spinner stopAnimating];
        
    }

}


- (IBAction)onClickUpdateItems:(id)sender
{
    
    if([mainDelegate.Collection CollectionId] > 0)
    {
        UIAlertController *dalert = [UIAlertController alertControllerWithTitle:@"Update item's fields" message: @"" preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *btnAllFields = [UIAlertAction actionWithTitle:@"All fields" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self updateAllFields];
        }];
        [dalert addAction:btnAllFields];
        UIAlertAction *btnEmptyFields = [UIAlertAction actionWithTitle:@"Empty fields" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self updateEmptyFields];
        }];
        [dalert addAction:btnEmptyFields];
        if(swYearRange.on)
        {
            UIAlertAction *btnYear = [UIAlertAction actionWithTitle:@"Year" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self updateYearAlert];
            }];
            [dalert addAction:btnYear];
        }
        UIAlertAction *btnCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
        [dalert addAction:btnCancel];
   
        
    }
    else
    {
        [Utility showAlertOneButton:ALERT_SAVE_COLLECTION alertTag:0];
    }
}

- (IBAction)onChangeUseCoinName:(id)sender
{
    switch (sgItemName.selectedSegmentIndex) {
        case 0:
            [txtCoinName setEnabled:NO];
            txtCoinName.text = @"";
            break;
        case 1:
            [txtCoinName setEnabled:NO];
            txtCoinName.text = txtBookName.text;
            break;
        case 2:
            [txtCoinName setEnabled:YES];
            break;
            
        default:
            break;
    }
           
}

- (IBAction)onBookNameEditEnd:(id)sender
{
    if(sgItemName.selectedSegmentIndex == 1)
    {
        txtCoinName.text = txtBookName.text;

    }
}

- (IBAction)onNameValueChanged:(id)sender
{
    if(sgItemName.selectedSegmentIndex == 1)
    {
        txtCoinName.text = txtBookName.text;        
    }
}


#pragma mark Alerts

-(void)updateAllFields
{
    if([mainDelegate.Collection CollectionId].length > 0)
    {
        
        Item *item;
        item =[[Item alloc]init];
        [item setName:txtCoinName.text];
        [item setFaceValue:txtFaceValue.text.length==0?@"":txtFaceValue.text];
        [item setWeight:txtWeight.text.length==0? 0:[txtWeight.text floatValue] ];
        [item setPreciousMetal: (int)swPreciousMetal.selectedSegmentIndex];
        [item setMaterial:txtMaterials.text];
        [item setDescription:[mainDelegate.Collection Description]];
        [item setCountryCode:[mainDelegate.Collection CountryCode]];
        [item setCurrencyCode:[mainDelegate.Collection CurrencyCode]];
        [item setWeightUnit:(int)swWeightUnit.selectedSegmentIndex];
        
        [item updateForceItemByCollectionId:[mainDelegate.Collection CollectionId]];
        [item release];
    }
}

-(void)updateYear
{
    [self updateBook];
}

-(void)updateEmptyFields
{
    if([mainDelegate.Collection CollectionId].length > 0)
    {
        Item *item;
        item =[[Item alloc]init];
        [item setName:txtCoinName.text];
        [item setFaceValue:txtFaceValue.text.length==0?@"":txtFaceValue.text];
        [item setWeight:txtWeight.text.length==0? 0:[txtWeight.text floatValue] ];
        [item setPreciousMetal: (int)swPreciousMetal.selectedSegmentIndex];
        [item setMaterial:txtMaterials.text];
        [item setWeightUnit:(int)swWeightUnit.selectedSegmentIndex];
        [item setDescription:[mainDelegate.Collection Description]];
        [item setCountryCode:[mainDelegate.Collection CountryCode]];
        [item setCurrencyCode:[mainDelegate.Collection CurrencyCode]];
        [item setWeightUnit:(int)swWeightUnit.selectedSegmentIndex];
        
        [item updateItemByCollectionId:[mainDelegate.Collection CollectionId]];
        [item release];
    }
}
-(void)updateYearAlert
{
    if ([self validateFields])
    {
        [Utility showAlert:@"All coins outside this range will be deleted." alertTag:ALERT_UPDATE_YEAR];
        
         UIAlertController *dalert = [UIAlertController alertControllerWithTitle:@"All coins outside this range will be deleted." message:  @"" preferredStyle:UIAlertControllerStyleAlert];

         UIAlertAction *btnUpdate = [UIAlertAction actionWithTitle:@"Update" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
             [self updateYear];
         }];
        [dalert addAction:btnUpdate];
        
        UIAlertAction *btnCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
        [dalert addAction:btnCancel];
        [self presentViewController:dalert animated:YES completion:nil];
    }
}
- (void)alertView:(UIAlertController *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == 0)
	{
        
        if (alertView.view.tag == ALERT_UPDATE)
		{
            if([mainDelegate.Collection CollectionId].length > 0)
            {
                Item *item;
                item =[[Item alloc]init];
                [item setName:txtCoinName.text];
                [item setFaceValue:txtFaceValue.text.length==0?@"":txtFaceValue.text];
                [item setWeight:txtWeight.text.length==0? 0:[txtWeight.text floatValue] ];
                [item setPreciousMetal: (int)swPreciousMetal.selectedSegmentIndex];
                [item setMaterial:txtMaterials.text];
                [item setDescription:[mainDelegate.Collection Description]];
                [item setCountryCode:[mainDelegate.Collection CountryCode]];
                [item setCurrencyCode:[mainDelegate.Collection CurrencyCode]];
                [item setWeightUnit:(int)swWeightUnit.selectedSegmentIndex];
                
                [item updateForceItemByCollectionId:[mainDelegate.Collection CollectionId]];
                [item release];
            }
		}

        else if(alertView.view.tag == ALERT_UPDATE_YEAR)
        {
            [self updateBook];
        }
       
        
       
	}/////////////////////////////////////////////second button/////////////////////////////////////////////
	else if (buttonIndex == 1)
	{
        if (alertView.view.tag == ALERT_UPDATE)
		{
            if([mainDelegate.Collection CollectionId].length > 0)
            {
                Item *item;
                item =[[Item alloc]init];
                [item setName:txtCoinName.text];
                [item setFaceValue:txtFaceValue.text.length==0?@"":txtFaceValue.text];
                [item setWeight:txtWeight.text.length==0? 0:[txtWeight.text floatValue] ];
                [item setPreciousMetal: (int)swPreciousMetal.selectedSegmentIndex];
                [item setMaterial:txtMaterials.text];
               
                [item setDescription:[mainDelegate.Collection Description]];
                [item setCountryCode:[mainDelegate.Collection CountryCode]];
                [item setCurrencyCode:[mainDelegate.Collection CurrencyCode]];
                [item setWeightUnit:(int)swWeightUnit.selectedSegmentIndex];
                
                [item updateItemByCollectionId:[mainDelegate.Collection CollectionId]];
                [item release];
            }
            
		}
			}
	//////////////////////////////////////////////THIRD BUTTON///////////////////////////////////
	else if (buttonIndex == 2)
	{
 		if (alertView.view.tag == ALERT_UPDATE)
		{
            if ([self validateFields])
            {
                [Utility showAlert:@"All coins outside this range will be deleted." alertTag:ALERT_UPDATE_YEAR];
            }
		}
	}
  
}




- (IBAction)onSwitchYearRange:(id)sender
{
    if (!swYearRange.on)
    {
        txtYearFinish.text = @"";
        txtYearStart.text = @"";
    }
}
//- (void)dealloc {
//    [swYearRange release];
//    [txtYearStart release];
//    [txtYearFinish release];
//    [txtCoinName release];
//    [sgItemName release];
//    [txtCountry release];
//    [txtFaceValue release];
//    [tapGesture release];
//    [topNavigation release];
//    [txtWeight release];
//    [swPreciousMetal release];
//    [btnUpdateItems release];
//    [scrView release];
//    [txtMaterials release];
//    [swWeightUnit release];
//    [sgItemName release];
//    [txtCurrency release];
//    [super dealloc];
//}

#pragma mark-Methods

-(void)updateBook
{
    [mainDelegate.Collection  updateCollection];
    [self updateTopLabel:TEXT_UPDATED];
    [self createItemByYear];

}
-(void)populateScreen
{
    if(mainDelegate.DropdownId > 0)
    {
        [self restoreCurrentState] ;
        if (mainDelegate.DropdownId == DROPDOWN_BOOK_COUNTRY)
        {
            
            [mainDelegate.Collection setCountry:mainDelegate.DropDownValue];
            [mainDelegate.Collection setCountryCode:mainDelegate.DropDownKey];
            CGPoint scrollPoint = CGPointMake(0.0, txtCountry.frame.origin.y);
            [scroller setContentOffset:scrollPoint animated:YES];
            
        }
        else if (mainDelegate.DropdownId== DROPDOWN_BOOK_CURRENCY)
        {
            
            [mainDelegate.Collection setCurrency:mainDelegate.DropDownValue];
            [mainDelegate.Collection setCurrencyCode:mainDelegate.DropDownKey];
            CGPoint scrollPoint = CGPointMake(0.0, txtCurrency.frame.origin.y);
            [scroller setContentOffset:scrollPoint animated:YES];
        }
        [mainDelegate setDropdownId:0];
    }
    
    
    
    if([mainDelegate.Collection CollectionId].length == 0)
    {
        isEdit = FALSE;
        topNavigation.topItem.title = @"New";
        if([mainDelegate.Collection Country].length ==0)
        {
            [mainDelegate.Collection setCountry:[mainDelegate.CoinUser country]];
            [mainDelegate.Collection setCountryCode:[mainDelegate.CoinUser countryCode]];
        }
        if([mainDelegate.Collection Currency].length ==0)
        {
            [mainDelegate.Collection setCurrency:[mainDelegate.CoinUser currency]];
            [mainDelegate.Collection setCurrencyCode:[mainDelegate.CoinUser currencyCode]];
        }
    }
    else
    {
        isEdit = TRUE;
        topNavigation.topItem.title = @"Edit";
    }
    txtBookName.text = [mainDelegate.Collection Name];
    txtDescription.text = [mainDelegate.Collection Description];
    txtCoinName.text = [mainDelegate.Collection CoinName];
    txtYearFinish.text=[mainDelegate.Collection  YearFinish];
    txtYearStart.text=[mainDelegate.Collection YearStart];
    
    txtCountry.delegate = self;
    txtCurrency.text = [mainDelegate.Collection Currency];
    txtFaceValue.text = [mainDelegate.Collection FaceValue];
    txtMaterials.text =[mainDelegate.Collection Material];
    if([mainDelegate.Collection  Weight] ==0)
    {
        txtWeight.text = @"";
    }
    else
    {
        txtWeight.text = [NSString stringWithFormat:@"%1.2f",[mainDelegate.Collection  Weight]];
    }
    swPreciousMetal.selectedSegmentIndex = [mainDelegate.Collection  PreciousMetal];
    swWeightUnit.selectedSegmentIndex = [mainDelegate.Collection  WeightUnit];
    
    txtCountry.text = [mainDelegate.Collection Country];
    txtCurrency.text = [mainDelegate.Collection Currency];
    
    
    sgItemName.selectedSegmentIndex= [mainDelegate.Collection isUseItemName];
    
    if([mainDelegate.Collection isYearRange] == YES)
    {
        swYearRange.on= TRUE;
    }

}
-(void)setupTags
{
    txtYearFinish.tag = ALERT_TAG_YEAR;
    txtYearStart.tag = ALERT_TAG_YEAR;
}

-(IBAction)hideKeyboard:(id)sender
{
	[txtCoinName  resignFirstResponder];
	[txtYearStart  resignFirstResponder];
	[txtYearFinish  resignFirstResponder];
	[txtBookName  resignFirstResponder];
    [txtDescription resignFirstResponder];
    [txtFaceValue resignFirstResponder];
    [txtCountry resignFirstResponder];
    [txtMaterials resignFirstResponder];
    [txtWeight resignFirstResponder];
    
	if(isEdit)
    {
        topNavigation.topItem.title = @"Edit";
    }
    else
    {
        topNavigation.topItem.title = @"New";
    }
}

-(void)hideNumberKeyboard
{
	[txtCoinName  resignFirstResponder];
	[txtYearStart  resignFirstResponder];
	[txtYearFinish  resignFirstResponder];
	[txtBookName  resignFirstResponder];
    [txtDescription resignFirstResponder];
    [txtFaceValue resignFirstResponder];
    [txtCountry resignFirstResponder];
    [txtMaterials resignFirstResponder];
    [txtWeight resignFirstResponder];

    if(isEdit)
    {
        topNavigation.topItem.title = @"Edit";
    }
    else
    {
        topNavigation.topItem.title = @"New";
    }
}

- (void)dealloc {
    [sgItemName release];
    sgItemName = nil;
    [txtCountry release];
    txtCountry = nil;
    [txtFaceValue release];
    txtFaceValue = nil;
    [topNavigation release];
    topNavigation = nil;
    [txtWeight release];
    txtWeight = nil;
    [swPreciousMetal release];
    swPreciousMetal = nil;
    [btnUpdateItems release];
    btnUpdateItems = nil;
    [scrView release];
    scrView = nil;
    [txtMaterials release];
    txtMaterials = nil;
    [swWeightUnit release];
    swWeightUnit = nil;
    [errorMessage release];
    [sgItemName release];
    sgItemName = nil;
    [txtCurrency release];
    [super dealloc];
    
}







-(void)saveCurrentState
{
   // mainDelegate.CollectionTemp = [[Collection alloc]init];
    [mainDelegate.Collection setName: txtBookName.text];
    [mainDelegate.Collection setCoinName: txtCoinName.text];
    [mainDelegate.Collection setYearFinish: txtYearFinish.text];
    [mainDelegate.Collection setYearStart:txtYearStart.text];
    [mainDelegate.Collection setIsUseItemName:(int)sgItemName.selectedSegmentIndex];
    [mainDelegate.Collection setIsYearRange:swYearRange.on];
    
    [mainDelegate.Collection setDescription:txtDescription.text];
    
    [mainDelegate.Collection setMaterial:txtMaterials.text];
    [mainDelegate.Collection setFaceValue:txtFaceValue.text];
    
    [mainDelegate.Collection setWeight:[txtWeight.text floatValue]];
    [mainDelegate.Collection setPreciousMetal:(int)swPreciousMetal.selectedSegmentIndex];
    [mainDelegate.Collection setWeightUnit:(int)swWeightUnit.selectedSegmentIndex];
    [mainDelegate.Collection setCountryCode:[mainDelegate.Collection CountryCode]];
    [mainDelegate.Collection setCurrencyCode:[mainDelegate.Collection CurrencyCode]];
    
    
}
-(void)restoreCurrentState
{
    /*
    [mainDelegate.Collection setName: [mainDelegate.CollectionTemp Name]];
    [mainDelegate.Collection setCoinName: [mainDelegate.CollectionTemp CoinName]];
    [mainDelegate.Collection setYearFinish: [mainDelegate.CollectionTemp YearFinish]];
    [mainDelegate.Collection setYearStart:[mainDelegate.CollectionTemp YearStart]];
    [mainDelegate.Collection setIsUseItemName:[mainDelegate.CollectionTemp isUseItemName]];
    [mainDelegate.Collection setIsYearRange:[mainDelegate.CollectionTemp isYearRange]];
    [mainDelegate.Collection setDescription:[mainDelegate.CollectionTemp Description]];
    
    [mainDelegate.Collection setMaterial:[mainDelegate.CollectionTemp Material]];
    [mainDelegate.Collection setFaceValue:[mainDelegate.CollectionTemp FaceValue]];
    
    [mainDelegate.Collection setWeight:[mainDelegate.CollectionTemp Weight]];
    [mainDelegate.Collection setPreciousMetal:[mainDelegate.CollectionTemp PreciousMetal]];
    [mainDelegate.Collection setWeightUnit:[mainDelegate.CollectionTemp WeightUnit]];
    [mainDelegate.Collection setCountryCode:[mainDelegate.Collection CountryCode]];
    [mainDelegate.Collection setCurrencyCode:[mainDelegate.Collection CurrencyCode]];
    */
    
}



#pragma mark Touches

/*-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)even
{

}
*/


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    BOOL retBool = NO;
   if (textField.tag ==ALERT_TAG_COUNTRY)
   {
       [self saveCurrentState];
       [mainDelegate setDropdownId:DROPDOWN_BOOK_COUNTRY];
       [mainDelegate setDropDownKey:[mainDelegate.Collection CountryCode]];
        [mainDelegate setDropDownValue:[mainDelegate.Collection Country]];
       DropDownList *vc = [[DropDownList alloc] init];
         [self presentViewController: vc animated:YES completion:nil];
          [vc release];
      
   }
   else if (textField.tag ==ALERT_TAG_CURRENCY)
   {
       [self saveCurrentState];
       [mainDelegate setDropdownId:DROPDOWN_BOOK_CURRENCY];
       [mainDelegate setDropDownKey:[mainDelegate.Collection CurrencyCode]];
       [mainDelegate setDropDownValue:[mainDelegate.Collection Currency]];
       DropDownList *vc = [[DropDownList alloc] init];
       [self presentViewController: vc animated:YES completion:nil];
       [vc release];
      

   }
   else if (textField.tag ==ALERT_TAG_YEAR)
   {
       if(swYearRange.on)
       {
           retBool = YES;
       }
       else
       {
           UIAlertController *dalert = [UIAlertController alertControllerWithTitle:@"Year range is not selecred." message:  @"Filter" preferredStyle:UIAlertControllerStyleAlert];
           dalert.view.tag=128;
           UIAlertAction *btnCancel = [UIAlertAction actionWithTitle:@"Continue" style:UIAlertActionStyleDefault handler:nil];
           [dalert addAction:btnCancel];
        
           [self presentViewController:dalert animated:YES completion:nil];
           
           retBool = NO;
       }
   }
    else
    {
        retBool = YES;
        activeField = textField;
    }
    return retBool;
}





#pragma mark Validation
-(BOOL)validateFields
{
    errorMessage = [[NSString alloc] init];
    BOOL isValid = true;
    
    NSString *collectionIdSQL ;
    BOOL booksNames ;
    
    if(mainDelegate.Collection.CollectionId == nil)
    {
        collectionIdSQL=@"-1";
    }
    else
    {
        collectionIdSQL = mainDelegate.Collection.CollectionId;
        
    }
    
    Collection *execCol = [[Collection alloc]init];
    booksNames = [execCol  checkBookName:txtBookName.text CollectionId:[collectionIdSQL intValue]];
    if (!booksNames)
    {
        isValid = false;
        errorMessage = [errorMessage stringByAppendingString:@"Book name already exist.\n"];
        
    }
       if (txtBookName.text.length==0)
        {
            isValid = false;
            errorMessage = [errorMessage stringByAppendingString:@"Book name is required.\n"];
        }

    if (swYearRange.on)
    {
        
                if (txtCoinName.text.length==0)
        {
            isValid = false;
            errorMessage = [errorMessage stringByAppendingString:@"With year range coin name is required.\n"];
        }
        if ([txtYearFinish.text intValue] < [txtYearStart.text intValue])
        {
            isValid = false;
            errorMessage = [errorMessage stringByAppendingString:@"Last year must be more than first.\n"];
        }
        if (txtYearFinish.text.length == 0)
        {
            isValid = false;
            errorMessage = [errorMessage stringByAppendingString:@"Last year is required.\n"];
        }
        if (txtYearStart.text.length == 0)
        {
            isValid = false;
            errorMessage = [errorMessage stringByAppendingString:@"First year is required.\n"];
        }
        if(isValid)
        {
            NSLog(@"%@", [mainDelegate.Collection CollectionId]);
            if((([txtYearFinish.text intValue] - [txtYearStart.text intValue]) + [execCol CountCollection:[[mainDelegate.Collection CollectionId] intValue]] )> COINS_MAX)
            {
                isValid = false;
                errorMessage = [errorMessage stringByAppendingFormat:@"Year range is over %d coins.\n Check if collection has cells without year or create another book.", COINS_MAX];

            }
        }
    }
    [execCol release];

    return isValid;
}


-(void)showAlert:(NSString*)topic
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:  @"Filter" preferredStyle:UIAlertControllerStyleAlert];
    
    [alert setMessage: errorMessage];
    alert.view.tag=ALERT_VALIDATE;
 
    UIAlertAction *btnCancel = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:btnCancel];
 
    [self presentViewController:alert animated:YES completion:nil];
}
/*
- (void)keyboardWasShown:(NSNotification*)aNotification {
    
    NSDictionary* info = [aNotification userInfo];
    
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    CGRect bkgndRect = activeField.superview.frame;
    
    bkgndRect.size.height += kbSize.height;
    
    [activeField.superview setFrame:bkgndRect];
    
    [scroller setContentOffset:CGPointMake(0.0, activeField.frame.origin.y-kbSize.height) animated:YES];
    
}
 */
@end
