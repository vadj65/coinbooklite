//
//  Collection.h
//  CoinBook
//
//  Created by Vadim Re on 2012-11-07.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DS.h"
#import "sqlite3.h"

@class AppDelegate;

@interface Collection : DS

{
 AppDelegate *mainDelegateCollection;
    
}
@property (nonatomic, copy) NSString *CollectionId;
@property (nonatomic, copy) NSString *Name;
@property (nonatomic, copy) NSString *Description;
@property (nonatomic, copy) NSString *YearStart;
@property (nonatomic, copy) NSString *YearFinish;
@property (nonatomic, copy) NSString *CoinName;
@property (nonatomic, copy) NSString *ItemCount;
@property (nonatomic, copy) NSDate *DateCreated;
@property (nonatomic, copy) NSString *Status;
@property (nonatomic, copy) NSString *Country;
@property (nonatomic, copy) NSString *CountryCode;
@property (nonatomic, copy) NSString *FaceValue;
@property (nonatomic, readwrite) float Weight;
@property (nonatomic, copy) NSString *Material;
@property (nonatomic, readwrite) int PreciousMetal;
@property (nonatomic, assign) BOOL isYearRange;
@property (nonatomic, assign) int isUseItemName;
@property (nonatomic, readwrite) int WeightUnit;
@property (nonatomic, copy) NSString *Currency;
@property (nonatomic, copy) NSString *CurrencyCode;

//Instance methods.
- (id) initWithPrimaryKey:(NSString*)pk;
-(void)getAllCollections:(NSString*)newSatus sortOrder:(NSString*)SortOreder CollectionList:(NSMutableArray*)result;
//-(NSMutableArray*)getAllCollections:(NSString*)newSatus sortOrder:(NSString*)SortOreder;
-(NSString*)getLastCollectionSeq;
-(void)updateCollection;
-(void)updateItemStatus:(NSString*)UpdatedCollectionId status:(NSString*)whereStatus;
-(BOOL)checkBookName:(NSString*)bookName CollectionId:(int)collectionId;
-(void)deleteCollectionYears:(NSString*)newCollectionId YearStart:(NSString*)yearStart  YearFinish:(NSString*)yearFinish;
-(void)deleteCollection:(NSString*)newCollectionId;
-(void)insertCollection;
-(int)CountCollection:(int)collectionId;

@end
