//
//  AddBookViewController.h
//  CoinBook
//
//  Created by Vadim Re on 2012-11-01.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DS.h"
#import "AppDelegate.h"
#import "MailViewController.h"
#import "Utility.h"

#import "BaseView.h"

@interface AddBookViewController : BaseView

{
    IBOutlet UIView *scrView;
    
    sqlite3 *contactDB;
    NSString        *databasePath;
    IBOutlet UILabel *lblStatus;
    IBOutlet UITextField *txtYearStart;
    IBOutlet UITextField *txtYearFinish;
    IBOutlet UITextField *txtCoinName;
  

    IBOutlet UITextField *txtFaceValue;
   
    IBOutlet UITextField *txtWeight;
    IBOutlet UISegmentedControl *swPreciousMetal;
    IBOutlet UIBarButtonItem *btnUpdateItems;
    IBOutlet UITextField *txtMaterials;
    IBOutlet UISegmentedControl *swWeightUnit;
    IBOutlet UISwitch *swYearRange;
    
    IBOutlet UISegmentedControl *sgItemName;
       //UITextField *activeField;
    
   // CountryPickerView *countryPickerView;
}

@property (strong, nonatomic) IBOutlet UITextField *txtBookName;
@property (strong, nonatomic) IBOutlet UILabel *lblStatus;
@property (strong, nonatomic) IBOutlet UITextField *txtDescription;

- (IBAction)touchBookName:(id)sender;

- (IBAction)onClickBack:(id)sender;
- (IBAction)onClickSave:(id)sender;
- (IBAction)onSwitchYearRange:(id)sender;

- (IBAction)onClickBtnSendEmail:(id)sender;
- (IBAction)onSwitchItemName:(id)sender;

- (IBAction)onClickUpdateItems:(id)sender;

- (IBAction)onChangeUseCoinName:(id)sender;
- (IBAction)onBookNameEditEnd:(id)sender;
- (IBAction)onNameValueChanged:(id)sender;

 - (IBAction)hideKeyboard:(id)sender;
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;
@end
