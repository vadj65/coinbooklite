//
//  CellLabelText.h
//  CoinBook
//
//  Created by Vadim Re on 2013-03-06.
//  Copyright (c) 2013 Vadim Re. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellLabelText : UITableViewCell
{
    
}
@property(nonatomic, retain) IBOutlet UILabel *lblLeft;
@property(nonatomic, retain) IBOutlet UITextField *txtField;
@property(nonatomic, readwrite) int cellNumber;
@property(nonatomic, retain) NSString *TextValue;
@property (nonatomic, readwrite)  int cellRow;
@property (nonatomic, readwrite)  int cellSection;

-(void)hideNumberKeyboard;

@end
