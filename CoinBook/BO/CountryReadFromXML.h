//
//  CountryReadFromXML.h
//  igQuartetto
//
//  Created by Vadim Reznikov on 11-10-18.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol NSXMLParserDelegate;

@interface CountryReadFromXML : NSObject <NSXMLParserDelegate>
{
    NSMutableArray *countries;

    
}

@property (nonatomic, retain) NSString *FileName;
@property (nonatomic, retain) NSMutableArray *Countries;

-(void)readCountries;
-(id)initWithFileName:(NSString*)fileName;

@end
