//
//  Utility.m
//  CoinBook
//
//  Created by Vadim Re on 2012-11-24.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import "Utility.h"
#import "Reachability.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>

@implementation Utility

@synthesize Response,UrlRequest;


BOOL isFinished;

-(id)init
{
    isFinished = YES;
    return  self;
}

-(void)runCommand:(NSString *)commandURL body:(NSString *)HTTPbody
{
    
   
	//NSURL *url = [NSURL URLWithString:commandURL];
	UrlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@" http://vadimreznikov.com/PrMetal.svc"]];//commandURL
	[UrlRequest setHTTPShouldHandleCookies:NO];
	if (!UrlRequest)
	{
		//NOTIFY_AND_LEAVE(@"Error creating the URL Request");
	}
	[UrlRequest setHTTPMethod: @"GET"];
	//[UrlRequest setHTTPBody: [HTTPbody dataUsingEncoding:NSUTF8StringEncoding]];
	[UrlRequest setValue:@"application/xml; charset=utf-8"	  forHTTPHeaderField:@"Content-Type"];
	
	// Place the request and wait for a response
    
    [UrlRequest addValue:@"application/xml;" forHTTPHeaderField:@"Accept"];
	NSError *error;// = nil;
	NSURLResponse *response;
	NSData *tw_result = [NSURLConnection sendSynchronousRequest:UrlRequest returningResponse:&response error:&error];
    // NSLog(@"%@", error.description);
	
	Response=[[NSString alloc] initWithData:tw_result encoding:NSUTF8StringEncoding];
    
    
    
	//[self cleanup:[[[NSString alloc] initWithData:tw_result  encoding:NSUTF8StringEncoding] autorelease]];
   	
}




-(void) connection:(NSURLConnection *) connection
didReceiveResponse:(NSURLResponse *) response {
    [webData setLength: 0];
}
-(void) connection:(NSURLConnection *) connection
    didReceiveData:(NSData *) data {
    [webData appendData:data];
    

}
-(void) connection:(NSURLConnection *) connection
  didFailWithError:(NSError *) error {
    
    [webData release];
    [connection release];
}
-(void) connectionDidFinishLoading:(NSURLConnection *) connection {
    NSLog(@"DONE. Received Bytes: %d", (int)[webData length]);
    NSString *theXML = [[NSString alloc]
                        initWithBytes: [webData mutableBytes]
                        length:[webData length]
                        encoding:NSUTF8StringEncoding];
    //---shows the XML---
  
    [self loadXMLData:theXML];
    [theXML release];
      // [activityIndicator stopAnimating];
    
    [connection release];
   [webData release];
}


#pragma mark Parse XML
-(id)loadXMLData:(NSString *)xmlData
{
    //NSLog(@"xmlData=%@", xmlData);
    
     mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([xmlData length] ==0)
    {
        //_appDelegate.userProfile.errorMessage = @"SERVER_NOT_FOUND";
    }
    else
    {
        NSXMLParser *parser = [[NSXMLParser alloc] initWithData:[xmlData dataUsingEncoding:NSUTF8StringEncoding]];
        [parser setDelegate:self];
        [parser setShouldProcessNamespaces:NO];
        [parser setShouldReportNamespacePrefixes:NO];
        [parser setShouldResolveExternalEntities:NO];
        
        [parser parse];
    }
	return self;
	
}
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
	if([elementName isEqualToString:@"errorMessage"])
	{
		//xmlStr = [[NSString alloc] init];
		//NSLog(@"Outer name tag: %@", xmlStr);
		
	}
	
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"loginResult"])
	{
        [xmlStr release];
        xmlStr = nil;
        return;
    }
	else if ([elementName isEqualToString:@"errorMessage"])
	{
		
		
		//NSLog(@"Outer name tag: %@", xmlStr);
		//_appDelegate.userProfile.errorMessage= xmlStr;
		
	}
	else	if ([elementName isEqualToString:@"silver"])
	{
       // NSLog(@"uid %@", xmlStr);
        if([xmlStr floatValue] !=0)
        {
            [mainDelegate.MyMarket setSilverOz:[xmlStr floatValue]];
            [mainDelegate.MyMarket setSilverGr:[mainDelegate.MyMarket getSilverOz]/31.10];
        }
	}
	else	if ([elementName isEqualToString:@"gold"])
	{
        // NSLog(@"uid %@", xmlStr);
        if( isFinished && [xmlStr floatValue]!= 0)
        {
            [mainDelegate.MyMarket setGoldOz:[xmlStr floatValue]];
            [mainDelegate.MyMarket setGoldGr:[mainDelegate.MyMarket getGoldOz]/31.10];
            isFinished = NO;
        }
        
	}
    else	if ([elementName isEqualToString:@"platinum"])
	{
        if([xmlStr floatValue] !=0)
        {
            [mainDelegate.MyMarket setPlatinumOz:[xmlStr floatValue]];
            [mainDelegate.MyMarket setPlatinumGr:[mainDelegate.MyMarket getGoldOz]/31.10];
            isFinished = NO;
        }
	}

	
    [xmlStr release];
    xmlStr = nil;
	
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	if (string)
	{
		xmlStr = [[NSMutableString alloc] initWithString: string];
		//NSLog(@"string: %@", xmlStr);
	}
}


#pragma mark - Preference method

+(NSString*)getPreference:(NSString*)prefKey
{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if ([prefs objectForKey:prefKey] != NULL)
    {
        return [prefs objectForKey:prefKey];
    }
    else
    {
        return @"";
    }
    
}

+(int)getPreferenceInt:(NSString*)prefKey
{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if ([prefs objectForKey:prefKey] != NULL)
    {
        return [[prefs objectForKey:prefKey] intValue];
    }
    else
    {
        return 0;
    }
    
}


+(void)setPreference:(NSString*)prefKey PrefValue:(NSString*)prefValue
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:prefValue forKey:prefKey];
}

+(void)setPreferenceInt:(NSString*)prefKey PrefValue:(int)prefValue
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setInteger:prefValue forKey: prefKey];
}


/// <#Description#>
/// @param commandURL <#commandURL description#>
/// @param HTTPbody <#HTTPbody description#>
-(void)runCommandMetal:(NSString *)commandURL body:(NSString *)HTTPbody
{
    
    
    
    
	//NSURL *url = [NSURL URLWithString:commandURL];
	_urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:commandURL]];
	[_urlRequest setHTTPShouldHandleCookies:NO];
	if (!_urlRequest)
	{
		//NOTIFY_AND_LEAVE(@"Error creating the URL Request");
	}
	[_urlRequest setHTTPMethod: @"POST"];
	[_urlRequest setHTTPBody: [HTTPbody dataUsingEncoding:NSUTF8StringEncoding]];
	[_urlRequest setValue:@"application/x-www-form-urlencoded"	  forHTTPHeaderField:@"Content-Type"];
	//NSLog(@"Contacting Twitter. This can take a minute or so...");
	// Place the request and wait for a response
	NSError *error;// = nil;
	NSURLResponse *response;
//rvv	NSData *tw_result = [NSURLConnection sendSynchronousRequest:_urlRequest returningResponse:&response error:&error];
    
	
    //rvv	_data=[[NSString alloc] initWithData:tw_result encoding:NSUTF8StringEncoding];
	//[self cleanup:[[[NSString alloc] initWithData:tw_result  encoding:NSUTF8StringEncoding] autorelease]];
    
    //replacement
    //rvv    NSURLSession *session = [NSURLSession sharedSession];
    //rvv    [[session dataTaskWithURL:[NSURL URLWithString:_urlRequest]
    //rvv              completionHandler:^(NSData *data,
    //rvv                                  NSURLResponse *response,
    //rvv                                  NSError *error) {
                // handle response

    //rvv      }] resume];
    
    //replacement
    
    
	
}

-(void)executeCommand
{
	_httpBody = [[NSString alloc] initWithString: @"username=coinInfoCollectioN&password=19641965ColLectioN&currency=usd"];
	//
	//NSLog(@"%@", _httpBody);
    [self runCommandMetal:@"http://www.nincha.ca/MetalPrice.ashx"  body:_httpBody];
	//NSString *data = [ut Data];
     //NSLog(@"data=%@", _data);
    
	[self loadXMLData:_data];
	
	
}

#pragma mark Common 

+(void)showAlert:(NSString*)topic alertTag:(int)AlertTag
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:topic message: @"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *btnContinue = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
       // [self filterSold];
    } ];
    [alert addAction:btnContinue];
   
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil ];
    [alert addAction:cancel];
    
   
    [alert release];
}
+(void)showAlertOneButton:(NSString*)topic alertTag:(int)AlertTag
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:topic message: @"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *btnContinue = [UIAlertAction actionWithTitle:@"Continue" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
       // [self filterSold];
    } ];
    [alert addAction:btnContinue];
    
   
    //?????????alert.setTa setTag:AlertTag];
 
    [alert release];
}

-(void)dealloc
{
    [conn release];
    [super dealloc];
}

-(void)menuButton:(UIButton *)button
{
    
    CGFloat nRed=133.0/255.0;
    CGFloat nBlue=133.0/255.0;
    CGFloat nGreen=133.0/255.0;
   
    
    button.backgroundColor = [UIColor  colorWithRed:nRed green:nBlue blue:nGreen alpha:1];
    button.layer.cornerRadius = 10.0;
     [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:@"button1.png"] forState:UIControlStateHighlighted];
    
}
+(UIColor*)colorGray
{
    CGFloat nRed=211.0/255.0;
    CGFloat nBlue=207.0/255.0;
    CGFloat nGreen=207.0/255.0;
    
    
    return  [UIColor  colorWithRed:nRed green:nBlue blue:nGreen alpha:1];
    
}

+(BOOL) validateEmail: (NSString *) candidate
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    //    return 0;
    return [emailTest evaluateWithObject:candidate];
}
@end
