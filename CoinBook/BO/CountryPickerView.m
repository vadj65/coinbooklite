//
//  CountryPickerView.m
//  Hex
//
//  Created by Balint F. Zoltan on 5/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CountryPickerView.h"

#import <QuartzCore/QuartzCore.h>

@interface NSString (MyCompare)

- (NSComparisonResult) myCompare: (NSString *) aString;

@end

@implementation NSString (MyCompare)

- (NSComparisonResult) myCompare: (NSString *) aString
{
    NSComparisonResult result;
    
    result = [self compare: aString];
    
    return result;
}

@end

@implementation CountryPickerView

@synthesize countries,val,selectedCountry;


- (id)initWithFrame:(CGRect)frame withDictionary:(NSMutableDictionary*)countriesDic controller:(UIViewController*)controller
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        contr = controller;
        self.countries = countriesDic;
        NSArray *values = [countries allValues];
        self.val = [NSMutableArray arrayWithArray:values];
        [self.val sortUsingSelector:@selector(myCompare:)];
        
        self.clipsToBounds = YES;        
        
        //picker init
        picker = [[UIPickerView alloc] initWithFrame:CGRectZero];
        picker.hidden = NO; 
        picker.delegate = self;
        picker.showsSelectionIndicator = YES;
        
        //Resize the picker, rotate it so that it is horizontal and set its position
        CGAffineTransform rotate = CGAffineTransformMakeRotation(0);
        rotate = CGAffineTransformScale(rotate, 1 ,1);
        CGAffineTransform t0 = CGAffineTransformMakeTranslation(-20,-15);
        picker.transform = CGAffineTransformConcat(rotate,t0);
        [self addSubview:picker];
        [picker bringSubviewToFront:self];
        [picker release]; 

    }
    return self;
}
#pragma mark Value
- (void)setValue:(NSString*)name
{
    int count = [self.val count];
    for (int i = 0; i < count; i++)
    {
        NSString *str = [self.val objectAtIndex:i];
        if ([str isEqualToString:name])
        {
            [picker selectRow:i inComponent:0 animated:YES];
            return;
        }
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
	
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component 
{	
	return [self.countries count];
}

/*
 - (UIView *)pickerView:(UIPickerView *)thePickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
 {
 
 return [self.countrieLabels objectAtIndex:row];	
 }
 */

-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return 274;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 40;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{	
    return [self.val objectAtIndex:row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSString *selected = [self.val objectAtIndex:row];
    NSArray *temp = [countries allKeysForObject:selected];
    NSString *key = [temp objectAtIndex:0];
    [contr setSelected:key selectedValue:selected];
   
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)dealloc
{
    [super dealloc];
}

@end
