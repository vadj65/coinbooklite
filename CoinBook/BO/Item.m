//
//  Item.m
//  CoinBook
//
//  Created by Vadim Re on 2012-11-09.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import "Item.h"
#import "sqlite3.h"
#import <MessageUI/MessageUI.h>
#import "DS.h"
#import "AppDelegate.h"

@implementation Item

@synthesize ItemId, Name, Description,  Year,
FaceValue, Material, Weight, Dimension, Finish, Price, Status,Country,Grade, PreciousMetal, WeightUnit,PurchaseDate, Currency;
@synthesize CurrencyCode, CountryCode, DateOut,CollectionId,ImageName;


#pragma mark-Init
-(id)init
{
   self = [super init];
    Name = [[NSString alloc]init];
    Description = [[NSString alloc]initWithString:@""];
     Material = [[NSString alloc]initWithString:@""];
    //_itemId = [[NSString alloc] init];
    ItemId= [[NSString alloc] initWithString:@""];
    Year = [[NSString alloc] init];
    Status = 0;
    Price=[[NSString alloc] initWithString:@"0.0"];;
     mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    return self;
}

- (id) initWithPrimaryKey:(NSString*) pk {
    
    self = [super init];
  
    return self;
}
-(void)dealloc
{
    [Name release];
    [ItemId release];
    [Description release];
   
    [super dealloc];
}


#pragma mark-Methods
/*
-(Item*)getItemById:(NSString*)itrmId
{
    Item *item = [[Item alloc]init];
    
    
    return item;
}
 
-(NSMutableArray*)getAllItems
{
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    return items;
}*/


-(void)getItemByItemId:(NSString*)parItemId
{
   
    mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    const char *sql = [[NSString  stringWithFormat:@"select item_id, name , ifnull( year, '') year, description, ifnull( status, '0'), ifnull( price, '0') price, ifnull( face_value, '0') face_value , ifnull(grade, 0) grade , ifnull( precious_metal, ''), ifnull( weight, '0'), ifnull( material, '0'),weight_unit, ifnull( purchase_date, ''), ifnull( items.country_code, ''), ifnull(items.currency_code, ''), ifnull(country.country,''), ifnull(currency.currency, '') , ifnull( status_changed_date, '')  from items left outer join country on items.country_code = country.country_code left outer join currency on items.currency_code = currency.currency_code  where item_id = %@ " , parItemId] UTF8String];
  
    const char *dbpath = [ mainDelegate.DataBase.DatabasePath UTF8String];
    //sqlite3_stmt    *statement;
    
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
        // const char *query_stmt = [selectSQL UTF8String];
        
        if (sqlite3_prepare_v2(_contactDB, sql, -1, &_statement, NULL) == SQLITE_OK)
        {
            //NSString *name;
            while (sqlite3_step(_statement) == SQLITE_ROW)
            {
                [self setItemId:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 0)]];
                [self setName: [NSString stringWithUTF8String: (const char *) sqlite3_column_text(_statement, 1)]];
                
                [self setDescription:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 3)]];
                if(sqlite3_column_text(_statement, 2) != NULL)
                {
                    [self setYear:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 2)]];
                }
                [self setStatus:[[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 4)] intValue]];
                [self setPrice:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 5)]];
                [self setFaceValue:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 6)]];
                [self setGrade:[[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 7)] intValue]];
                [self setPreciousMetal:[[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 8) ]intValue]];
                [self setWeight:[[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 9)] floatValue]];
                [self setMaterial:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 10)]];
                [self setWeightUnit:[[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 11)] intValue]];
                [self setPurchaseDate:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 12)]];
                [self setCountryCode:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 13)]];
                [self setCurrencyCode:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 14)]];
                [self setCountry:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 15)]];
                if(sqlite3_column_text(_statement, 16) != NULL)
                {
                    [self setCurrency:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 16)]];
                }
                [self setDateOut:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 17)]];
                
                [mainDelegate.DataBase setSqlStatus: @"1"];
                
                          }
            
            sqlite3_finalize(_statement);
        }
        sqlite3_close(_contactDB);
    }
 
}



-(void)getAllItemsByCollectionId:(NSString*)filter order:(NSString*)NewOrder ItemList:(NSMutableArray*)items
{
   // NSMutableArray *items = [[NSMutableArray alloc] init];
    NSString *where = [[NSString alloc] initWithFormat:@" and  status like \'%@\'", filter.length == 0?@"%": filter ];
    mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    const char *sql = [[NSString  stringWithFormat:@"select items.item_id, name , ifnull( year, '') year, ifnull(description ,''), ifnull( status, '0'), ifnull( price, '0') price, ifnull( face_value, '0') face_value , ifnull(grade, 0) grade , precious_metal, ifnull( weight, '0'), ifnull( material, ''), ifnull(weight_unit, 0), ifnull(items.country_code, '') , ifnull(purchase_date, ''), ifnull(items.currency_code , ''), ifnull(country.country, ''), ifnull(currency.currency, ''), ifnull( status_changed_date, '') from items   left outer join country on items.country_code = country.country_code left outer join currency on items.currency_code = currency.currency_code where items.collection_id = %@ %@ order by %@" , [mainDelegate.Collection CollectionId], where , NewOrder] UTF8String];
   
    const char *dbpath = [ _databasePath UTF8String];
    //sqlite3_stmt    *statement;
    Item *itemObj = nil;//[[Item alloc]init];
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
        // const char *query_stmt = [selectSQL UTF8String];
        
        if (sqlite3_prepare_v2(_contactDB, sql, -1, &_statement, NULL) == SQLITE_OK)
        {
           //NSString *name;
            while (sqlite3_step(_statement) == SQLITE_ROW)
            {
             // NSString  *pk = [[NSString alloc] initWithFormat:@"%s",(const char *)sqlite3_column_text(_statement, 0) ] ;
                //[[NSString alloc] initWithUTF8String:
                
                 itemObj = [[Item alloc] initWithPrimaryKey:[NSString stringWithFormat:@"%s",(const char *)sqlite3_column_text(_statement, 0) ]  ];
                
                
                ///name = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(_statement, 1)] ;
                [itemObj setName: [NSString stringWithUTF8String: (const char *) sqlite3_column_text(_statement, 1)]];
                [itemObj setItemId:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 0)]];
                [itemObj setDescription:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 3)]];
                if(sqlite3_column_text(_statement, 2) != NULL)
                {
                    [itemObj setYear:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 2)]];
                }
                [itemObj setStatus:[[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 4)] intValue]];
                [itemObj setPrice:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 5)]];
                [itemObj setFaceValue:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 6)]];
                [itemObj setGrade:[[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 7)] intValue]];
                [itemObj setPreciousMetal:[[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 8) ]intValue]];
                [itemObj setWeight:[[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 9)] floatValue]];
                [itemObj setMaterial:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 10)]];
                [itemObj setWeightUnit:[[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 11)] intValue]];
                [itemObj setCountryCode:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 12)]];
                [itemObj setPurchaseDate:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 13)]];
                [itemObj setCurrencyCode:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 14)]];
                [itemObj setCurrency:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 16)]];
                [itemObj setCountry:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 15)]];
                [itemObj setDateOut:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 17)]];
                
                
                [mainDelegate.DataBase setSqlStatus: @"1"];
              
                [items addObject:itemObj];
                [itemObj release];
               // [name release];
            }
            
            sqlite3_finalize(_statement);
        }
        sqlite3_close(_contactDB);
    }
    [where release];
    //[itemObj release];
   // return items ;
}

-(void)getAllItems:(NSString*)filter sort:(NSString*)Sort ItemList:(NSMutableArray*)items
{
   // NSMutableArray *items = [[NSMutableArray alloc] init];
    NSString *sortClause  =@" year Asc";
    if (Sort.length > 0)
    {
        sortClause = Sort;
    }
    NSString *where = [[NSString alloc] initWithFormat:@" where  status like \'%@\'", filter.length == 0?@"%": filter ];
    mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    const char *sql = [[NSString  stringWithFormat:@"select item_id, name , ifnull( year, '') year, status from items  %@ order by %@" ,  where, sortClause ] UTF8String];
   
    const char *dbpath = [ mainDelegate.DataBase.DatabasePath UTF8String];
    //sqlite3_stmt    *statement;
    
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
        // const char *query_stmt = [selectSQL UTF8String];
        
        if (sqlite3_prepare_v2(_contactDB, sql, -1, &_statement, NULL) == SQLITE_OK)
        {
            //NSString *name;
            while (sqlite3_step(_statement) == SQLITE_ROW)
            {
                // NSString  *pk = [[NSString alloc] initWithFormat:@"%s",(const char *)sqlite3_column_text(_statement, 0) ] ;
                //[[NSString alloc] initWithUTF8String:
                
                Item *itemObj = [[Item alloc] initWithPrimaryKey:[NSString stringWithFormat:@"%s",(const char *)sqlite3_column_text(_statement, 0) ]  ];
                
                
                ///name = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(_statement, 1)] ;
                [itemObj setName: [NSString stringWithUTF8String: (const char *) sqlite3_column_text(_statement, 1)]];
                [itemObj setItemId:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 0)]];
                
                [itemObj setYear:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 2)]];
                [itemObj setStatus:[[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 3)] intValue]];
                 
                [mainDelegate.DataBase setSqlStatus: @"1"];
               
                [items addObject:itemObj];
                [itemObj release];
                // [name release];
            }
            
            sqlite3_finalize(_statement);
        }
        sqlite3_close(_contactDB);
    }
    [where release];
    [sortClause release];
    
   // return items ;
}

-(void)insertItem
{
        
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    //uncomment to get the time only
    //[formatter setDateFormat:@"hh:mm a"];
    //[formatter setDateFormat:@"MMM dd, YYYY"];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    NSString *dateToday = [formatter stringFromDate:[NSDate date]];
    NSString *SQLStatement = @"INSERT INTO items (name , description , year, face_value, country_code, status, date_created, grade, precious_metal, weight, price,weight_unit, material, purchase_date, currency_code, status_changed_date, collection_id) VALUES (?, ?, ?, ?, ?, ?, ?, 0, ?, ?, ? ,?, ?,?,?,?, ?)" ;
                      //Name, Description, Year, FaceValue, CountryCode, Status, dateToday, PreciousMetal, Weight, Price, WeightUnit, Material, PurchaseDate, CurrencyCode] ];
    
    
    _dirPaths = NSSearchPathForDirectoriesInDomains(  NSDocumentDirectory, NSUserDomainMask, YES);
  
    
    
    // NSLog(@"%@",updatedSQL );
    _docsDir = [_dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    //_databasePath = [_databasePath UTF8String];
    
    //NSFileManager *filemgr = [NSFileManager defaultManager];
    
    const char *dbpath = [_databasePath UTF8String];
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
        
        
        
        //get the date today
        
        
        const char *insert_stmt = [SQLStatement UTF8String];
  
        sqlite3_prepare_v2(_contactDB, insert_stmt, -1, &_statement,NULL);
        sqlite3_bind_text(_statement, 1, [Name UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(_statement, 2, [Description UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(_statement, 3, [Year intValue]);
        sqlite3_bind_text(_statement, 4, [FaceValue UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(_statement, 5, [CountryCode UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(_statement, 6, Status);
        sqlite3_bind_text(_statement, 7, [dateToday UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(_statement, 8, PreciousMetal);
        sqlite3_bind_double(_statement, 9, Weight );
       
        sqlite3_bind_double(_statement, 10, [Price doubleValue]);
       
        sqlite3_bind_int(_statement, 11, WeightUnit);
        sqlite3_bind_text(_statement, 12, [Material UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(_statement, 13, [PurchaseDate UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(_statement, 14, [CurrencyCode UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(_statement, 15, [DateOut UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(_statement, 16, CollectionId);
        
       
       
        
        int result =sqlite3_step(_statement);
      
        if ( result== SQLITE_DONE)
        {
            
            [mainDelegate.DataBase setSqlStatus: @"1"];
            
        }
        else
        {
            [mainDelegate.DataBase setSqlStatus: @"0"];
        }
        
        sqlite3_finalize(_statement);
        sqlite3_close(_contactDB);
        
    }
  //  [filemgr release];
    [formatter release];
    if ([[mainDelegate.DataBase sqlStatus] isEqualToString:@"1"])
    {
  
        [mainDelegate.Item setItemId:[self getItemSeq]];
        [self commandInsertCollectionItem];
    }
}

-(NSString*)getItemSeq
{
    mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    //SELECT seq FROM sqlite_sequence where name='Collection'
    
    const char *dbpath = [ _databasePath UTF8String];
   // NSString *sqlStatement = [[NSString alloc]initWithFormat:@"select collection_id, name from collections" ];
    NSString *ret =@"";
    //_databasePath = [[NSString alloc] initWithString: [_docsDir stringByAppendingPathComponent: @"CoinBook.sqlite"]];
    
    //NSFileManager *filemgr = [NSFileManager defaultManager];
    const char *insert_stmt = [@"SELECT seq FROM sqlite_sequence where name='Items'"  UTF8String];
    
    
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
        
        
        if(sqlite3_prepare_v2(_contactDB, insert_stmt, -1, &_statement, NULL)== SQLITE_OK)
        {
            if (sqlite3_step(_statement) == SQLITE_ROW)
            {
                if(sqlite3_column_text(_statement, 0) != NULL)
                {
                    ret = [NSString stringWithUTF8String: (const char *) sqlite3_column_text(_statement, 0)];
                    [mainDelegate.DataBase setSqlStatus: @"1"];
                }
            }
            else
            {
                [mainDelegate.DataBase setSqlStatus: @"0"];
            }
        }
        sqlite3_finalize(_statement);
        sqlite3_close(_contactDB);
        
    }
    else
    {
        [mainDelegate.DataBase setSqlStatus: @"0"];
    }
    //[sqlStatement release];
    //[filemgr release];
    return ret;
    
}

-(NSString*)getItemImagePath:(NSString*)itemIdImage
{
    mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    //SELECT seq FROM sqlite_sequence where name='Collection'
    
    const char *dbpath = [ _databasePath UTF8String];
    // NSString *sqlStatement = [[NSString alloc]initWithFormat:@"select collection_id, name from collections" ];
    NSString *ret =@"";
    //_databasePath = [[NSString alloc] initWithString: [_docsDir stringByAppendingPathComponent: @"CoinBook.sqlite"]];
    
    //NSFileManager *filemgr = [NSFileManager defaultManager];
    const char *insert_stmt = [[NSString stringWithFormat:@"SELECT image_name FROM items where item_id = %@", itemIdImage]  UTF8String];
    
    
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
        
        
        if(sqlite3_prepare_v2(_contactDB, insert_stmt, -1, &_statement, NULL)== SQLITE_OK)
        {
            if (sqlite3_step(_statement) == SQLITE_ROW)
            {
                if(sqlite3_column_text(_statement, 0) != NULL)
                {
                    ret = [NSString stringWithUTF8String: (const char *) sqlite3_column_text(_statement, 0)];
                    [mainDelegate.DataBase setSqlStatus: @"1"];
                }
            }
            else
            {
                [mainDelegate.DataBase setSqlStatus: @"0"];
            }
        }
        sqlite3_finalize(_statement);
        sqlite3_close(_contactDB);
        
    }
    else
    {
        [mainDelegate.DataBase setSqlStatus: @"0"];
    }
    //[sqlStatement release];
    //[filemgr release];
    return ret;
    
}


-(NSString*)itemYearExist:(int)checkYear

{
    mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    const char *dbpath = [ mainDelegate.DataBase.DatabasePath UTF8String];
    NSString *ret =@"";
   
  //  NSFileManager *filemgr = [NSFileManager defaultManager];
    const char *insert_stmt = [[NSString stringWithFormat:@"SELECT count(*) FROM items  where items.collection_id = %@ and items.year = %d",[mainDelegate.Collection CollectionId], checkYear]  UTF8String];
    
    
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
        
        
        if(sqlite3_prepare_v2(_contactDB, insert_stmt, -1, &_statement, NULL)== SQLITE_OK)
        {
            if (sqlite3_step(_statement) == SQLITE_ROW)
            {
                if(sqlite3_column_text(_statement, 0) != NULL)
                {
                    ret = [NSString stringWithUTF8String: (const char *) sqlite3_column_text(_statement, 0)];
                    [mainDelegate.DataBase setSqlStatus: @"1"];
                }
            }
            else
            {
                [mainDelegate.DataBase setSqlStatus: @"0"];
            }
        }
        sqlite3_finalize(_statement);
        sqlite3_close(_contactDB);
        
    }
    else
    {
        [mainDelegate.DataBase setSqlStatus: @"0"];
    }
     //  [filemgr release];
    return ret;
    
}


-(void)commandInsertCollectionItem
{
   // NSString *_docsDir = [[NSString alloc] init];
    AppDelegate *mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    [mainDelegate.DataBase setDirPath: NSSearchPathForDirectoriesInDomains(  NSDocumentDirectory, NSUserDomainMask, YES)];
    
   // _docsDir = [mainDelegate.DataBase.DirPath objectAtIndex:0];
    
    
   NSString *sqlStatement = [NSString stringWithFormat:@" insert into collection_item  (collection_id, item_id) values (\"%@\", (SELECT seq FROM sqlite_sequence where name='Items'))", [mainDelegate.Collection CollectionId]];
    
    
    // Build the path to the database file
   // [mainDelegate.DataBase setDatabasePath:[NSString stringWithString: [[mainDelegate.DataBase.DirPath objectAtIndex:0] stringByAppendingPathComponent: @"CoinBook.sqlite"]]];
    
   
    
    const char *dbpath = [_databasePath UTF8String];
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
        
        
       
        
        const char *insert_stmt = [sqlStatement UTF8String];
        
        sqlite3_prepare_v2(_contactDB, insert_stmt, -1, &_statement, NULL);
        if (sqlite3_step(_statement) == SQLITE_DONE)
        {
            [mainDelegate.DataBase setSqlStatus:@"1"];
        }
    }
    else
    {
        [mainDelegate.DataBase setSqlStatus: @"0"];
    }
    
    sqlite3_finalize(_statement);
    sqlite3_close(_contactDB);
    
    
    //[filemgr release];

}
-(void)clearItem
{
    [self setItemId:@""];
    [self setName:@"" ];
    [self setDescription:@""];
    [self setYear:@""];
    [self setFaceValue:@""];
    [self setCountry:@"" ];
    [self setPreciousMetal:0];
    [self setWeight:0.0];
    [self setMaterial:@""];
    [self setWeightUnit:0];
    [self setCurrency:@""];
    [self setCountryCode:@"" ];
    [self setCurrencyCode:@"" ];
    [self setPrice:@""];
    [self setPurchaseDate:@"" ];
    [self setDateOut:@"" ];
    
    
}

#pragma mark Delete Item
-(void)deleteItem
{
    
    ImageName = [self getItemImagePath:ItemId];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:ImageName])
    {
        NSError *error;
        if (![fileManager removeItemAtPath:ImageName error:&error])
        {
            //NSLog(@"Error removing file: %@", error);
        };
    }

    NSString *sqlStatement = [NSString stringWithFormat:@" delete from items where item_id ='\%@\'", ItemId];
   
    [mainDelegate.DataBase  commandSQL:sqlStatement];
    sqlStatement = [[NSString alloc] initWithFormat:@" delete from collection_item where item_id ='\%@\'", ItemId];
   
    [mainDelegate.DataBase  commandSQL:sqlStatement];

    
   

    [sqlStatement release];

}

#pragma mark Update

-(void)updateItem
{
    /*
      NSString *sqlStatement = [[NSString alloc] initWithFormat:@" Update items set name= '\%@\' , description ='\%@\' ,year= '\%@\' ,face_value= '\%@\',status= %d,grade= %d , country_code = '%@' ,price= '%@',weight= '%f', weight_unit = %d , purchase_date = '%@', currency_code = '%@', material = '%@' where item_id ='\%@\'", Name, Description, Year, FaceValue, Status,Grade, CountryCode,  Price,Weight, WeightUnit, PurchaseDate,CurrencyCode, Material,ItemId];
     */
    
    NSString *SQLStatement = @" Update items set name= ? , description =? ,year= ? ,face_value= ? ,status= ?,grade= ? , country_code = ? ,price= ?,weight= ?, weight_unit =? , purchase_date = ?, currency_code = ?, material = ? , precious_metal = ? , status_changed_date = ? where item_id =?";
    // , Name, Description, Year, FaceValue, Status,Grade, CountryCode,  Price,Weight, WeightUnit, PurchaseDate,CurrencyCode, Material,ItemId];
                               
                              _dirPaths = NSSearchPathForDirectoriesInDomains(  NSDocumentDirectory, NSUserDomainMask, YES);
    
                              
                              
                              // NSLog(@"%@",updatedSQL );
                              _docsDir = [_dirPaths objectAtIndex:0];
                              
                              // Build the path to the database file
                              _databasePath = [[NSString alloc] initWithString: [_docsDir stringByAppendingPathComponent: @"CoinBook.sqlite"]];
                              
                             // NSFileManager *filemgr = [NSFileManager defaultManager];
                              
                              const char *dbpath = [_databasePath UTF8String];
                              if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
                              {
                                  
                                  
                                  
                                  //get the date today
                                  
                                  
                                  const char *insert_stmt = [SQLStatement UTF8String];
                                
                                  sqlite3_prepare_v2(_contactDB, insert_stmt, -1, &_statement,NULL);
                                  sqlite3_bind_text(_statement, 1, [Name UTF8String], -1, SQLITE_TRANSIENT);
                                  sqlite3_bind_text(_statement, 2, [Description UTF8String], -1, SQLITE_TRANSIENT);
                                  sqlite3_bind_int(_statement, 3, [Year intValue]);
                                  sqlite3_bind_text(_statement, 4, [FaceValue UTF8String], -1, SQLITE_TRANSIENT);
                                  sqlite3_bind_int(_statement, 5, Status);
                                  sqlite3_bind_int(_statement, 6, Grade);
                                  sqlite3_bind_text(_statement, 7, [CountryCode UTF8String], -1, SQLITE_TRANSIENT);
                                  sqlite3_bind_double(_statement, 8, [Price doubleValue]);
                                  sqlite3_bind_double(_statement, 9, Weight );
                                  sqlite3_bind_int(_statement, 10, WeightUnit);
                                  sqlite3_bind_text(_statement, 11, [PurchaseDate UTF8String], -1, SQLITE_TRANSIENT);
                                  sqlite3_bind_text(_statement, 12, [CurrencyCode UTF8String], -1, SQLITE_TRANSIENT);
                                  sqlite3_bind_text(_statement, 13, [Material UTF8String], -1, SQLITE_TRANSIENT);
                             /*   */
                                   sqlite3_bind_text(_statement, 16, [ItemId UTF8String], -1, SQLITE_TRANSIENT);
                                  sqlite3_bind_int(_statement, 14, PreciousMetal);
                                  sqlite3_bind_text(_statement, 15, [DateOut UTF8String], -1, SQLITE_TRANSIENT);

                                  
                                  int result =sqlite3_step(_statement);
                                  //NSLog(@"result=%d", result);
                                  if ( result== SQLITE_DONE)
                                  {
                                      
                                      [mainDelegate.DataBase setSqlStatus: @"1"];
                                      
                                  }
                                  else
                                  {
                                      [mainDelegate.DataBase setSqlStatus: @"0"];
                                  }
                                  
                                  sqlite3_finalize(_statement);
                                  sqlite3_close(_contactDB);
                                  
                              }
                              //[filemgr release];

                              
    
}
// = case when coalesce( material, '') = '' then  'f2'  else material  end
-(void)updateItemByCollectionId:(NSString*)collectionId
{
    
    NSString *SQLStatement = [[NSString alloc] initWithFormat:@" Update items set name = case when coalesce( name, '') = '' then  ?  else name  end , description  = case when coalesce( description, '') = '' then  ?  else description  end  ,face_value = case when coalesce( face_value, '') = '' then  ?  else face_value  end ,country_code = case when coalesce( country_code, '') = '' then  ?  else country_code  end ,   weight = case when coalesce( weight, '') = ''or  weight = '0.0'  then  ?  else weight  end , weight_unit = case when coalesce( weight_unit, '') = '' then  ?  else weight_unit  end , currency_code  = case when coalesce( currency_code, '') = '' then  ?  else currency_code  end  , material = case when coalesce( material, '') = '' then  ?  else material  end  where  collection_id =?" ];
                                                               //], Name, Description,  FaceValue, CountryCode,  Weight,  WeightUnit, CurrencyCode, Material, collectionId];
    
    _dirPaths = NSSearchPathForDirectoriesInDomains(  NSDocumentDirectory, NSUserDomainMask, YES);
       
    
    // NSLog(@"%@",updatedSQL );
    _docsDir = [_dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    _databasePath = [[NSString alloc] initWithString: [_docsDir stringByAppendingPathComponent: @"CoinBook.sqlite"]];
    
   // NSFileManager *filemgr = [NSFileManager defaultManager];
    
    const char *dbpath = [_databasePath UTF8String];
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
        
        
        
        //get the date today
        
        
        const char *insert_stmt = [SQLStatement UTF8String];
      
        sqlite3_prepare_v2(_contactDB, insert_stmt, -1, &_statement,NULL);
        sqlite3_bind_text(_statement, 1, [Name UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(_statement, 2, [Description UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(_statement, 3, [FaceValue UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(_statement, 4, [CountryCode UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_double(_statement, 5, Weight );
        sqlite3_bind_int(_statement, 6, WeightUnit);
        sqlite3_bind_text(_statement, 7, [CurrencyCode UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(_statement, 8, [Material UTF8String], -1, SQLITE_TRANSIENT);
        /*   */
        sqlite3_bind_text(_statement, 9, [collectionId UTF8String], -1, SQLITE_TRANSIENT);
        
        int result =sqlite3_step(_statement);
        //NSLog(@"result=%d", result);
        if ( result== SQLITE_DONE)
        {
            
            [mainDelegate.DataBase setSqlStatus: @"1"];
            
        }
        else
        {
            [mainDelegate.DataBase setSqlStatus: @"0"];
        }
        
        sqlite3_finalize(_statement);
        sqlite3_close(_contactDB);
        
    }
    //[filemgr release];
    
    [SQLStatement release];
}

-(void)updateForceItemByCollectionId:(NSString*)collectionId
{
    
    NSString *sqlStatement = [[NSString alloc] initWithFormat:@" Update items set name= '\%@\' , description ='\%@\' ,face_value= '\%@\',country_code= '\%@\',  weight = %f , weight_unit = %d , currency_code = '%@' , material = '%@' where collection_id =%@", Name, Description,  FaceValue, CountryCode,Weight,  WeightUnit, CurrencyCode, Material, collectionId];
      _dirPaths = NSSearchPathForDirectoriesInDomains(  NSDocumentDirectory, NSUserDomainMask, YES);
  
    
    
    // NSLog(@"%@",updatedSQL );
    _docsDir = [_dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    _databasePath = [[NSString alloc] initWithString: [_docsDir stringByAppendingPathComponent: @"CoinBook.sqlite"]];
    
   // NSFileManager *filemgr = [NSFileManager defaultManager];
    
    const char *dbpath = [_databasePath UTF8String];
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
        
        
        
        //get the date today
        
        
        const char *insert_stmt = [sqlStatement UTF8String];
       
        sqlite3_prepare_v2(_contactDB, insert_stmt, -1, &_statement,NULL);
        sqlite3_bind_text(_statement, 1, [Name UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(_statement, 2, [Description UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(_statement, 3, [FaceValue UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(_statement, 4, [CountryCode UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_double(_statement, 5, Weight );
        sqlite3_bind_int(_statement, 6, WeightUnit);
        sqlite3_bind_text(_statement, 7, [CurrencyCode UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(_statement, 8, [Material UTF8String], -1, SQLITE_TRANSIENT);
        /*   */
        sqlite3_bind_text(_statement, 9, [collectionId UTF8String], -1, SQLITE_TRANSIENT);
        
        int result =sqlite3_step(_statement);
        //NSLog(@"result=%d", result);
        if ( result== SQLITE_DONE)
        {
            
            [mainDelegate.DataBase setSqlStatus: @"1"];
            
        }
        else
        {
            [mainDelegate.DataBase setSqlStatus: @"0"];
        }
        
        sqlite3_finalize(_statement);
        sqlite3_close(_contactDB);
        
    }
    //[filemgr release];

    [sqlStatement release];
}



-(void)updateItemStatus:(NSString*)UpdatedItemId status:(NSString*)whereStatus
{
    
    NSString *sqlStatement = [[NSString alloc] initWithFormat:@" Update items set status= '\%@\'  where item_id ='\%@\'", whereStatus ,UpdatedItemId ];
    //NSLog(@"%@", sqlStatement);
    [mainDelegate.DataBase commandSQL:sqlStatement];
    [sqlStatement release];
    
}
-(void)updateItemImagePath:(NSString*)UpdatedItemId ImagePath:(NSString*)imagePath
{
    
    NSString *sqlStatement = [[NSString alloc] initWithFormat:@" Update items set image_name= '\%@\'  where item_id ='\%@\'", imagePath ,UpdatedItemId ];
    //NSLog(@"%@", sqlStatement);
    [mainDelegate.DataBase commandSQL:sqlStatement];
    [sqlStatement release];
    
}

-(void)createCVS:(NSString*)yourFileName PriceIncluded:(BOOL)priceIncluded
{
    NSMutableArray *collection = [[NSMutableArray alloc]init ];
     [self   getAllItemsByCollectionId:mainDelegate.whereField order:@" year " ItemList:collection];
    
    NSMutableString *csv;
    if (priceIncluded)
    {
        csv = [NSMutableString stringWithString:@"Name,Date,PurchaseDate,Price,FaceValue,Description,Status,StatusCode,Grade,Country,CountryCode,Currency,CurrencyCode,Metal,MetalCode,Weight,WeightUnit,Unit,Material"];
    }
    else
    {
        csv = [NSMutableString stringWithString:@"Name,Date,PurchaseDate,FaceValue,Description,Status,StatusCode,Grade,Country,CountryCode,Currency,CurrencyCode,Metal,MetalCode,Weight,WeightUnit,Unit,Material"];
    }
    NSString *status = @"";
    NSUInteger count = [collection count];
    NSString *metal=@"";
     NSString *unit=@"";
  //  NSString *weightUnit=[[NSString alloc] init];

    // provided all arrays are of the same length
    for (NSUInteger i=0; i<count; i++ )
    {
        switch ([(Item*)[collection objectAtIndex:i] Status]) {
            case 0:
                status= @"New";
                break;
            case 1:
                status= @"Added";
                break;
            case 2:
                status= @"Sold";
                break;
            case 3:
                status= @"Traded";
                break;
                
            default:
                break;
        }
        
        
        switch ([(Item*)[collection objectAtIndex:i] PreciousMetal])
        {
            case 0:
                metal= @"None";
                break;
            case 1:
                metal= @"Silver";
                break;
            case 2:
                metal= @"Gold";
                break;
            case 3:
                metal= @"Platinum";
                break;
        }
        switch ([(Item*)[collection objectAtIndex:i] PreciousMetal])
        {
            case 0:
                metal= @"None";
                break;
            case 1:
                metal= @"Silver";
                break;
            case 2:
                metal= @"Gold";
                break;
            case 3:
                metal= @"Platinum";
                break;
        }
        switch ([(Item*)[collection objectAtIndex:i] WeightUnit])
        {
            case 0:
                unit= @"Gramms";
                break;
            case 1:
                unit= @"Ounce";
                break;
            case 100:
                unit= @"0- Gramms 1 - Ounce";
                break;
        }

        if(!priceIncluded)
        {

        [csv appendFormat:@"\n\"%@\",%@,\"%@\",\"%@\",\"%@\",\"%@\",\"%d\",\"%d\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%d\",\"%f\",\"%d\",\"%@\",\"%@\"",
         [[collection objectAtIndex:i] Name] ,
         [ (Item*)[collection objectAtIndex:i] Year],
          [ (Item*)[collection objectAtIndex:i] PurchaseDate],
         [(Item*)[collection objectAtIndex:i] FaceValue],
         [(Item*)[collection objectAtIndex:i] Description],
                 status,
         [(Item*)[collection objectAtIndex:i] Status]
         ,[(Item*)[collection objectAtIndex:i] Grade]+1
        ,[(Item*)[collection objectAtIndex:i] Country]
         ,[(Item*)[collection objectAtIndex:i] CountryCode]
         ,[(Item*)[collection objectAtIndex:i] Currency]
         ,[(Item*)[collection objectAtIndex:i] CurrencyCode],
         metal,[(Item*)[collection objectAtIndex:i] PreciousMetal] 
         ,[(Item*)[collection objectAtIndex:i] Weight]
         ,[(Item*)[collection objectAtIndex:i] WeightUnit],unit
         ,[(Item*)[collection objectAtIndex:i] Material]
         ];
        }
        else
        {
            [csv appendFormat:@"\n\"%@\",%@,\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%d\",\"%d\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%d\",\"%f\",\"%d\",\"%@\",\"%@\"",
             [[collection objectAtIndex:i] Name] ,
             [ (Item*)[collection objectAtIndex:i] Year],
             [ (Item*)[collection objectAtIndex:i] PurchaseDate],
             [(Item*)[collection objectAtIndex:i] Price],
             [(Item*)[collection objectAtIndex:i] FaceValue],
             [(Item*)[collection objectAtIndex:i] Description],
             status,
             [(Item*)[collection objectAtIndex:i] Status]
                         ,[(Item*)[collection objectAtIndex:i] Grade]+1
             ,[(Item*)[collection objectAtIndex:i] Country]
             ,[(Item*)[collection objectAtIndex:i] CountryCode]
             ,[(Item*)[collection objectAtIndex:i] Currency]
             ,[(Item*)[collection objectAtIndex:i] CurrencyCode],
             metal,[(Item*)[collection objectAtIndex:i] PreciousMetal]
             ,[(Item*)[collection objectAtIndex:i] Weight]
             ,[(Item*)[collection objectAtIndex:i] WeightUnit],unit
             ,[(Item*)[collection objectAtIndex:i] Material]
             ];

        }
        // instead of integerValue may be used intValue or other, it depends how array was created
    }
    
    [status release];
   // NSString *yourFileName = @"coinCollection.cvs";
    NSError *error;
    
    
    //create instance of NSFileManager
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    //create an array and store result of our search for the documents directory in itNSDocumentDirectory
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    
    //create NSString object, that holds our exact path to the documents directory
    NSString *documentsDirectory = [paths objectAtIndex:0];
  
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.csv", yourFileName]]; //add our file to the path
  
    [fileManager createFileAtPath:fullPath contents:[csv dataUsingEncoding:NSUTF8StringEncoding] attributes:nil]; //finally save the path (file)
    
    NSData *mainBundleFile = [NSData dataWithContentsOfFile:csv ];
    [[NSFileManager defaultManager] createFileAtPath:fullPath contents:mainBundleFile attributes:nil];
  
    
    BOOL res = [csv writeToFile:fullPath atomically:YES encoding:NSUTF8StringEncoding error:&error];
    
    if (!res)
    {
        //NSLog(@"Error %@ while writing to file %@", [error localizedDescription], yourFileName );
    }
   
    //[error release];
    [collection release];
    
}


-(void)createCVSTemplate:(NSString*)yourFileName
{
    NSMutableArray *collection = [[NSMutableArray alloc]init ];
    
    Item *one = [[Item alloc]init];
    
    one.Name = @"Silver Coin";
    one.Year = @"1918";
    one.PurchaseDate =@"2013-06-25";
    one.Price=@"23.56";
    one.FaceValue=@"5";
    one.Description = @"Siver coin description";
    one.Status = 1;
    one.Grade =2;
    one.Country= @"UNITED STATES";
    one.CountryCode=@"US";
    one.Currency= @"US Dollar";
    one.CurrencyCode = @"USD";
    one.Material=@"Silver";
    one.Weight = 1.0;
    one.WeightUnit = 2;
    one.Material = @"Silevr 90% cooper 10%";
    
    [collection addObject:one];
    
    
    Item *two = [[Item alloc]init];
    
    two.Name = @"10 frank";
    two.Year = @"1989";
    two.PurchaseDate =@"2010-11-05";
    two.Price=@"23.56";
    two.FaceValue=@"5";
    two.Description = @"France Liberty";
    two.Status = 1;
    two.Grade =2;
    two.Country= @"France";
    two.CountryCode=@"FR";
    two.Currency= @"US Dollar";
    two.CurrencyCode = @"USD";
    two.Material=@"Silver";
    two.Weight = 1.0;
    two.WeightUnit = 2;
    two.Material = @"Silevr 90% cooper 10%";
    
    [collection addObject:two];
    
        
    Item *four = [[Item alloc]init];
    
    four.Name = @"";
    four.Year = @"";
    four.PurchaseDate =@"";
    four.Price=@"";
    four.FaceValue=@"";
   
    four.Description = @"";
    four.Status = 101;
    four.Grade =101;
    four.Country= @"";
    four.CountryCode=@"";
    four.Currency= @"";
    four.CurrencyCode = @"";
    four.PreciousMetal =101;
    four.Material=@"";
    // one.Weight = 1.0;
    four.WeightUnit = 101;
    four.Material = @"";
    [collection addObject:four];
    
    four = [[Item alloc]init];
    
    four.Name = @"This row just for information. When cvs file is ready to import it has to be deleted:";
    four.Year = @"";
    four.PurchaseDate =@"";
    four.Price=@"";
    four.FaceValue=@"";
    
    four.Description = @"";
    four.Status = 100;
    four.Grade =100;
    four.Country= @"";
    four.CountryCode=@"";
    four.Currency= @"";
    four.CurrencyCode = @"";
    four.PreciousMetal=100;
    // one.Weight = 1.0;
     four.WeightUnit = 100;
    four.Material = @"";
    [collection addObject:four];


    NSMutableString *csv;
    
        csv = [NSMutableString stringWithString:@"Name,Mint,PurchaseDate,Price,FaceValue,Description,Status,StatusCode,Grade,Country,CountryCode,Currency,CurrencyCode,Metal,MetalCode,Weight,WeightUnit,Unit,Material"];
    int statusId=0;
    NSString *status = @"";
    NSUInteger count = [collection count];
    NSString *metal=@"";
     NSString *unit=@"";
    NSString *grade = @"Grade is int from 0-4. Shown like 0-4 + 1.";
    //  NSString *weightUnit=[[NSString alloc] init];
    
    // provided all arrays are of the same length
    
    for (NSUInteger i=0; i<count; i++ )
    {
        switch ([(Item*)[collection objectAtIndex:i] Status]) {
            case 0:
                status= @"New";
                break;
            case 1:
                status= @"Added";
                break;
            case 2:
                status= @"Sold";
                break;
            case 3:
                status= @"Traded";
                break;
            case 100:
                status= @"0 - none 1 - Added 2 - Sold 3 - Traded";
                break;
            case 101:
                status= @"";
                break;

            default:
                break;
        }
        
        
        switch ([(Item*)[collection objectAtIndex:i] PreciousMetal])
        {
            case 0:
                metal= @"None";
                break;
            case 1:
                metal= @"Silver";
                break;
            case 2:
                metal= @"Gold";
                break;
            case 3:
                metal= @"Platinum";
                break;
            case 100:
                metal= @"0- None 1 - Silver 2 - Gold 3 - Platinum";
                break;
            case 101:
                metal= @"";
                break;
        }
        switch ([(Item*)[collection objectAtIndex:i] WeightUnit])
        {
            case 0:
                unit= @"Gramms";
                break;
            case 1:
                unit= @"Ounce";
                break;
            case 100:
                unit= @"0 - Gramms 1 - Ounce";
                break;
            case 101:
                unit= @"";
                break;
        }
        switch ([(Item*)[collection objectAtIndex:i] Grade])
        {
            case 101:
                grade= @"";
                break;
          
            case 100:
                grade= @"Grade is int from 0-4. Shown like 0-4 + 1.";
                break;
          
        }
        statusId=[[NSString stringWithFormat:@"%d", [(Item*)[collection objectAtIndex:i] Status]] intValue];
        if(statusId >= 100)
        {
        
            [csv appendFormat:@"\n\"%@\",%@,\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\"",
             [[collection objectAtIndex:i] Name] ,
             [ (Item*)[collection objectAtIndex:i] Year],
             [ (Item*)[collection objectAtIndex:i] PurchaseDate],
             [(Item*)[collection objectAtIndex:i] Price],
             [(Item*)[collection objectAtIndex:i] FaceValue],
             [(Item*)[collection objectAtIndex:i] Description],
             status,
             @""
             ,grade
             ,[(Item*)[collection objectAtIndex:i] Country]
             ,[(Item*)[collection objectAtIndex:i] CountryCode]
             ,[(Item*)[collection objectAtIndex:i] Currency]
             ,[(Item*)[collection objectAtIndex:i] CurrencyCode],
             metal,@""
             ,@""
             ,@"", unit
             ,@""
             ];
        }
        else
        {
            [csv appendFormat:@"\n\"%@\",%@,\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%d\",\"%d\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%d\",\"%f\",\"%d\",\"%@\",\"%@\"",
             [[collection objectAtIndex:i] Name] ,
             [ (Item*)[collection objectAtIndex:i] Year],
             [ (Item*)[collection objectAtIndex:i] PurchaseDate],
             [(Item*)[collection objectAtIndex:i] Price],
             [(Item*)[collection objectAtIndex:i] FaceValue],
             [(Item*)[collection objectAtIndex:i] Description],
             status,
             [(Item*)[collection objectAtIndex:i] Status]
             ,[(Item*)[collection objectAtIndex:i] Grade]+1
             ,[(Item*)[collection objectAtIndex:i] Country]
             ,[(Item*)[collection objectAtIndex:i] CountryCode]
             ,[(Item*)[collection objectAtIndex:i] Currency]
             ,[(Item*)[collection objectAtIndex:i] CurrencyCode],
             metal,[(Item*)[collection objectAtIndex:i] PreciousMetal]
             ,[(Item*)[collection objectAtIndex:i] Weight]
             ,[(Item*)[collection objectAtIndex:i] WeightUnit], unit
             ,[(Item*)[collection objectAtIndex:i] Material]
             ];

        }
        
        // instead of integerValue may be used intValue or other, it depends how array was created
    }
    
    [status release];
    // NSString *yourFileName = @"coinCollection.cvs";
    NSError *error;
    
    
    //create instance of NSFileManager
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    //create an array and store result of our search for the documents directory in itNSDocumentDirectory
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    
    //create NSString object, that holds our exact path to the documents directory
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.csv", yourFileName]]; //add our file to the path
    
    [fileManager createFileAtPath:fullPath contents:[csv dataUsingEncoding:NSUTF8StringEncoding] attributes:nil]; //finally save the path (file)
    
    NSData *mainBundleFile = [NSData dataWithContentsOfFile:csv ];
    [[NSFileManager defaultManager] createFileAtPath:fullPath contents:mainBundleFile attributes:nil];
    
    
    BOOL res = [csv writeToFile:fullPath atomically:YES encoding:NSUTF8StringEncoding error:&error];
    
    if (!res)
    {
        //NSLog(@"Error %@ while writing to file %@", [error localizedDescription], yourFileName );
    }
    
    //[error release];
    [collection release];
    
}



-(void)getAllItemsCVS:(NSString*)filter order:(NSString*)NewOrder ItemList:(NSMutableArray*)items
{
    // NSMutableArray *items = [[NSMutableArray alloc] init];
    NSString *where = [[NSString alloc] initWithFormat:@" and  status like \'%@\'", filter.length == 0?@"%": filter ];
    mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    const char *sql = [@"select items.item_id, items.name, ifnull( year, '') year, ifnull(items.description ,''), ifnull( items.status, '0'), ifnull( price, '0') price, ifnull( items.face_value, '0') face_value , ifnull(grade, 0) grade , items.precious_metal, ifnull( items.weight, '0'), ifnull( items.material, ''), ifnull(items.weight_unit, 0), ifnull(items.country_code, '') , ifnull(purchase_date, ''), ifnull(items.currency_code , ''), ifnull(country.country, ''), ifnull(currency.currency, ''), collections.name  from items left outer join country on items.country_code = country.country_code                        left outer join currency on items.currency_code = currency.currency_code                        left join collections on items.collection_id = collections.collection_id order by collections.name" UTF8String];
   
    const char *dbpath = [ _databasePath UTF8String];
    //sqlite3_stmt    *statement;
    Item *itemObj = nil;//[[Item alloc]init];
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
        // const char *query_stmt = [selectSQL UTF8String];
        
        if (sqlite3_prepare_v2(_contactDB, sql, -1, &_statement, NULL) == SQLITE_OK)
        {
            //NSString *name;
            while (sqlite3_step(_statement) == SQLITE_ROW)
            {
                // NSString  *pk = [[NSString alloc] initWithFormat:@"%s",(const char *)sqlite3_column_text(_statement, 0) ] ;
                //[[NSString alloc] initWithUTF8String:
                
                itemObj = [[Item alloc] initWithPrimaryKey:[NSString stringWithFormat:@"%s",(const char *)sqlite3_column_text(_statement, 0) ]  ];
                
                
                ///name = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(_statement, 1)] ;
                [itemObj setName: [NSString stringWithUTF8String: (const char *) sqlite3_column_text(_statement, 1)]];
                [itemObj setItemId:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 0)]];
                [itemObj setDescription:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 3)]];
                if(sqlite3_column_text(_statement, 2) != NULL)
                {
                    [itemObj setYear:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 2)]];
                }
                [itemObj setStatus:[[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 4)] intValue]];
                [itemObj setPrice:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 5)]];
                [itemObj setFaceValue:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 6)]];
                [itemObj setGrade:[[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 7)] intValue]];
                [itemObj setPreciousMetal:[[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 8) ]intValue]];
                [itemObj setWeight:[[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 9)] floatValue]];
                [itemObj setMaterial:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 10)]];
                [itemObj setWeightUnit:[[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 11)] intValue]];
                [itemObj setCountryCode:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 12)]];
                [itemObj setPurchaseDate:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 13)]];
                [itemObj setCurrencyCode:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 14)]];
                [itemObj setCurrency:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 16)]];
                [itemObj setCountry:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 15)]];
                [itemObj setCollectionName:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 17)]];
                
                
                [mainDelegate.DataBase setSqlStatus: @"1"];
               
                [items addObject:itemObj];
                [itemObj release];
                // [name release];
            }
            
            sqlite3_finalize(_statement);
        }
        sqlite3_close(_contactDB);
    }
    [where release];
    //[itemObj release];
    // return items ;
}




-(void)createCVSAllItems:(NSString*)yourFileName PriceIncluded:(BOOL)priceIncluded
{
    NSMutableArray *collection = [[NSMutableArray alloc]init ];
    [self   getAllItemsCVS:@"" order:@" year " ItemList:collection];
    
    NSMutableString *csv;
    if (priceIncluded)
    {
        csv = [NSMutableString stringWithString:@"CollectionName,Name,Date,PurchaseDate,Price,FaceValue,Description,Status,StatusCode,Grade,Country,CountryCode,Currency,CurrencyCode,Metal,MetalCode,Weight,WeightUnit,Unit,Material"];
        
    }
    else
    {
        csv = [NSMutableString stringWithString:@"CollectionName,Name,Date,PurchaseDate,FaceValue,Description,Status,StatusCode,Grade,Country,CountryCode,Currency,CurrencyCode,Metal,MetalCode,Weight,WeightUnit,Unit,Material"];
        
    }
     NSString *status = @"";
    NSUInteger count = [collection count];
    NSString *metal=@"";
    NSString *unit=@"";
    //  NSString *weightUnit=[[NSString alloc] init];
    
    // provided all arrays are of the same length
    for (NSUInteger i=0; i<count; i++ )
    {
        switch ([(Item*)[collection objectAtIndex:i] Status]) {
            case 0:
                status= @"New";
                break;
            case 1:
                status= @"Added";
                break;
            case 2:
                status= @"Sold";
                break;
            case 3:
                status= @"Traded";
                break;
                
            default:
                break;
        }
        
        
        switch ([(Item*)[collection objectAtIndex:i] PreciousMetal])
        {
            case 0:
                metal= @"None";
                break;
            case 1:
                metal= @"Silver";
                break;
            case 2:
                metal= @"Gold";
                break;
            case 3:
                metal= @"Platinum";
                break;
        }
        switch ([(Item*)[collection objectAtIndex:i] PreciousMetal])
        {
            case 0:
                metal= @"None";
                break;
            case 1:
                metal= @"Silver";
                break;
            case 2:
                metal= @"Gold";
                break;
            case 3:
                metal= @"Platinum";
                break;
        }
        switch ([(Item*)[collection objectAtIndex:i] WeightUnit])
        {
            case 0:
                unit= @"Gramms";
                break;
            case 1:
                unit= @"Ounce";
                break;
            case 100:
                unit= @"0- Gramms 1 - Ounce";
                break;
        }

        if (priceIncluded)
        {
            [csv appendFormat:@"\n\"%@\",\"%@\",%@,\"%@\",\"%@\",\"%@\",\"%@\",\"%d\",\"%@\",\"%d\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%d\",\"%f\",\"%d\",\"%@\",\"%@\"",
             [[[collection objectAtIndex:i] CollectionName] stringByReplacingOccurrencesOfString:@"\"" withString:@"'" ],
             [(Item*)[collection objectAtIndex:i] Name] ,
             [ (Item*)[collection objectAtIndex:i] Year],
             [ (Item*)[collection objectAtIndex:i] PurchaseDate],
             [(Item*)[collection objectAtIndex:i] FaceValue],
             [(Item*)[collection objectAtIndex:i] Description],
             status,
             [(Item*)[collection objectAtIndex:i] Status],
             [(Item*)[collection objectAtIndex:i] Price]
             ,[(Item*)[collection objectAtIndex:i] Grade]+1
             ,[(Item*)[collection objectAtIndex:i] Country]
             ,[(Item*)[collection objectAtIndex:i] CountryCode]
             ,[(Item*)[collection objectAtIndex:i] Currency]
             ,[(Item*)[collection objectAtIndex:i] CurrencyCode],
             metal,[(Item*)[collection objectAtIndex:i] PreciousMetal]
             ,[(Item*)[collection objectAtIndex:i] Weight]
             ,[(Item*)[collection objectAtIndex:i] WeightUnit],unit
             ,[(Item*)[collection objectAtIndex:i] Material]
             ];

        }
        else
        {
            [csv appendFormat:@"\n\"%@\",\"%@\",%@,\"%@\",\"%@\",\"%@\",\"%@\",\"%d\",\"%d\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%d\",\"%f\",\"%d\",\"%@\",\"%@\"",
             
        
         [[[collection objectAtIndex:i] CollectionName] stringByReplacingOccurrencesOfString:@"\"" withString:@"'" ],
         [(Item*)[collection objectAtIndex:i] Name] ,
         [ (Item*)[collection objectAtIndex:i] Year],
         [ (Item*)[collection objectAtIndex:i] PurchaseDate],
         [(Item*)[collection objectAtIndex:i] FaceValue],
         [(Item*)[collection objectAtIndex:i] Description],
         status,
         [(Item*)[collection objectAtIndex:i] Status]
          ,[(Item*)[collection objectAtIndex:i] Grade]+1
         ,[(Item*)[collection objectAtIndex:i] Country]
         ,[(Item*)[collection objectAtIndex:i] CountryCode]
         ,[(Item*)[collection objectAtIndex:i] Currency]
         ,[(Item*)[collection objectAtIndex:i] CurrencyCode],
         metal,[(Item*)[collection objectAtIndex:i] PreciousMetal]
         ,[(Item*)[collection objectAtIndex:i] Weight]
         ,[(Item*)[collection objectAtIndex:i] WeightUnit],unit
         ,[(Item*)[collection objectAtIndex:i] Material]
         ];
            }
        // instead of integerValue may be used intValue or other, it depends how array was created
    }
    
    [status release];
    // NSString *yourFileName = @"coinCollection.cvs";
    NSError *error;
    
    
    //create instance of NSFileManager
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    //create an array and store result of our search for the documents directory in itNSDocumentDirectory
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    
    //create NSString object, that holds our exact path to the documents directory
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.csv", yourFileName]]; //add our file to the path
   
    [fileManager createFileAtPath:fullPath contents:[csv dataUsingEncoding:NSUTF8StringEncoding] attributes:nil]; //finally save the path (file)
    
    NSData *mainBundleFile = [NSData dataWithContentsOfFile:csv ];
    [[NSFileManager defaultManager] createFileAtPath:fullPath contents:mainBundleFile attributes:nil];
    
    
    BOOL res = [csv writeToFile:fullPath atomically:YES encoding:NSUTF8StringEncoding error:&error];
    
    if (!res)
    {
        //NSLog(@"Error %@ while writing to file %@", [error localizedDescription], yourFileName );
    }
    
    //[error release];
    [collection release];
    
}



@end
