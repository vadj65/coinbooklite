//
//  User.h
//  CoinBook
//
//  Created by Vadim Re on 2013-01-27.
//  Copyright (c) 2013 Vadim Re. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject


@property (nonatomic, retain) NSString *userName;
@property (nonatomic, retain) NSString *password;
@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) NSString *emailTemp;
@property (nonatomic, retain) NSString *country;
@property (nonatomic, retain) NSString *currency;
@property (nonatomic, retain) NSString *countryCode;
@property (nonatomic, retain) NSString *currencyCode;
@property (nonatomic, assign) BOOL isPassword;

-(void)setUserName:(NSString *)newValue;
-(void)setCountry:(NSString *)newValue;
-(void)setCurrency:(NSString *)newValue;
-(void)setCountryCode:(NSString *)newValue;
-(void)setCurrencyCode:(NSString *)newValue;
-(void)setEmail:(NSString *)newValue;
-(void)setPassword:(NSString *)newValue;
-(NSString*)getPassword;
-(BOOL)getIsPassword;
-(void)setIsPassword:(BOOL )newValue;
-(NSString *)getEmail;
@end
