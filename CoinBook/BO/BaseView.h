//
//  BaseView.h
//  CoinBook
//
//  Created by Vadim Re on 2013-01-27.
//  Copyright (c) 2013 Vadim Re. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class AppDelegate;

@interface BaseView  : UIViewController
<UITextFieldDelegate>
{
    AppDelegate *mainDelegate;
    IBOutlet UIScrollView *scroller;
    UITextField *activeField;
    IBOutlet UITextField *txtCountry;
    IBOutlet UITextField *txtCurrency;
    IBOutlet UINavigationBar *topNavigation;

}


///@property (nonatomic, retain) IBOutlet UIScrollView *scroller;

- (void)registerForKeyboardNotifications;
-(void)setupGesture;
-(void)updateTopLabel:(NSString*)text;
- (void)handleSwipe:(UISwipeGestureRecognizer *)swipe;

@end
