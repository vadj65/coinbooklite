//
//  BaseView.m
//  CoinBook
//
//  Created by Vadim Re on 2013-01-27.
//  Copyright (c) 2013 Vadim Re. All rights reserved.
//

#import "BaseView.h"
#import "DropDownList.h"

#import "AppDelegate.h"

@implementation BaseView



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
  
}
#pragma mark Keyboard movement

- (void)textFieldDidBeginEditing:(UITextField *)textField

{
    
    activeField = textField;
    
}



- (void)textFieldDidEndEditing:(UITextField *)textField

{
    
    activeField = nil;
    
}

- (void)registerForKeyboardNotifications

{
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification   object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification

{
    
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scroller.contentInset = contentInsets;
    scroller.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    
    // Your application might not need or want this behavior.
    
    CGRect aRect = self.view.frame;
    
    aRect.size.height -= kbSize.height;
    
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) )
    {
        float yyy = (activeField.frame.origin.y-kbSize.height)+70;
        if(yyy > 70)
        {
            CGPoint scrollPoint = CGPointMake(0.0, (activeField.frame.origin.y-kbSize.height)+70);//
            [scroller setContentOffset:scrollPoint animated:YES];
        }
    }
    
}

// Called when the UIKeyboardWillHideNotification is sent

- (void)keyboardWillBeHidden:(NSNotification*)aNotificati
{
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scroller.contentInset = contentInsets;
    scroller.scrollIndicatorInsets = contentInsets;
}


-(void)setupGesture
{
    UISwipeGestureRecognizer *swipeGestureLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    swipeGestureLeft.numberOfTouchesRequired = 1;
    swipeGestureLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeGestureLeft];
    
    UISwipeGestureRecognizer *swipeGestureRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    swipeGestureRight.direction = UISwipeGestureRecognizerDirectionRight;
    swipeGestureRight.numberOfTouchesRequired = 1;
    [self.view addGestureRecognizer:swipeGestureRight];
    [swipeGestureRight release];
    [swipeGestureLeft release];
}

- (void)handleSwipe:(UISwipeGestureRecognizer *)swipe {
    
    if (swipe.direction == UISwipeGestureRecognizerDirectionLeft) {
        NSLog(@"Left Swipe");
    }
    
    if (swipe.direction == UISwipeGestureRecognizerDirectionRight) {
        NSLog(@"Right Swipe");
    }
    
}

#pragma  mark Methods

-(void)updateTopLabel:(NSString*)text
{
      topNavigation.topItem.title = text;
 
}




@end
