//
//  CellLabelLabel.m
//  CoinBook
//
//  Created by Vadim Re on 2013-03-06.
//  Copyright (c) 2013 Vadim Re. All rights reserved.
//

#import "CellLabelLabel.h"

@implementation CellLabelLabel


@synthesize lblLabelLeft ;//= intlblLabelLeft;
@synthesize lblLabelRight, cellSection, cellRow;// = intlblLabelRight;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self setSelected:UITableViewCellSelectionStyleGray];

    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
   // [self setSelected:UITableViewCellSelectionStyleGray];

    // Configure the view for the selected state
}
@end
