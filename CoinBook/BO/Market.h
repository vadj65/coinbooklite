//
//  Market.h
//  CoinBook
//
//  Created by Vadim Re on 2012-11-27.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Market : NSObject
{
    NSUserDefaults *prefs;
    
    float _goldOz;
    float _silverOz;
    float _platinumOz;
    float _goldGr;
    float _silverGr;
    float _platinumGr;
   
    
}


@property (nonatomic, readwrite) float GoldOz;
@property (nonatomic, readwrite) float SilverOz;
@property (nonatomic, readwrite) float PlatinumOz;
@property (nonatomic, readwrite) float GoldGr;
@property (nonatomic, readwrite) float SilverGr;
@property (nonatomic, readwrite) float PlatinumGr;
@property (nonatomic, readwrite) int requester;

-(float)getGoldGr;
-(void)setGoldGr:(float)newGoldGr;

-(float)getGoldOz;
-(void)setGoldOz:(float)newGoldOz;
-(float)getSilverGr;
-(void)setSilverGr:(float)newSilverGr;
-(float)getSilverOz;
-(void)setSilverOz:(float)newSilverOz;
-(float)getPlatinumGr;
-(void)setPlatinumGr:(float)newPlatinumGr;
-(float)getPlatinumOz;
-(void)setPlatinumOz:(float)newPlatinumOz;


@end
