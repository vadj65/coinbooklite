//
//  Utility.h
//  CoinBook
//
//  Created by Vadim Re on 2012-11-24.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import <Foundation/Foundation.h>
@class AppDelegate;
@interface Utility : NSObject  <NSXMLParserDelegate>
{
    AppDelegate *mainDelegate;
    NSURLConnection *conn;
    NSMutableData *webData;
    NSString *xmlStr;
    NSString *_httpBody;
    NSString *_data;
    NSMutableURLRequest *_urlRequest;
}

@property (nonatomic,retain) NSString *Response;
@property(retain, nonatomic) NSMutableURLRequest *UrlRequest;
//@property (nonatomic, retain) UIActivityIndicatorView *activityIndicator;



+(int)getPreferenceInt:(NSString*)prefKey;
+(NSString*)getPreference:(NSString*)prefKey;
+(void)setPreferenceInt:(NSString*)prefKey PrefValue:(int)prefValue;
+(void)setPreference:(NSString*)prefKey PrefValue:(NSString*)prefValue;
-(void)executeCommand;

+(void)showAlert:(NSString*)topic alertTag:(int)AlertTag;
+(void)showAlertOneButton:(NSString*)topic alertTag:(int)AlertTag;
-(void)menuButton:(UIButton *)button;
+(UIColor*)colorGray;
+(BOOL) validateEmail: (NSString *) candidate;

@end
