//
//  ItemCell.h
//  CoinBook
//
//  Created by Vadim Re on 2012-11-15.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemCell : UITableViewCell
{
    UILabel *ivLblName;
    UISwitch *ivSwStatus;
}
@property (retain, nonatomic) IBOutlet UILabel *lblName;
@property (retain, nonatomic) IBOutlet UISwitch *swStatus;
@property (retain, nonatomic) IBOutlet NSString *ItemId;
@property (nonatomic, readwrite)  int ItemIndex;

- (IBAction)onStatusChanged:(id)sender;

@end
