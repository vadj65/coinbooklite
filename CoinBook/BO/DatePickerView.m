//
//  DatePickerView.m
//  Hex
//
//  Created by macko on 5/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DatePickerView.h"


@implementation DatePickerView

//@synthesize datePicker;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.clipsToBounds = YES;
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"yyyy"];
        
        //picker init
        datePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
        datePicker.datePickerMode = UIDatePickerModeDate;
        datePicker.hidden = NO;
        datePicker.minimumDate = [df dateFromString:@"1900"];
        datePicker.maximumDate = [df dateFromString:@"2012"];
      //  NSDate *date;
       // NSString *formattedDate = [df stringFromDate:date];
        [df release];
       // datePicker.date = formattedDate;
        /*[datePicker addTarget:self 
         action:<#(SEL)#> forControlEvents:(UIControlEvents)]*/
        
        
        //Resize the picker, rotate it so that it is horizontal and set its position
        CGAffineTransform rotate = CGAffineTransformMakeRotation(0);//(-1.57);  
        rotate = CGAffineTransformScale(rotate, 1 ,1);//(rotate, .46, 2.25);
        CGAffineTransform t0 = CGAffineTransformMakeTranslation(-20,-15);//(3, 22.5);
        datePicker.transform = CGAffineTransformConcat(rotate,t0);
        [self addSubview:datePicker];
        [datePicker bringSubviewToFront:self];
        [datePicker release]; 
        
    }
    return self;
}


-(void) setDatePickerDate:(NSDate *)date
{
    [datePicker setDate:date];
}

-(NSDate *) getDatePickerDate
{
    return [datePicker date];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)dealloc
{
    [super dealloc];
}

@end
