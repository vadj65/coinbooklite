//
//  CellLabelText.m
//  CoinBook
//
//  Created by Vadim Re on 2013-03-06.
//  Copyright (c) 2013 Vadim Re. All rights reserved.
//

#import "CellLabelText.h"
#import "AppDelegate.h"
#import "DropDownList.h"

@implementation CellLabelText
@synthesize lblLeft, txtField, cellNumber, TextValue, cellRow,cellSection;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)hideNumberKeyboard
{
    TextValue = txtField.text;
    [txtField resignFirstResponder];
}



@end
