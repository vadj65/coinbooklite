//
//  CountryPickerView.h
//  Hex
//
//  Created by Balint F. Zoltan on 5/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ItemViewController;
@interface CountryPickerView : UIView <UIPickerViewDelegate>
{
    UIPickerView *picker;
    NSMutableDictionary *countries;
    NSMutableDictionary *currency;
    NSMutableArray *val;
    UIViewController *contr;
    UIViewController *vcCurrency;
}

@property (nonatomic,retain) NSMutableDictionary* countries;
@property (nonatomic,retain) NSMutableArray* val;
@property (nonatomic,retain) NSString* selectedCountry;

- (id)initWithFrame:(CGRect)frame withDictionary:(NSMutableDictionary*)countriesDic controller:(UIViewController*)controller;
- (void)setValue:(NSString*)name;

@end
