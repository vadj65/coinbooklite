//
//  CountryReadFromXML.m
//  igQuartetto
//
//  Created by Vadim Reznikov on 11-10-18.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CountryReadFromXML.h"
#import "PairKeyValue.h"


@implementation CountryReadFromXML

@synthesize Countries;
@synthesize  FileName;

NSString *xmlStr;
int ind1 = 1;

- (void)dealloc 
{
    [countries release];
    countries = nil;
   
    [xmlStr release];
    xmlStr = nil;
   
    [super dealloc];
  
}
-(NSMutableArray *)Countries
{
    return countries;
}


-(id)init
{
    self= [super init];
    countries = [[NSMutableArray alloc] init];
    
    return self;
}
-(id)initWithFileName:(NSString*)fileName
{
    self= [super init];
    FileName = fileName;
    countries = [[NSMutableArray alloc] init];
   
    return self;
}

-(id)loadXMLData:(NSString *)xmlData
{
	NSXMLParser *parser = [[NSXMLParser alloc] initWithData:[xmlData dataUsingEncoding:NSUTF8StringEncoding]];
	[parser setDelegate:self];
	[parser setShouldProcessNamespaces:NO];
    [parser setShouldReportNamespacePrefixes:NO];
    [parser setShouldResolveExternalEntities:NO];
	
	[parser parse];
	return self;
	
}


-(void)readCountries
{
    //populate contry picker from xml
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"/%@",FileName] ofType:@"xml"];
    NSData* data = [[NSData alloc] initWithContentsOfFile:filePath];
    NSString * xml = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
    NSLog(@"%@", xml);
    [data release];
    filePath = nil;
    [self loadXMLData:xml];
    
   //rvv [xml release];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName 
	attributes:(NSDictionary *)attributeDict
{
	
	
    
	if([elementName isEqualToString:@"country"])
	{
        NSLog(@"%@",[attributeDict objectForKey:@"code"]);
          NSLog(@"key= %@",[attributeDict objectForKey:@"name"]);
		PairKeyValue *item = [[PairKeyValue alloc]init];
        [item setKey:[attributeDict objectForKey:@"code"]];
        [item setValue:[attributeDict objectForKey:@"name"]];
        
        [countries addObject:item];
        
        [item release];
			
	}
	    
}


- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{     
    
	if ([elementName isEqualToString:@"gameOptions"])
	{
        return;
    }
/*	else if ([elementName isEqualToString:@"errorMessage"])
	{
		
		//		strXML = [[NSString alloc] init];
		NSLog(@"Error: %@", self.xmlStr);
		_appDelegate.errorMessage= self.xmlStr;
		
	}
	else	if ([elementName isEqualToString:@"sessionInfo"])
	{
		//str= self.xmlStr;
		////NSLog(@"sessionInfo: %@", xmlStr);
	}
	else	if ([elementName isEqualToString:@"memberInfo"])
	{
		//str = self.xmlStr;
		////NSLog(@"memberInfo: %@", xmlStr);
	}
	else	if ([elementName isEqualToString:@"player"])
	{
		//str = self.xmlStr;
		////NSLog(@"player: %@", xmlStr);
	}
	else	if ([elementName isEqualToString:@"event"])
	{
		//str = self.xmlStr;
		////NSLog(@"event: %@", xmlStr);
	}
	
	else	if ([elementName isEqualToString:@"private"])
	{
		if(_appDelegate.game.privateGame != [self.xmlStr intValue])
		{
            [prefs setObject: self.xmlStr  forKey:@"private"];
			_appDelegate.game.settingsChanged = 1;
			_appDelegate.game.privateGame = [self.xmlStr intValue];
		}
		////NSLog(@"private: %@", xmlStr);
	}
	else	if ([elementName isEqualToString:@"scored"])
	{
		if(_appDelegate.game.scored != [self.xmlStr intValue])
		{
            [prefs setObject: self.xmlStr  forKey:@"scored"];
			_appDelegate.game.settingsChanged = 1;
			_appDelegate.game.scored = [self.xmlStr intValue];
		}
		////NSLog(@"memberInfo: %@", elementName);
	}
	else	if ([elementName isEqualToString:@"timerTotal"])
	{
		if(_appDelegate.game.timerTotal != [self.xmlStr intValue])
		{
            [prefs setObject: self.xmlStr  forKey:@"igTimerTotal"];
			_appDelegate.game.settingsChanged = 1;
			_appDelegate.game.timerTotal = [self.xmlStr intValue];
		}
		////NSLog(@"memberInfo: %@", elementName);
	}
	else	if ([elementName isEqualToString:@"timerInc"])
	{
		if(_appDelegate.game.timerInc != [self.xmlStr intValue])
		{
            [prefs setObject: self.xmlStr forKey:@"igTimerInc"];
            
			_appDelegate.game.settingsChanged = 1;
			_appDelegate.game.timerInc = [self.xmlStr intValue];
		}
		////NSLog(@"memberInfo: %@", elementName);
	}
	else	if ([elementName isEqualToString:@"initPos"])
	{
		if(![_appDelegate.game.initPos isEqualToString:self.xmlStr] && self.xmlStr != nil)
		{
            [prefs setObject:self.xmlStr  forKey:@"igPlace"];
            
			_appDelegate.game.settingsChanged = 1;
			_appDelegate.game.initPos = self.xmlStr;
		}
		////NSLog(@"memberInfo: %@", elementName);
	}
    
    */    
    [xmlStr release];
    xmlStr = nil;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	if (string)
	{
        xmlStr = [[NSMutableString alloc] initWithString: string];
		//NSLog(@"string: %@", xmlStr);
	}
}


@end
