//
//  User.m
//  CoinBook
//
//  Created by Vadim Re on 2013-01-27.
//  Copyright (c) 2013 Vadim Re. All rights reserved.
//

#import "User.h"
#import "Utility.h"

@implementation User

@synthesize userName, country, currency,email,password, countryCode, currencyCode,emailTemp,isPassword;

-(id)init
{
   self = [super init];
    userName = [Utility getPreference:@"coinUserName"];
    country = [Utility getPreference:@"coinCountry"];
    currency = [Utility getPreference:@"coinCurrency"];
    countryCode = [Utility getPreference:@"coinCountryCode"];
    currencyCode = [Utility getPreference:@"coinCurrencyCode"];
    email = [Utility getPreference:@"coinEmail"];
    password = [Utility getPreference:@"coinPassword"];
    emailTemp = [[NSString alloc]init];
    
    isPassword = false;
    return self;
}

-(void)setUserName:(NSString *)newValue
{
    userName = newValue;
    [Utility setPreference:@"coinUserName" PrefValue:newValue];
}
-(void)setCountry:(NSString *)newValue
{
    country = newValue;
    [Utility setPreference:@"coinCountry" PrefValue:newValue];
}
-(void)setCurrency:(NSString *)newValue
{
    currency = newValue;
    [Utility setPreference:@"coinCurrency" PrefValue:newValue];
}
-(void)setCountryCode:(NSString *)newValue
{
    countryCode = newValue;
    [Utility setPreference:@"coinCountryCode" PrefValue:newValue];
}
-(void)setCurrencyCode:(NSString *)newValue
{
    currencyCode = newValue;
    [Utility setPreference:@"coinCurrencyCode" PrefValue:newValue];
}
-(void)setEmail:(NSString *)newValue
{
    email = newValue;
    [Utility setPreference:@"coinEmail" PrefValue:newValue];
}
-(void)setPassword:(NSString *)newValue
{
    password = newValue;
    [Utility setPreference:@"coinPassword" PrefValue:newValue];
}
-(void)setIsPassword:(BOOL )newValue
{
    isPassword = newValue;
    [Utility setPreference:@"coinIsPassword" PrefValue:newValue? @"YES":@"NO"];
}
-(BOOL)getIsPassword
{
    if([Utility getPreference:@"coinIsPassword"].length ==0)
    {
        isPassword = NO;
    }
    else
    {
        NSString *isPSW = [Utility getPreference:@"coinIsPassword"];
        if([isPSW isEqualToString:@"NO" ])
            {
                isPassword = FALSE;
            }
            else
            {
                 isPassword = TRUE;
            }
    }
    return isPassword;
}
-(NSString*)getPassword
{
    password =[Utility getPreference:@"coinPassword"];
    return password;
}
-(NSString*)getEmail
{
    email = [Utility getPreference:@"coinEmail"];
    return email;
}

@end
