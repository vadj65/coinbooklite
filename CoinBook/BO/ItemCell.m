//
//  ItemCell.m
//  CoinBook
//
//  Created by Vadim Re on 2012-11-15.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import "ItemCell.h"
#import "Item.h"
#import "AppDelegate.h"
#import "Password.h"

@implementation ItemCell

@synthesize lblName = ivLblName;
@synthesize swStatus ;
@synthesize ItemId, ItemIndex;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
      
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [ivSwStatus release];
    [ivLblName release];
    [super dealloc];
}
- (IBAction)onStatusChanged:(id)sender
{
    AppDelegate * mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(swStatus.tag==TAG_Settings)
    {
        if(swStatus.on)
        {
             [mainDelegate.CoinUser setIsPassword:TRUE];
           
        }
        else
        {
             [mainDelegate.CoinUser setIsPassword:FALSE];
        }
       
    }
    else
    {
        
        Item *itemExec =[[Item alloc]init];
        [itemExec updateItemStatus:ItemId status:swStatus.on?@"1":@"0"];
        [(Item*)[mainDelegate.ItemList  objectAtIndex:ItemIndex] setStatus:swStatus.on] ;
        [itemExec release];
    }
    
}
@end
