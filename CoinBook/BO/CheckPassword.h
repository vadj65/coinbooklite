//
//  CheckPassword.h
//  CoinBook
//
//  Created by Vadim Re on 2013-06-25.
//  Copyright (c) 2013 Vadim Re. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AppDelegate;

@interface CheckPassword : UIViewController
{
    IBOutlet UIButton *btnLogin;
    AppDelegate  *mainDelegate;
    IBOutlet UITextField *txtPassword;
}
- (IBAction)onClickLogin:(id)sender;

@end
