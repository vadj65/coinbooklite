//
//  UIImage+fixOrientation.h
//  CoinBook
//
//  Created by Vadim Re on 2013-06-24.
//  Copyright (c) 2013 Vadim Re. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface UIImage (fixOrientation)

- (UIImage *)fixOrientation;

@end