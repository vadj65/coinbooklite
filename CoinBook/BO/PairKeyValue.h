//
//  PairKeyValue.h
//  CoinBook
//
//  Created by Vadim Re on 2013-01-18.
//  Copyright (c) 2013 Vadim Re. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PairKeyValue : NSObject

@property (nonatomic, retain) NSString *Key;
@property (nonatomic, retain) NSString *Value;

@end
