//
//  Market.m
//  CoinBook
//
//  Created by Vadim Re on 2012-11-27.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import "Market.h"
#define REQUESTER_GRAM 1
#define REQUESTER_OZ 2


@implementation Market

//@synthesize PriceOzGold,PriceOzPlatinum, PriceOzSilver,PriceGrGold,PriceGrPlatinum, PriceGrSilver;
@synthesize requester;
-(id)init
{
    prefs = [NSUserDefaults standardUserDefaults];
	
    return self;
}

-(float)getGoldGr
{
    if(_goldGr == 0 )
    {
        
        if( [prefs objectForKey:@"coinGoldGr"] != NULL && [prefs floatForKey: @"coinGoldGr"] !=0)
        {
            return [prefs floatForKey: @"coinGoldGr"];
        }
        else
        {
            if (requester != REQUESTER_OZ && [self getGoldOz] != 0)
            {
                return [self getGoldOz]/31.1;
            }
            else
            {
                return 0;
            }
        }
    }
    else
    {
        return _goldGr;
    }

}
-(void)setGoldGr:(float)newGoldGr
{
    if(newGoldGr > 0)
    {
        _goldGr = newGoldGr;
        [prefs setFloat:newGoldGr forKey:@"coinGoldGr"];
        [prefs synchronize];
    }
}

-(float)getGoldOz
{
   
        if(_goldOz == 0)
        {
            if( [prefs objectForKey:@"coinGoldOz"] != NULL)
            {
                return [prefs floatForKey: @"coinGoldOz"];
            }
            else
            {
                if (requester != REQUESTER_GRAM && [self getGoldGr] != 0)
                {
                    return [self getGoldGr]*31.1;
                }
                else
                {
                    return 0;
                }

            }
        }
        else
        {
            return _goldOz;
        }
    
}
-(void)setGoldOz:(float)newGoldOz
{
    if(newGoldOz> 0)
    {
        _goldOz = newGoldOz;
        [prefs setFloat:newGoldOz forKey:@"coinGoldOz"];
        [prefs synchronize];
    }
}

-(float)getSilverGr
{
    if(_silverGr == 0)
    {
         
        if( [prefs objectForKey:@"coinSilverGr"] != NULL && [prefs floatForKey: @"coinSilverGr"]!= 0)
        {
            _silverGr = [prefs floatForKey: @"coinSilverGr"];
            return _silverGr;
        }
        else
        {
            if (requester != REQUESTER_OZ && [self getSilverOz] != 0)
            {
                _silverGr = [self getSilverOz]/31.1;
                return _silverGr;
            }
            else
            {
                return 0;
            }
        }
    }
    else
    {
        return _silverGr;
    }

  }
-(void)setSilverGr:(float)newSilverGr
{
    if(newSilverGr> 0)
    {
        _silverGr = newSilverGr;
        [prefs setFloat:newSilverGr forKey:@"coinSilverGr"];
        [prefs synchronize];
    }
}
-(float)getSilverOz
{
    if(_silverOz == 0)
    {
        if( [prefs objectForKey:@"coinSilverOz"] != NULL)
        {
            _silverOz = [prefs floatForKey: @"coinSilverOz"];
            return _silverOz;
        }
        else
        {
            if (requester != REQUESTER_GRAM && [self getSilverGr] != 0)
            {
                _silverOz = [self getSilverGr]*31.1;
                return _silverOz;
            }
            else
            {
                return 0;
            }

        }
    }
    else
    {
        return _silverOz;
    }

  
}
-(void)setSilverOz:(float)newSilverOz
{
    if(newSilverOz> 0)
    {
        _silverOz = newSilverOz;
        [prefs setFloat:newSilverOz forKey:@"coinSilverOz"];
        [prefs synchronize];
    }
}
-(float)getPlatinumGr
{
    if(_platinumGr == 0)
    {
        if( [prefs objectForKey:@"coinPlatinumGr"] != NULL)
        {
            return [prefs floatForKey: @"coinPlatinumGr"];
        }
        else
        {
            if (requester != REQUESTER_OZ && [self getPlatinumOz] != 0)
            {
                return [self getPlatinumOz]/31.1;
            }
            else
            {
                return 0;
            }

        }
    }
    else
    {
        return _platinumGr;
    }

   
}
-(void)setPlatinumGr:(float)newPlatinumGr
{
    if(newPlatinumGr > 0)
    {
        _platinumGr = newPlatinumGr;
        [prefs  setFloat:newPlatinumGr forKey:@"coinPlatinumGr"];
        [prefs synchronize];
    }
}
-(float)getPlatinumOz
{
    if(_platinumOz == 0)
    {
      if( [prefs objectForKey:@"coinPlatinumOz"] != NULL)
        {
            return [prefs floatForKey: @"coinPlatinumOz"];
        }
        else
        {
            if (requester != REQUESTER_GRAM && [self getPlatinumGr] != 0)
            {
                return [self getPlatinumGr]*31.1;
            }
            else
            {
                return 0;
            }
        }
    }
    else
    {
        return _platinumOz;
    }
    
}
-(void)setPlatinumOz:(float)newPlatinumOz
{
    if(newPlatinumOz > 0 )
    {
        _platinumOz = newPlatinumOz;
        [prefs setFloat:newPlatinumOz forKey:@"coinPlatinumOz"];
        [prefs synchronize];
    }
}


@end
