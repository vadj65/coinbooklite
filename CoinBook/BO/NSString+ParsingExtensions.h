//
//  NSString+ParsingExtensions.h
//  CoinBook
//
//  Created by Vadim Re on 2012-12-06.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (ParsingExtensions)

-(NSArray *)csvRows;
@end
