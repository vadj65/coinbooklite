//
//  AppSettings.h
//  CoinBook
//
//  Created by Vadim Re on 2012-11-24.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppSettings : NSObject

@property (nonatomic, readwrite) int Connection;

@end
