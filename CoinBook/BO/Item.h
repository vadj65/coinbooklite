//
//  Item.h
//  CoinBook
//
//  Created by Vadim Re on 2012-11-09.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "sqlite3.h"
#import "DS.h"
@class AppDelegate;

@interface Item : DS
{
    AppDelegate *mainDelegate;

    //NSString *_itemId;
 //   NSString *_name;
  //  NSString *_description;
    NSString *_location;
    NSString *_faceValue;
  // sqlite3_stmt    *_statement;
  //  sqlite3 *_contactDB;

}


@property (nonatomic, retain) NSString *ItemId;
@property (nonatomic, retain) NSString *Year;
@property (nonatomic, retain) NSString *Name;
@property (nonatomic, retain)  NSString *Description;


@property (nonatomic, retain) NSString *FaceValue;
@property (nonatomic, retain) NSString *Material;
@property (nonatomic, readwrite) float Weight;

@property (nonatomic, retain) NSString *Dimension;
@property (nonatomic, retain) NSString *Finish;
@property (nonatomic, readwrite) int Status;
@property (nonatomic, retain) NSString *Price;
@property (nonatomic, retain) NSString *Country;
@property (nonatomic, copy) NSString *CountryCode;
@property (nonatomic, retain) NSString *PurchaseDate;
@property (nonatomic, retain) NSString *DateOut;
@property (nonatomic, readwrite) int Grade;
@property (nonatomic, readwrite) int PreciousMetal;
@property (nonatomic, readwrite) int WeightUnit;
@property (nonatomic, retain) NSString *Currency;
@property (nonatomic, copy) NSString *CurrencyCode;
@property (nonatomic, retain) NSString *CollectionName;
@property (nonatomic, readwrite) int CollectionId;
@property (nonatomic, retain) NSString *ImageName;



- (id) initWithPrimaryKey:(NSString*)pk;
//-(Item*)getItemById:(NSString*)itrmId;
//-(NSMutableArray*)getAllItems;
-(void)getAllItemsByCollectionId:(NSString*)filter order:(NSString*)NewOrder ItemList:(NSMutableArray*)items;

//-(NSMutableArray*)getAllItemsByCollectionId:(NSString*)filter order:(NSString*)NewOrder;
-(void)insertItem;
-(void)commandInsertCollectionItem;
-(void)getItemByItemId:(NSString*)parItemId;
-(void)updateItem;
-(void)updateItemStatus:(NSString*)ItemId status:(NSString*)Status;
-(NSString*)getItemSeq;
-(void)getAllItems:(NSString*)filter sort:(NSString*)Sort ItemList:(NSMutableArray*)items;
//-(NSMutableArray*)getAllItems:(NSString*)filter sort:(NSString*)Sort;
-(void)deleteItem;
-(void)updateItemByCollectionId:(NSString*)collectionId;
-(NSString*)itemYearExist:(int)checkYear;
-(void)updateForceItemByCollectionId:(NSString*)collectionId;
-(void)clearItem;
-(void)createCVSAllItems:(NSString*)yourFileName PriceIncluded:(BOOL)priceIncluded;
-(void)createCVS:(NSString*)yourFileName PriceIncluded:(BOOL)priceIncluded;
-(void)createCVSTemplate:(NSString*)yourFileName;
-(void)updateItemImagePath:(NSString*)UpdatedItemId ImagePath:(NSString*)imagePath;
-(NSString*)getItemImagePath:(NSString*)itemIdImage;
@end
