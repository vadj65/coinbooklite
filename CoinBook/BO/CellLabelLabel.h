//
//  CellLabelLabel.h
//  CoinBook
//
//  Created by Vadim Re on 2013-03-06.
//  Copyright (c) 2013 Vadim Re. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellLabelLabel : UITableViewCell
{
    UILabel *intlblLabelLeft;
    UILabel *intlblLabelRight;
}
@property (retain, nonatomic) IBOutlet UILabel *lblLabelLeft;
@property (retain, nonatomic) IBOutlet UILabel *lblLabelRight;
@property (retain, nonatomic) IBOutlet NSString *ItemId;
@property (nonatomic, readwrite)  int cellRow;
@property (nonatomic, readwrite)  int cellSection;


@end
