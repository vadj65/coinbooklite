//
//  DatePickerView.h
//  Hex
//
//  Created by macko on 5/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DatePickerView : UIView {
    UIDatePicker *datePicker;
    
}

//@property (retain, nonatomic) UIDatePicker *datePicker;

-(void) setDatePickerDate:(NSDate *)date;
-(NSDate *) getDatePickerDate;
@end
