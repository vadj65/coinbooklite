//
//  CheckPassword.m
//  CoinBook
//
//  Created by Vadim Re on 2013-06-25.
//  Copyright (c) 2013 Vadim Re. All rights reserved.
//

#import "CheckPassword.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "Utility.h"
#import <QuartzCore/QuartzCore.h>
#import "ImportCSVViewController.h"

@interface CheckPassword ()

@end

@implementation CheckPassword


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
          mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
      [self.view autoresizingMask];
    //Hide top bar/navigation control
    [self.navigationController setNavigationBarHidden:YES];
    
    
    
    btnLogin.hidden=TRUE;
    
    
    txtPassword.alpha=0;
  
  
   
    if(![mainDelegate.CoinUser getIsPassword])
    {
        if(mainDelegate.AppOptions ==0)
        {
            ViewController *coinBook = [[ViewController alloc] init];
            [self presentViewController:coinBook animated:YES completion: nil ];
            [coinBook release];
        }
        else
        {
            
            ImportCSVViewController *coinBook = [[ImportCSVViewController alloc] init];
            [self presentViewController:coinBook animated:YES completion: nil ];
            [coinBook release];
        }
        
    }
    else
    {
        
        txtPassword.alpha=1;
        btnLogin.hidden = FALSE;
        Utility *ut =[[Utility alloc]init];
        // [ut executeCommand];
        [ut menuButton:btnLogin];
        [ut release];

    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onClickLogin:(id)sender
{
    if([[mainDelegate.CoinUser getPassword] isEqualToString:txtPassword.text])
    {
        if(mainDelegate.AppOptions == 0)
        {
            ViewController *coinBook = [[ViewController alloc] init];
            [self presentViewController:coinBook animated:YES completion: nil ];
            [coinBook release];
        }
        else
        {
            
            ImportCSVViewController *coinBook = [[ImportCSVViewController alloc] init];
            [self presentViewController:coinBook animated:YES completion: nil ];
            [coinBook release];
        }

    }
    else
    {
        [Utility showAlertOneButton:@"Password is not correct." alertTag:128];
    }
}
- (void)dealloc {
    [txtPassword release];
    [txtPassword release];
    txtPassword = nil;
    [btnLogin release];
    btnLogin = nil;
    [super dealloc];
}
//- (void)viewDidUnload {
//    
//    [super viewDidUnload];
//}
@end
