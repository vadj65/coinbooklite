//
//  Collection.m
//  CoinBook
//
//  Created by Vadim Re on 2012-11-07.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import "Collection.h"
#import "AppDelegate.h"

@implementation Collection
@synthesize  CollectionId, Description, Name, DateCreated, YearFinish, YearStart, CoinName, isYearRange,isUseItemName, ItemCount, Status;
@synthesize Country, FaceValue, PreciousMetal,Weight, Material, WeightUnit, Currency, CurrencyCode, CountryCode;

//rvv AppDelegate *mainDelegate;
sqlite3_stmt    *_statement;
sqlite3 *_contactDB;


#pragma mark - Init
-(id)init
{
    self = [super init];
    //Name = [[NSString alloc] init];
    //CollectionId = [[NSString alloc] init];
    mainDelegateCollection =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self setIsUseItemName:0];
    return self;
}
- (id) initWithPrimaryKey:(NSString*) pk {
    
    self = [super init];
    [self setCollectionId:pk];
   
    return self;
}

#pragma  mark - Property





#pragma mark - Methods
-(void)getAllCollections:(NSString*)newSatus sortOrder:(NSString*)SortOreder CollectionList:(NSMutableArray*)result
{
    const char *sql = [[NSString stringWithFormat: @"select collections.collection_id, name ,ifnull(description,'') , ifnull(year_start, ''), ifnull(year_finish, ''), date_created,ifnull(use_item_name, ''), ifnull(use_year_range, '') , (select count(*) from items where items.collection_id =collections.collection_id ) , ifnull(cntr.country, '') countryName, ifnull(face_value, ''), precious_metal, weight, ifnull(material, ''), ifnull(weight_unit, 0) ,ifnull(coin_name,''), ifnull(crn.currency, '') currency, ifnull(collections.country_code, ''), ifnull(collections.currency_code,'') from collections  left outer join currency crn on collections.currency_code = crn.currency_code  left outer join country cntr on  collections.country_code = cntr.country_code where status = %@ order by %@", newSatus, SortOreder ] UTF8String];
    //NSLog(@"%s" ,sql );
    
    
    const char *dbpath = [_databasePath UTF8String];
    //sqlite3_stmt    *statement;
    
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
       // const char *query_stmt = [selectSQL UTF8String];
        
        if (sqlite3_prepare_v2(_contactDB, sql, -1, &_statement, NULL) == SQLITE_OK)
        {
           // int resultSql =sqlite3_step(_statement);
           // NSLog(@"resultSql=%d", resultSql);
           

            while (sqlite3_step(_statement) == SQLITE_ROW)
            {
                //pk = [[(const char *) sqlite3_column_text(_statement, 0)] intValue] ;[[NSString alloc] initWithUTF8String:
                Collection *collectionObj = [[Collection alloc] initWithPrimaryKey:[NSString stringWithUTF8String: (const char *) sqlite3_column_text(_statement, 0)] ];
                
                [collectionObj setName:[NSString stringWithUTF8String: (const char *) sqlite3_column_text(_statement, 1)] ];
                [collectionObj setDescription:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 2)]];
                [collectionObj setYearStart:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 3)]];
                [collectionObj setYearFinish:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 4)]];
                [collectionObj setIsUseItemName:[[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 6)] intValue]];
                [collectionObj setIsYearRange:[[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 7)] boolValue]];
                [collectionObj setItemCount:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 8)] ];
                [collectionObj setFaceValue:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement,10 )] ];
                [collectionObj setCountry:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 9)] ];
                [collectionObj setPreciousMetal:[[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 11)]  intValue]];
                [collectionObj setWeight:[[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 12)] floatValue ]];
                [collectionObj setMaterial:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 13)] ];
                [collectionObj setWeightUnit:[[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 14)] intValue ]];
                [collectionObj setCoinName:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 15)] ];
                [collectionObj setCurrency:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 16)] ];
                [collectionObj setCountryCode:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 17)] ];
                [collectionObj setCurrencyCode:[NSString stringWithUTF8String: (const char *)  sqlite3_column_text(_statement, 18)] ];
                
                [mainDelegateCollection.DataBase setSqlStatus: @"Match found"];
                [result addObject:collectionObj];
                [collectionObj release];
                
            }
           
           
           
        }
        else
        {
            [mainDelegateCollection.DataBase setSqlStatus: @"0"];
        }
        sqlite3_finalize(_statement);
        sqlite3_close(_contactDB);
    }
    else
    {
        [mainDelegateCollection.DataBase setSqlStatus: @"1"];
    }
    
   // return result ;
    
    
}

-(void)updateCollection
{
    NSString *sqlStatement = @"update collections set name = ? , description = ? , year_start = ?, year_finish = ?, use_item_name = ?, use_year_range  = ?, face_value  = ? , precious_metal  = ?, weight  = ? ,material = ?, weight_unit = ? ,coin_name = ?  ,currency_code = ? ,country_code = ? where collection_id = ?" ;
                                                             // ], Name, Description, YearStart, YearFinish, isUseItemName, isYearRange?@"YES":@"NO", FaceValue, PreciousMetal, Weight ,Material, WeightUnit, CoinName, CurrencyCode, CountryCode, CollectionId];
    
    _dirPaths = NSSearchPathForDirectoriesInDomains(  NSDocumentDirectory, NSUserDomainMask, YES);
    //NSLog(@"%@",sqlStatement );
    
    
    // NSLog(@"%@",updatedSQL );
    _docsDir = [_dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    _databasePath = [[NSString alloc] initWithString: [_docsDir stringByAppendingPathComponent: @"CoinBook.sqlite"]];
    
   // NSFileManager *filemgr = [NSFileManager defaultManager];
    
    const char *dbpath = [_databasePath UTF8String];
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
       const char *insert_stmt = [sqlStatement UTF8String];
        //NSLog(@"%s", insert_stmt);
        sqlite3_prepare_v2(_contactDB, insert_stmt, -1, &_statement,NULL);
        sqlite3_bind_text(_statement, 1, [Name UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(_statement, 2, [Description UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(_statement, 3,[YearStart UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(_statement, 4, [YearFinish UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(_statement, 5, isUseItemName );
        sqlite3_bind_text(_statement, 6, [isYearRange?@"YES":@"NO" UTF8String], -1, SQLITE_TRANSIENT);
        
        sqlite3_bind_text(_statement, 7, [FaceValue UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(_statement, 8, PreciousMetal);
        sqlite3_bind_double(_statement, 9, Weight );
        sqlite3_bind_text(_statement, 10, [Material UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(_statement, 11, WeightUnit);
        sqlite3_bind_text(_statement, 12, [CoinName UTF8String], -1, SQLITE_TRANSIENT);
       
        sqlite3_bind_text(_statement, 13, [CurrencyCode UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(_statement, 14, [CountryCode UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(_statement, 15, [CollectionId UTF8String], -1, SQLITE_TRANSIENT);
        
        int result =sqlite3_step(_statement);
        //NSLog(@"result=%d", result);
        if ( result== SQLITE_DONE)
        {
            
            [mainDelegateCollection.DataBase setSqlStatus: @"1"];
            
        }
        else
        {
            [mainDelegateCollection.DataBase setSqlStatus: @"0"];
        }
        
        sqlite3_finalize(_statement);
        sqlite3_close(_contactDB);
        
    }
   
    [sqlStatement release];
}

-(BOOL)checkBookName:(NSString*)bookName CollectionId:(int)collectionId
{//=? and   where lower(name)
    NSString *sqlStatement =@" SELECT 1, count(*) FROM collections where  lower(name)= ? and (collection_id <>? or ? =-1 )" ;
    BOOL retBool= FALSE;
    _dirPaths = NSSearchPathForDirectoriesInDomains(  NSDocumentDirectory, NSUserDomainMask, YES);
    NSLog(@"%@",sqlStatement );
    NSString *ret;
    
    
    _docsDir = [_dirPaths objectAtIndex:0];
    
    _databasePath = [[NSString alloc] initWithString: [_docsDir stringByAppendingPathComponent: @"CoinBook.sqlite"]];
    
    
    
    const char *dbpath = [_databasePath UTF8String];
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
        
        
        const char *insert_stmt = [sqlStatement UTF8String];
        NSLog(@"%s", insert_stmt);
        sqlite3_prepare_v2(_contactDB, insert_stmt, -1, &_statement,NULL);
        sqlite3_bind_text(_statement, 1, [[bookName lowercaseString] UTF8String], -1, SQLITE_STATIC);
        sqlite3_bind_int(_statement, 3, collectionId );
        sqlite3_bind_int(_statement, 2, collectionId );
        
        
        if (sqlite3_step(_statement) == SQLITE_ROW)
        {
            if(sqlite3_column_text(_statement, 0) != NULL)
            {
                ret = [NSString stringWithUTF8String: (const char *) sqlite3_column_text(_statement, 1)];
                if ([ret isEqualToString:@"0"])
                {
                    retBool = TRUE;
                }
                else
                {
                    retBool = FALSE;
                }
                
            }
            //}
            [mainDelegateCollection.DataBase setSqlStatus: @"1"];
            
        }
        else
        {
            [mainDelegateCollection.DataBase setSqlStatus: @"0"];
        }
        
        sqlite3_finalize(_statement);
        sqlite3_close(_contactDB);
        
    }
    // [filemgr release];
    [sqlStatement release];
    
    return  retBool;
}


-(NSString*)getLastCollectionSeq
{
    
    mainDelegateCollection =(AppDelegate *)[[UIApplication sharedApplication] delegate];
   //SELECT seq FROM sqlite_sequence where name='Collection'
    
    const char *dbpath = [ mainDelegateCollection.DataBase.DatabasePath UTF8String];
    NSString *sqlStatement = [[NSString alloc]initWithFormat:@"select collection_id, name from collections" ];
    NSString *ret =@"";
    //NSLog(@"%@" , sqlStatement);
    // Build the path to the database file;
    
  //  NSFileManager *filemgr = [NSFileManager defaultManager];
     const char *insert_stmt = [@"SELECT seq FROM sqlite_sequence where name='Collections'"  UTF8String];

   
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
        
                
        if(sqlite3_prepare_v2(_contactDB, insert_stmt, -1, &_statement, NULL)== SQLITE_OK)
        {
            if (sqlite3_step(_statement) == SQLITE_ROW)
            {
                if(sqlite3_column_text(_statement, 0) != NULL)
                {
                    ret = [NSString stringWithUTF8String: (const char *) sqlite3_column_text(_statement, 0)];
                    [mainDelegateCollection.DataBase setSqlStatus: @"1"];
                }
            }
            else
            {
                [mainDelegateCollection.DataBase setSqlStatus: @"0"];
            }
        }
        sqlite3_finalize(_statement);
        sqlite3_close(_contactDB);
        
    }
    else
    {
        [mainDelegateCollection.DataBase setSqlStatus: @"0"];
    }
    [sqlStatement release];
    //[filemgr release];
    return ret;
    
}

-(int)CountCollection:(int)collectionId
{
    
    mainDelegateCollection =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    //SELECT seq FROM sqlite_sequence where name='Collection'
    
    const char *dbpath = [ mainDelegateCollection.DataBase.DatabasePath UTF8String];
    NSString *sqlStatement = [[NSString alloc]initWithFormat:@"select count(*) from items where collection_id = %d and year =0", collectionId ];
    int ret =0;
    //NSLog(@"%@" , sqlStatement);
    // Build the path to the database file;
    
    //  NSFileManager *filemgr = [NSFileManager defaultManager];
    const char *insert_stmt = [sqlStatement  UTF8String];
    
    
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
        
        
        if(sqlite3_prepare_v2(_contactDB, insert_stmt, -1, &_statement, NULL)== SQLITE_OK)
        {
            if (sqlite3_step(_statement) == SQLITE_ROW)
            {
                if(sqlite3_column_text(_statement, 0) != NULL)
                {
                    ret = [[NSString stringWithUTF8String: (const char *) sqlite3_column_text(_statement, 0)] intValue];
                    [mainDelegateCollection.DataBase setSqlStatus: @"1"];
                }
            }
            else
            {
                [mainDelegateCollection.DataBase setSqlStatus: @"0"];
            }
        }
        sqlite3_finalize(_statement);
        sqlite3_close(_contactDB);
        
    }
    else
    {
        [mainDelegateCollection.DataBase setSqlStatus: @"0"];
    }
    [sqlStatement release];
    //[filemgr release];
    return ret;
    
}

-(void)insertCollection
{
    NSString *sqlStatement = @"INSERT INTO Collections (name , description , year_start, year_finish, date_created,use_item_name,use_year_range, face_value, country_code, status, precious_metal, weight, material, weight_unit, coin_name, currency_code) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, 1, ?, ?, ?, ?, ?, ?)" ;
                              //, Name, Description, YearStart, YearFinish, DateCreated, isUseItemName , isYearRange?@"YES":@"NO"  , FaceValue,  CountryCode , PreciousMetal, Weight, Material, WeightUnit, CoinName,  CurrencyCode];
    
    _dirPaths = NSSearchPathForDirectoriesInDomains(  NSDocumentDirectory, NSUserDomainMask, YES);
    //NSLog(@"%@",sqlStatement );
    
    
    // NSLog(@"%@",updatedSQL );
    _docsDir = [_dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    _databasePath = [[NSString alloc] initWithString: [_docsDir stringByAppendingPathComponent: @"CoinBook.sqlite"]];
    
    ///NSFileManager *filemgr = [NSFileManager defaultManager];
    
    const char *dbpath = [_databasePath UTF8String];
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        //uncomment to get the time only
        //[formatter setDateFormat:@"hh:mm a"];
        //[formatter setDateFormat:@"MMM dd, YYYY"];
        //[formatter setDateStyle:NSDateFormatterMediumStyle];
        
        NSString *dateToday = [formatter stringFromDate:[NSDate date]];
        
        const char *insert_stmt = [sqlStatement UTF8String];
        //NSLog(@"%s", insert_stmt);
        sqlite3_prepare_v2(_contactDB, insert_stmt, -1, &_statement,NULL);
        sqlite3_bind_text(_statement, 1, [Name UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(_statement, 2, [Description UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(_statement, 3,[YearStart UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(_statement, 4, [YearFinish UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(_statement, 5, [dateToday UTF8String], -1, SQLITE_TRANSIENT);

        sqlite3_bind_int(_statement, 6, isUseItemName );
        sqlite3_bind_text(_statement, 7, [isYearRange?@"YES":@"NO" UTF8String], -1, SQLITE_TRANSIENT);
        
        sqlite3_bind_text(_statement, 8, [FaceValue UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(_statement, 9, [CountryCode UTF8String], -1, SQLITE_TRANSIENT);

        
        sqlite3_bind_int(_statement, 10, PreciousMetal);
        sqlite3_bind_double(_statement, 11, Weight );
        sqlite3_bind_text(_statement, 12, [Material UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(_statement, 13, WeightUnit);
        sqlite3_bind_text(_statement, 14, [CoinName UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(_statement, 15, [CurrencyCode UTF8String], -1, SQLITE_TRANSIENT);
        
        [formatter release];
        
        int result =sqlite3_step(_statement);
        //NSLog(@"result=%d", result);
        if ( result== SQLITE_DONE)
        {
            
            [mainDelegateCollection.DataBase setSqlStatus: @"1"];
            
        }
        else
        {
            [mainDelegateCollection.DataBase setSqlStatus: @"0"];
        }
        
        sqlite3_finalize(_statement);
        sqlite3_close(_contactDB);
        
    }
   // [filemgr release];
    [sqlStatement release];

    [mainDelegateCollection.Collection setCollectionId:[mainDelegateCollection.Collection getLastCollectionSeq]];

}
-(void)deleteCollectionYears:(NSString*)newCollectionId YearStart:(NSString*)yearStart  YearFinish:(NSString*)yearFinish
{
    
   
    NSString *sqlStatement = [NSString stringWithFormat:@" delete from collection_item where item_id  in (select items.item_id from items   where (year < %@ or  year > %@) and items.collection_id = %@)", yearStart, yearFinish, newCollectionId];
    [mainDelegateCollection.DataBase commandSQL:sqlStatement];
    sqlStatement = [NSString stringWithFormat:@"delete from items where (year< %@ or  year > %@) and items.collection_id = %@" , yearStart, yearFinish, newCollectionId];
    [mainDelegateCollection.DataBase commandSQL:sqlStatement];
    
}

-(void)deleteCollection:(NSString*)newCollectionId
{
    mainDelegateCollection =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *sqlStatement = [NSString stringWithFormat:@" delete from collections where collection_id = %@", newCollectionId];
    [mainDelegateCollection.DataBase commandSQL:sqlStatement];
    sqlStatement = [NSString stringWithFormat:@"delete from items where collection_id = %@ " , newCollectionId];
    [mainDelegateCollection.DataBase commandSQL:sqlStatement];
    sqlStatement = [NSString stringWithFormat:@" delete from collection_item where collection_id = %@ ", newCollectionId];
    [mainDelegateCollection.DataBase commandSQL:sqlStatement];

}

-(void)updateItemStatus:(NSString*)UpdatedCollectionId status:(NSString*)whereStatus
{
    
    NSString *sqlStatement = [[NSString alloc] initWithFormat:@" Update collections set status= '\%@\'  where collection_id ='\%@\'", whereStatus ,UpdatedCollectionId ];
    //NSLog(@"%@", sqlStatement);
    [mainDelegateCollection.DataBase commandSQL:sqlStatement];
    [sqlStatement release];
    
}

#pragma mark - Init

@end
