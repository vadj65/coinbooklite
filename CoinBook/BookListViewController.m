//
//  BookListViewController.m
//  CoinBook
//
//  Created by Vadim Re on 2012-10-31.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import "BookListViewController.h"
#import "ViewController.h"
#import "AddBookViewController.h"
#import "Collection.h"
#import "CollectionItemsViewController.h"
#import "AppDelegate.h"

@interface BookListViewController ()

@end

@implementation BookListViewController

AppDelegate *mainDelegate ;
#pragma mark - Init
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self)
    {
        mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
        Collection *collection = [[Collection alloc] init];
        _bookList =[[NSMutableArray alloc] init];
        _bookList = [collection getAllCollections];
        [collection release];
            }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return [_bookList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    //Get the object from the array.
    //Collection *bookObj =[[Collection alloc] init];
   // bookObj = [_bookList objectAtIndex:indexPath.row];
    
    //Set .
   
    cell.textLabel.text = [(Collection*)[_bookList objectAtIndex:indexPath.row] Name];
       return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
   // Collection *coll = [[Collection alloc]init];
  //  coll=[_bookList  objectAtIndex:indexPath.row] ;
   // NSLog(@"indexPath = %@", [coll CollectionId]);
    [mainDelegate setCollection:[_bookList  objectAtIndex:indexPath.row] ];
   // NSLog(@"indexPath = %d", indexPath.row);
    CollectionItemsViewController *vc = [[CollectionItemsViewController alloc] init];
    [self presentViewController: vc animated:YES completion:nil];
    [vc release];

}
#pragma mark - Methods

- (IBAction)onClickBack:(id)sender {
    
    ViewController *vc = [[ViewController alloc] init];
    [self presentViewController: vc animated:YES completion:nil];
    [vc release];

}
- (IBAction)onClickAddBook:(id)sender {
    AddBookViewController *addBookVC = [[AddBookViewController alloc] init];
    [self presentViewController:addBookVC animated:YES completion:nil];
    [addBookVC release];

}
@end
