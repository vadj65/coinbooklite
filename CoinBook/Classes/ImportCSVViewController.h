//
//  ImportCSVViewController.h
//  CoinBook
//
//  Created by Vadim Re on 2012-12-06.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSString+ParsingExtensions.h"
#import "BaseView.h"
@class AppDelegate;

@interface ImportCSVViewController : BaseView
<UITableViewDelegate, UITableViewDataSource>

{
   //rvv  AppDelegate *mainDelegate;
    NSMutableArray *BookList;
    IBOutlet UILabel *lblBookName;
    IBOutlet UITableView *tblCoinBooks;
    IBOutlet UITextField *txtNewBook;
        IBOutlet UINavigationItem *topNav;   
    IBOutlet UILabel *lblFileName;
    
}
- (IBAction)onClickAddNewBook:(id)sender;

- (IBAction)onClickImport:(id)sender;
- (IBAction)onClickBack:(id)sender;
//-(void) handleOpenURL:(NSURL *)url;
- (IBAction)HideKeyboard:(id)sender;

@end
