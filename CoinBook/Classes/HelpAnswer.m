//
//  HelpAnswer.m
//  CoinBook
//
//  Created by Vadim Re on 2013-02-07.
//  Copyright (c) 2013 Vadim Re. All rights reserved.
//

#import "HelpAnswer.h"

@interface HelpAnswer ()

@end

@implementation HelpAnswer

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onClickMainMenu:(id)sender {
}

- (IBAction)onClickBack:(id)sender {
}
- (void)dealloc {
    [txtHelp release];
    [topNav release];
    [super dealloc];
}
- (void)viewDidUnload {
    [txtHelp release];
    txtHelp = nil;
    [topNav release];
    topNav = nil;
    [super viewDidUnload];
}
@end
