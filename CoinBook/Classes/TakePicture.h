//
//  TakePicture.h
//  CoinBook
//
//  Created by Vadim Re on 2013-06-21.
//  Copyright (c) 2013 Vadim Re. All rights reserved.
//

#import "ViewController.h"
#import "UIImage+fixOrientation.h"
@class AppDelegate;


@interface TakePicture : ViewController
<UINavigationControllerDelegate, UIImagePickerControllerDelegate>
{
    //AppDelegate *mainDelegate;
}

@property (retain, nonatomic) IBOutlet UIImageView *pictureImageView;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *btnDelete;
@property (retain, nonatomic) IBOutlet UINavigationItem *topTitle;


-(IBAction)takePhoto :(id)sender;
- (IBAction)onClickBack:(id)sender;
- (IBAction)onClickSaveImage:(id)sender;
- (IBAction)onClickDelete:(id)sender;

@end
