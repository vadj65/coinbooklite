//
//  Help.h
//  CoinBook
//
//  Created by Vadim Re on 2013-01-28.
//  Copyright (c) 2013 Vadim Re. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
@class AppDelegate;

@interface Help : UIViewController
<UITableViewDelegate, UITableViewDataSource,
MFMailComposeViewControllerDelegate>
{
    AppDelegate *mainDelegate;
           IBOutlet UINavigationItem *topNav;
     
    IBOutlet UIButton *btnEmail;
    IBOutlet UITextView *lblMessage;
    IBOutlet UITableView *tblHelp;
    
}

-(IBAction)onClickMainMenu:(id)sender;
-(IBAction)onClickBack:(id)sender;
- (IBAction)onClickLink:(id)sender;
- (IBAction)onClickSendEmail:(id)sender;
- (IBAction)onClickHelp:(id)sender;


@end
