 //
//  CollectionItemsViewController.m
//  CoinBook
//
//  Created by Vadim Re on 2012-11-08.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import "CollectionItemsViewController.h"
#import "ItemCell.h"
#import "CellLabelLabel.h"
#import "MailViewController.h"
#import "Utility.h"
#import "AppDelegate.h"

#define ALERT_FILTER 1
#define ALERT_SORT 2

@interface CollectionItemsViewController ()

@end

@implementation CollectionItemsViewController

@synthesize itemCell = inItemCell;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
        self.modalPresentationStyle = UIModalPresentationFullScreen;
       
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    //spinner.center = CGPointMake(160, 240);
    //spinner.tag = 12;
    //[self.view addSubview:spinner];
   // [spinner startAnimating];
    //[spinner release];
    // Do any additional setup after loading the view from its nib.
    
    //[tableCollection setContentSize:CGSizeMake(320, self.view.frame.size.height - 88)];
    [tableCollection setFrame:CGRectMake(0, 0, 420, self.view.frame.size.height - 4)];
    [self.view setMultipleTouchEnabled:YES];
    [scroller setScrollEnabled:YES];
    
    [scroller setContentSize:CGSizeMake(320, tableCollection.frame.size.height)];
   
    callScroll= TRUE;
    sortDirectionName = @" Asc ";
    sortDirectionYear = @" Asc ";
    sortDirection = @" Asc ";
    [mainDelegate setSortField :@" year Asc " ];
    
    filterType = @"All";
    [self populateList: mainDelegate.whereField Order: mainDelegate.sortField];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark-Buttons

- (IBAction)onClickAdd:(id)sender
{
    if([mainDelegate.ItemList count] < COINS_MAX)
    {
        mainDelegate.Item=nil;
        mainDelegate.Item =[[Item alloc]init];
   
        ItemViewController *vc = [[ItemViewController alloc] init];
        [self presentViewController: vc animated:YES completion:nil];
        [vc release];
    }
    else
    {
        [Utility showAlertOneButton:@"Book has maximum cells. Create another book." alertTag:128  ];
    }
}

- (IBAction)onClickBack:(id)sender
{
    [mainDelegate setSelectedRowIndexItem:0];
    CollectionListViewController *vc = [[CollectionListViewController alloc] init];
    [self presentViewController: vc animated:YES completion:nil];
    [vc release];

}

- (IBAction)onClickBtnEdit:(id)sender {
}

- (IBAction)onClickBtnSort:(id)sender
{
    if([mainDelegate.ItemList count])
    {
   
	
        NSString *alertName;
        NSString *alertYear;
        if([sortDirectionName isEqualToString:@" Asc "])
        {
            alertName =  @"by Name Desc";
            sortDirectionName = @" Desc ";
        }
        else
        {
            alertName =  @"by Name Asc";
            sortDirectionName = @" Asc ";
        }

        if([sortDirectionYear isEqualToString:@" Asc "])
        {
            alertYear =  @"by Year Desc";
            sortDirectionYear =@" Desc ";
        }
        else
        {
            alertYear =  @"by Year Asc";
            sortDirectionYear =@" Asc ";
        }
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message: @"" preferredStyle:UIAlertControllerStyleAlert];
//[alert setMessage: [NSString stringWithFormat:@"Delete collection %@", [mainDelegate.Collection Name]]];
//        [alert setDelegate:self];
//        [alert addButtonWithTitle:alertName];
//        [alert addButtonWithTitle:alertYear];
//        [alert addButtonWithTitle:@"Cancel"];
//
//        [alert setTag:ALERT_SORT];
        
        UIAlertAction *btnAlertName = [UIAlertAction actionWithTitle:alertName style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self sortName];}
            ];
        [alert addAction:btnAlertName];
        UIAlertAction *btnAlertYear = [UIAlertAction actionWithTitle:alertYear style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self sortYear];
        } ];
        [alert addAction:btnAlertYear];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil ];
       
        [alert addAction:cancel];
        alert.view.tag =ALERT_SORT;
        [self presentViewController:alert animated:YES completion:nil];
    }

}

- (IBAction)onClickFilter:(id)sender
{
    if([mainDelegate.Collection ItemCount])
    {
       
        UIAlertController *dalert = [UIAlertController alertControllerWithTitle:@"" message:  @"Filter" preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *btnAll = [UIAlertAction actionWithTitle:@"All" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self filterAll];
        }];
        [dalert addAction:btnAll];
        
        UIAlertAction *btnInBook = [UIAlertAction actionWithTitle:@"In Book" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self filterInBook];
        }];
        [dalert addAction:btnInBook];
        
        UIAlertAction *btnEmptyPocket = [UIAlertAction actionWithTitle:@"Empty pockets" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self filterEmptyPocket];
        }];
        [dalert addAction:btnEmptyPocket];
        
        UIAlertAction *btnTraded = [UIAlertAction actionWithTitle:@"Traded" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self filterTraded];
        }];
        [dalert addAction:btnTraded];
       
        UIAlertAction *btnSold = [UIAlertAction actionWithTitle:@"Sold" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self filterSold];
        }];
        [dalert addAction:btnSold];
        
        UIAlertAction *btnCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
        [dalert addAction:btnCancel];
     
        [self presentViewController:dalert animated:YES completion:nil];
        
    }
}

- (IBAction)onClickEmail:(id)sender
{
    if([mainDelegate.ItemList count])
    {
        [mainDelegate setEmailReturn:2];
        MailViewController *vc = [[MailViewController alloc] init];
        [self presentViewController: vc animated:YES completion:nil];
        [vc release];
    }
}
#pragma mark Alerts

-(void)sortYear
{
    if ([sortDirectionYear isEqualToString:@" Desc "])
    {
        [mainDelegate setSortField: @" year Desc"];
        sortDirectionYear = @" Desc ";
    }
    else
    {
        [mainDelegate setSortField: @" year Asc"];
        sortDirectionYear = @" Asc ";
    }
    [self populateList:mainDelegate.whereField Order:mainDelegate.sortField];
    [tableCollection reloadData];
}

-(void)sortName
{
    if ([sortDirectionName isEqualToString:@" Desc "])
    {
        [mainDelegate setSortField: @" name Desc"];
        sortDirectionName = @" Desc ";
    }
    else
    {
        [mainDelegate setSortField: @" name Asc"];
        sortDirectionName = @" Asc ";
    }
    [self populateList:mainDelegate.whereField Order:mainDelegate.sortField];
    [tableCollection reloadData];
}

-(void)filterAll
{
    [mainDelegate setWhereField: @""];
    btnFilter.tintColor = [UIColor grayColor];
    btnFilter.title = @"Filter";
    [self populateList:mainDelegate.whereField Order:mainDelegate.sortField];
    [tableCollection reloadData];
}

-(void)filterInBook
{
    [mainDelegate setWhereField: @"1"];
    btnFilter.tintColor = [UIColor redColor];
    btnFilter.title = @"Filter On";
    [self populateList:mainDelegate.whereField Order:mainDelegate.sortField];
    [tableCollection reloadData];
}

-(void)filterEmptyPocket
{
    [mainDelegate setWhereField: @"0"];
    btnFilter.tintColor = [UIColor redColor];
    btnFilter.title = @"Filter On";
    [self populateList:mainDelegate.whereField Order:mainDelegate.sortField];
    [tableCollection reloadData];
}

-(void)filterInPocket
{
    [mainDelegate setWhereField: @"3"];
    btnFilter.tintColor = [UIColor redColor];
    btnFilter.title = @"Filter On";
    [self populateList:mainDelegate.whereField Order:mainDelegate.sortField];
    [tableCollection reloadData];
}

-(void)filterTraded
{
    [mainDelegate setWhereField: @"4"];
    btnFilter.tintColor = [UIColor redColor];
    btnFilter.title = @"Filter On";
    [self populateList:mainDelegate.whereField Order:mainDelegate.sortField];
    [tableCollection reloadData];
}
-(void)filterSold
{
    [mainDelegate setWhereField: @"5"];
    btnFilter.tintColor = [UIColor redColor];
    btnFilter.title = @"Filter On";
    [self populateList:mainDelegate.whereField Order:mainDelegate.sortField];
    [tableCollection reloadData];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    
        return [mainDelegate.ItemList count];
    
}
/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    //Get the object from the array.
   //Item *itemObj =[[Item alloc] init];
    //itemObj = [_collectionList objectAtIndex:indexPath.row];
    
    //Set .(Item*)[_collectionList objectAtIndex:indexPath.row]
   // NSLog(@"%@", [itemObj getName]);
    cell.textLabel.text = [NSString stringWithFormat:@"%@ -%@",[(Item*)[_collectionItems objectAtIndex:indexPath.row] Year], [(Item*)[_collectionItems objectAtIndex:indexPath.row] Name]];
     return cell;
}

*/

- (id)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
       NSString *leftText =@"";
    //NSLog(@"%d",indexPath.row);
    
    
    
    if([[(Item*)[mainDelegate.ItemList objectAtIndex:indexPath.row] Year] isEqualToString:@"0"])
    {
        leftText=[NSString stringWithFormat:@"%@", [(Item*)[mainDelegate.ItemList objectAtIndex:indexPath.row] Name]];
        
    }
    else
    {
        if ([[(Item*)[mainDelegate.ItemList objectAtIndex:indexPath.row] Name] isEqualToString:@""])
        {
            leftText= [NSString stringWithFormat:@"%@",[(Item*)[mainDelegate.ItemList objectAtIndex:indexPath.row] Year]];
        }
        else
        {
            leftText = [NSString stringWithFormat:@"%@ - %@",[(Item*)[mainDelegate.ItemList objectAtIndex:indexPath.row] Year], [(Item*)[mainDelegate.ItemList objectAtIndex:indexPath.row] Name]];
        }
        
        
    }

    
    if ([(Item*)[mainDelegate.ItemList objectAtIndex:indexPath.row] Status] <= 1)
    {
        //static NSString *CellIdentifier = @"CustomCellIdentifier";

        ItemCell *cellSwitch = [tableView dequeueReusableCellWithIdentifier:@"CustomCellIdentifier"];
    
        if (cellSwitch == nil)
        {
           //cellSwitch = [[ItemCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CustomCellIdentifier"];

           /* NSArray *topObject =  [[NSBundle mainBundle] loadNibNamed:@"ItemCell" owner:self options:nil];
            for(id currentObject in topObject)
            {
                if ([currentObject isKindOfClass:[ItemCell class]])
                {
                    cellSwitch = (ItemCell*)currentObject;
                    break;
                }
            }
*/
            [[NSBundle mainBundle] loadNibNamed:@"ItemCell" owner:self options:nil];
            cellSwitch = [self itemCell];
            [self setItemCell:nil];
        }
        else
        {
            if (![cellSwitch isKindOfClass:[ItemCell class]])
            {
                /*NSArray *topObject =  [[NSBundle mainBundle] loadNibNamed:@"ItemCell" owner:self options:nil];
                for(id currentObject in topObject)
                {
                    if ([currentObject isKindOfClass:[ItemCell class]])
                    {
                        cellSwitch = (ItemCell*)currentObject;
                        break;
                    }
                }
                */
                [[NSBundle mainBundle] loadNibNamed:@"ItemCell" owner:self options:nil];
                cellSwitch = [self itemCell];
                [self setItemCell:nil];
            }
       }
       
       [cellSwitch setItemId:[NSString stringWithFormat:@"%@",[(Item*)[mainDelegate.ItemList objectAtIndex:indexPath.row] ItemId]]];
        if ([(Item*)[mainDelegate.ItemList objectAtIndex:indexPath.row] Status] ==1)
        {
            [cellSwitch swStatus].on = TRUE;
        }   
        else
        {
            [cellSwitch swStatus].on = FALSE;
        }
        [cellSwitch.lblName setText:leftText];
        [cellSwitch setItemIndex:(int)indexPath.row];
        cellSwitch.selectionStyle = UITableViewCellSelectionStyleGray;

        if(callScroll)
        {
            int indTemp;
            if ([mainDelegate.ItemList count] <= [mainDelegate selectedRowIndexItem])
            {
                indTemp =(int)[mainDelegate.ItemList count]-1;
            }
            else
            {
                indTemp = [mainDelegate selectedRowIndexItem];
            }
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indTemp inSection:0];
            [tableCollection scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
            //[tableCollection selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        
            callScroll= false;
        }
        cellSwitch.backgroundColor =[UIColor whiteColor];
        cellSwitch.textLabel.textColor =[UIColor blackColor];
        return cellSwitch;
    }
    else
    {
       // status > 1
        CellLabelLabel *cell = [tableView dequeueReusableCellWithIdentifier:@"CustomCellLabel"];
        
        if (cell == nil)
        {
            NSArray *topObject =  [[NSBundle mainBundle] loadNibNamed:@"CellLabelLabel" owner:self options:nil];
            
            for(id currentObject in topObject)
            {
                if ([currentObject isKindOfClass:[CellLabelLabel class]])
                {
                    cell = (CellLabelLabel*)currentObject;
                    break;
                }
            }
            // cell = [self itemCell];
            // [self setItemCell:nil];
        }
        
      //  cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
       
        [cell.lblLabelRight setFrame:CGRectMake(210, 11, 80, 21)];
        [cell.lblLabelRight setTextAlignment:NSTextAlignmentRight];
        [cell.lblLabelLeft setFrame:CGRectMake(6, 8, 200, 30)];
        [cell.lblLabelLeft setText:leftText];
         cell.selectionStyle = UITableViewCellSelectionStyleGray;
        NSString *status= @"";
        if([(Item*)[mainDelegate.ItemList objectAtIndex:indexPath.row] Status] == 2)
        {
            status = @"Sold";
        }
        else if([(Item*)[mainDelegate.ItemList objectAtIndex:indexPath.row] Status] == 3)
        {
            status = @"Traded";
        }
        [cell.lblLabelRight setText:status];
        cell.backgroundColor =[UIColor whiteColor];
        cell.textLabel.textColor =[UIColor blackColor];
        return cell;

    }
}


 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return NO;
 }
 /**/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}
-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section==0)
    {
        return [mainDelegate.Collection Name];
    }
    else
    {
        return @"";
    }
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
     headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)] autorelease];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, tableView.bounds.size.width, 30)];
    
    if(section==0)
    {
        [label setText:[mainDelegate.Collection Name]];
    }
    label.textColor = [UIColor blackColor];
    label.backgroundColor =[UIColor clearColor];
    [headerView addSubview:label];
    //[headerView setBackgroundColor:[Utility greyColor]];
    [label release];
    return headerView;
}


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
      //NSLog(@"indexPath = %d", indexPath.row);
    [mainDelegate setItem :(Item*)[mainDelegate.ItemList  objectAtIndex:indexPath.row] ];
 
    [mainDelegate setItemId: (int)[mainDelegate.Item.ItemId integerValue ]];
    [mainDelegate setSelectedRowIndexItem:(int)indexPath.row];
    [mainDelegate setItemIndex:(int)indexPath.row];
   
    ItemViewController *vc = [[ItemViewController alloc] init];
    [self presentViewController: vc animated:YES completion:nil];
    [vc release];
    
}

- (void)dealloc {
    [tableCollection release];
    [btnEdit release];
    [btnSort release];
    [inItemCell release];
    inItemCell=nil;
    [mainDelegate release];
    [scroller release];
    [btnEmail release];
    [topNav release];
    [btnFilter release];
    [mainDelegate.ItemList removeAllObjects];
    [headerView release];
   
    [scroller release];
    scroller = nil;
    [btnEmail release];
    btnEmail = nil;
    [topNav release];
    topNav = nil;
    [btnFilter release];
    btnFilter = nil;
    [headerView release];
    [toolBarBottom release];
    toolBarBottom = nil;
       [super dealloc];
}

#pragma mark Methods

-(void)populateList:(NSString*)filter Order:(NSString*)order
{
  
    Item *itemExec = [[Item alloc] init];
    //[mainDelegate.ItemList release];
    mainDelegate.ItemList=nil;
    mainDelegate.ItemList = [[NSMutableArray alloc]init];
    [itemExec getAllItemsByCollectionId:filter order:order ItemList:[mainDelegate ItemList]];
    [itemExec release];
    topNav.title = [NSString stringWithFormat: @"Coins(%lu)", [mainDelegate.ItemList count]];
    
}




@end
