//
//  Metals.m
//  CoinBook
//
//  Created by Vadim Re on 2012-11-27.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//nslo

#import "Metals.h"
#import "AppDelegate.h"
#import "Settings.h"
#import "ViewController.h"
#import "CellLabelLabel.h"

@interface Metals ()

@end

@implementation Metals



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
         mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
                
         self.modalPresentationStyle = UIModalPresentationFullScreen;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   // [[self.view viewWithTag:128] stopAnimating];
    // Do any additional setup after loading the view from its nib.
    //tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    //[tblMetals addGestureRecognizer:tapGesture];
    // prevents the scroll view from swallowing up the touch event of child buttons
    //tapGesture.cancelsTouchesInView = NO;

    [self populateTable];
    topNav.title = @"Metals";

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
       
    [topNav release];
    [tblMetals release];
    tblMetals = nil;
    [super dealloc];
}


- (IBAction)onClickRefresh:(id)sender
{
    [self populateTable];
}

- (void)hideKeyboard
{/*
    NSArray *cellsAll = [tblMetals visibleCells];
    for (CellLabelText  *item in  cellsAll)
    {
        [item hideNumberKeyboard ];
    }
  */
}

#pragma mark Buttons

- (IBAction)onClickSave:(id)sender
{
    /*
    prefs = [NSUserDefaults standardUserDefaults];
    
    NSArray *cellsAll = [tblMetals visibleCells];
    for (CellLabelText  *item in  cellsAll)
    {
        if (item.cellSection == 0)
        {
            if(item.cellRow==0)
            {
                [prefs setObject:item.txtField.text forKey:@"coinSilverOz"];

            }
            else
            {
                [prefs setObject:item.txtField.text forKey:@"coinSilverGr"];
            }
        } else if (item.cellSection == 1)
        {
            if(item.cellRow==0)
            {
                [prefs setObject:item.txtField.text forKey:@"coinGoldOz"];
                
            }
            else
            {
                [prefs setObject:item.txtField.text forKey:@"coinGoldGr"];
            }
        }
        else if (item.cellSection == 2)
        {
                if(item.cellRow==0)
                {
                    [prefs setObject:item.txtField.text forKey:@"coinPlatinumOz"];
                    
                }
                else
                {
                    [prefs setObject:item.txtField.text forKey:@"coinPlatinumGr"];
                }
        } 
   
    
    }
	
    [prefs synchronize];
    */
}

- (IBAction)onClickBack:(id)sender
{
    Settings *vc = [[Settings alloc] init];
    [self presentViewController: vc animated:YES completion:nil];
    [vc release];
    
}

- (IBAction)onClickClear:(id)sender
{
    
}

- (IBAction)onClickMainMenu:(id)sender
{
    ViewController *vc = [[ViewController alloc] init];
    [self presentViewController: vc animated:YES completion:nil];
    [vc release];

}

-(void)hideNumberKeyboard
{
   }
#pragma mark Methods

-(void)populateTable
{
    if ([mainDelegate.AppSettings Connection] ==1)
    {
        
    
        Utility *ut =[[Utility alloc]init];
        [ut executeCommand];
    
        [ut release];
    }
        prefs = [NSUserDefaults standardUserDefaults];
           
    if ([mainDelegate.AppSettings Connection] ==0)
    {
        [Utility showAlertOneButton:@"No internet connection." alertTag:22];
    }
}
#pragma marks Table

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0)
    {
        return 0;
    } else {
        return 2;
    }
    
    
}


- (id)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CustomCellIdentifier";
       
    
        CellLabelLabel *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            NSArray *topObject =  [[NSBundle mainBundle] loadNibNamed:@"CellLabelLabel" owner:self options:nil];
            
            for(id currentObject in topObject)
            {
                if ([currentObject isKindOfClass:[CellLabelLabel class]])
                {
                    cell = (CellLabelLabel*)currentObject;
                    break;
                }
            }
            // cell = [self itemCell];
            // [self setItemCell:nil];
        }
    if (indexPath.section == 0)
    {}
    else if (indexPath.section == 1)
    {
        if(indexPath.row ==0 )
        {
            [cell.lblLabelLeft setText: @"Ounce"];
            [cell.lblLabelRight setText:[NSString stringWithFormat:@"%1.2f" ,[[prefs objectForKey:@"coinSilverOz"] floatValue]]];
        }
        else
        {
            [cell.lblLabelLeft setText: @"Gramm"];
            [cell.lblLabelRight setText:[NSString stringWithFormat:@"%1.2f" ,[[prefs objectForKey:@"coinSilverGr"] floatValue]]];
        }
    }
    else  if (indexPath.section == 2)
    {
        if(indexPath.row ==0 )
        {
            [cell.lblLabelLeft setText: @"Ounce"];
            [cell.lblLabelRight setText:[NSString stringWithFormat:@"%1.2f" ,[[prefs objectForKey:@"coinGoldOz"] floatValue]]];
        }
        else
        {
            [cell.lblLabelLeft setText: @"Gramm"];
            [cell.lblLabelRight setText:[NSString stringWithFormat:@"%1.2f" ,[[prefs objectForKey:@"coinGoldGr"] floatValue]]];
        }
    }
    else  if (indexPath.section == 3)
    {
        if(indexPath.row ==0 )
        {
            [cell.lblLabelLeft setText: @"Ounce"];
            [cell.lblLabelRight setText:[NSString stringWithFormat:@"%1.2f" ,[[prefs objectForKey:@"coinPlatinumOz"] floatValue]]];
        }
        else
        {
            [cell.lblLabelLeft setText: @"Gramm"];
            [cell.lblLabelRight setText:[NSString stringWithFormat:@"%1.2f" ,[[prefs objectForKey:@"coinPlatinumGr"] floatValue]]];
        }
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
   
    [cell  setCellRow:(int)indexPath.row];
    [cell setCellSection:(int)indexPath.section];
     [cell.lblLabelRight setTextAlignment:NSTextAlignmentRight];
    [cell.lblLabelRight setFrame:CGRectMake(110, 12, 185, 30)];
    //[cell.lblLabelRight setKeyboardType:UIKeyboardTypeDecimalPad];
    return cell;
    
       
    //[cell setItemIndex:indexPath.row];
    
    
    
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 4;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    
    
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section==0)
    {
        return @"Precious Metal's Prices";
    }
    else if(section==1)
    {
           return @"Silver";
    }
    else if(section==2)
    {
        return @"Gold";
    }
    else if(section==3)
    {
        return @"Platinum";
    }
    else
    {
        return @"";
    }
}


@end
