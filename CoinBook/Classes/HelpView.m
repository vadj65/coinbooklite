//
//  HelpView.m
//  CoinBook
//
//  Created by Vadim Re on 2013-04-09.
//  Copyright (c) 2013 Vadim Re. All rights reserved.
//

#import "HelpView.h"
#import "Help.h"
@interface HelpView ()

@end

@implementation HelpView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   // NSString *path = [[NSBundle mainBundle] pathForResource:@"/Index" ofType:@"html"];
   // [helpWebView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath: path]]];
    
    //NSString *url = [NSURL URLWithString: [path lastPathComponent]                 relativeToURL: [NSURL fileURLWithPath: [path stringByDeletingLastPathComponent]                                           isDirectory: YES]];
    
   // helpWebView. = UIDataDetectorTypeLink;
    NSString *path = [[NSBundle mainBundle] pathForResource:@"/index" ofType:@"htm" ];
   // NSString *url = [NSURL URLWithString: [path lastPathComponent] relativeToURL: [NSURL fileURLWithPath: [path stringByDeletingLastPathComponent] isDirectory: YES]];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString: [path lastPathComponent] relativeToURL: [NSURL fileURLWithPath: [path stringByDeletingLastPathComponent] isDirectory: YES]]];
    [helpWebView loadRequest:request];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [helpWebView release];
    [super dealloc];
}
//- (void)viewDidUnload {
//   
//    [super viewDidUnload];
//}
- (IBAction)onClickBack:(id)sender
{
    Help *vc = [[Help alloc] init];
    [self presentViewController: vc animated:YES completion:nil];
    [vc release];

}
@end
