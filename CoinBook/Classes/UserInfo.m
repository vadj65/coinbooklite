//
//  UserInfo.m
//  CoinBook
//
//  Created by Vadim Re on 2012-12-15.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import "UserInfo.h"
#import "Settings.h"
#import "Utility.h"
#import "DropDownList.h"
#import "PairKeyValue.h"
#import "CellLabelText.h"
#import "CellLabelLabel.h"
#import "ItemCell.h"
#import "Password.h"
#import "AppDelegate.h"

@interface UserInfo ()

@end

@implementation UserInfo

@synthesize itemCell = inItemCell;
NSMutableArray *cells;




- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
         self.modalPresentationStyle = UIModalPresentationFullScreen;
    }
    return self;
}

- (void)viewDidLoad
{
   [super viewDidLoad];
    tblUser.viewForFirstBaselineLayout.backgroundColor =[UIColor whiteColor];//[Utility color];

   [self updateTopLabel:@"Edit"];
   // [navTop setTitle:@"Application Settings"];
    [self populateScreen];
}

#pragma mark Methods
-(void)populateScreen
{
    cells = [[NSMutableArray alloc] init];
    PairKeyValue *key = [[PairKeyValue alloc]init];
    
    key.Value = [mainDelegate.CoinUser getEmail];
    key.Key = @"Email:";
    
    
    if(mainDelegate.DropdownId > 0)
    {
        key.Value = [mainDelegate.CoinUser emailTemp];
        if (mainDelegate.DropdownId == DROPDOWN_SETTINGS_COUNTRY)
        {
            
            [mainDelegate.CoinUser setCountry:mainDelegate.DropDownValue];
            [mainDelegate.CoinUser setCountryCode:mainDelegate.DropDownKey];
            
            
        }
        else if (mainDelegate.DropdownId== DROPDOWN_SETTINGS_CURRENCY)
        {
            
            [mainDelegate.CoinUser setCurrency:mainDelegate.DropDownValue];
            [mainDelegate.CoinUser setCurrencyCode:mainDelegate.DropDownKey];
            
        }
        [mainDelegate setDropdownId:0];
    }
    [cells addObject:key];
    
    [key release];
    
    key = [[PairKeyValue alloc]init];
    key.Value = [mainDelegate.CoinUser getIsPassword]? @"YES":@"NO";
    key.Key = @"Password:";
    [cells addObject:key];
    
    
    [key release];
    
    key = [[PairKeyValue alloc]init];
    key.Value = [mainDelegate.CoinUser country];
    key.Key = @"Country:";
    [cells addObject:key];
    [key release];
    
    key = [[PairKeyValue alloc]init];
    key.Value = [mainDelegate.CoinUser currency];
    key.Key = @"Currency:";
    [cells addObject:key];
    [key release];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
       
    [navTop release];
    navTop = nil;
    [tblUser release];
    tblUser = nil;
    [super dealloc];
}


#pragma mark Button methods
- (IBAction)onCLickCLear:(id)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Clear all settings" message:@"" preferredStyle:UIAlertControllerStyleAlert];

          UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Clear"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [mainDelegate.CoinUser setEmail:@""];
                                 [mainDelegate.CoinUser setCountry:@""];
                                 [mainDelegate.CoinUser setCurrency:@""];
                                      [mainDelegate.CoinUser setIsPassword:false];
                                      [mainDelegate.CoinUser setPassword:@""];
                                 
                                  [alert dismissViewControllerAnimated:YES completion:nil];

                             }];
                  UIAlertAction* cancel = [UIAlertAction
                                  actionWithTitle:@"Cancel"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                      
                                      [alert dismissViewControllerAnimated:YES completion:nil];
                                 }];
          
          [alert addAction:ok];
          [alert addAction:cancel];
           [self presentViewController:alert animated:YES completion:nil];
    
}

- (IBAction)onClickSave:(id)sender
{
    NSString *email = @"";
    BOOL isSaved = TRUE;
    NSArray *cellsAll = [tblUser visibleCells];
    for (CellLabelText  *item in  cellsAll)
    {
        
        if([Utility validateEmail:item.txtField.text] ==1)
        {
            email =item.txtField.text;
            [mainDelegate.CoinUser setEmail:email];

        }
        else
        {
            isSaved = FALSE;
          
           
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Message" message: @"Email is incoorect." preferredStyle:UIAlertControllerStyleAlert];

          
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
           
            [alert addAction:ok];
         
            
            
        }
       
        [item hideNumberKeyboard];
        break;
    }
    if(isSaved)
    {
        [self updateTopLabel:@"Saved"];
    }
}

- (IBAction)onClickBack:(id)sender
{
    Settings *vc = [[Settings alloc] init];
    [self presentViewController: vc animated:YES completion:nil];
    [vc release];

}

#pragma mark Methods


-(IBAction)hideKeyboard:(id)sender
{
    NSString *email = @"";
    NSArray *cellsAll = [tblUser visibleCells];
    for (CellLabelText  *item in  cellsAll)
    {
        email= item.txtField.text;
        break;
    }
    [mainDelegate.CoinUser setEmail:email];
}


#pragma mark Alerts

- (void)alertView:(UIAlertController *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
}
#pragma mark Table

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)] autorelease];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, tableView.bounds.size.width, 30)];
    
    if(section==0)
    {
        [label setText:@"Application Settings"];
    }
    label.textColor = [UIColor blackColor];
    label.backgroundColor =[UIColor clearColor];
    [headerView addSubview:label];
    //[headerView setBackgroundColor:[Utility greyColor]];
    [label release];
    return headerView;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return 4;
}


- (id)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CustomCellIdentifier";
   // NSLog(@":%d", (int)indexPath.row);
    
    if(indexPath.row== 0)
    {
        CellLabelText *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            NSArray *topObject =  [[NSBundle mainBundle] loadNibNamed:@"CellLabelText" owner:self options:nil];
            
            for(id currentObject in topObject)
            {
                if ([currentObject isKindOfClass:[CellLabelText class]])
                {
                    cell = (CellLabelText*)currentObject;
                    break;
                }
            }
            // cell = [self itemCell];
            // [self setItemCell:nil];
        }
        
        PairKeyValue *tKey = (PairKeyValue*)[cells objectAtIndex:indexPath.row];
        [cell.lblLeft setFrame:CGRectMake(30, 8, 100, 30)];
        [cell.lblLeft setText: tKey.Key];
        [cell.txtField setFrame:CGRectMake(110, 8, 190, 30)];
        [cell.txtField setText:tKey.Value];
        
       // [cell.txtField setKeyboardType:UIKeyboardTypeEmailAddress];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if(indexPath.row== 1)
    {
        /*
        ItemCell *cellSwitch = [tableView dequeueReusableCellWithIdentifier:@"CustomCellIdentifier"];
        
        if (cellSwitch == nil)
        {
                       [[NSBundle mainBundle] loadNibNamed:@"ItemCell" owner:self options:nil];
            cellSwitch = [self itemCell];
            [self setItemCell:nil];
        }
        else
        {
            if (![cellSwitch isKindOfClass:[ItemCell class]])
            {
                [[NSBundle mainBundle] loadNibNamed:@"ItemCell" owner:self options:nil];
                cellSwitch = [self itemCell];
                [self setItemCell:nil];
            }
        }*/

        CellLabelLabel *cellSwitch = [tableView dequeueReusableCellWithIdentifier:@"CustomCellLabel"];
        
        if (cellSwitch == nil)
        {
            NSArray *topObject =  [[NSBundle mainBundle] loadNibNamed:@"CellLabelLabel" owner:self options:nil];
            
            for(id currentObject in topObject)
            {
                if ([currentObject isKindOfClass:[CellLabelLabel class]])
                {
                    cellSwitch = (CellLabelLabel*)currentObject;
                    break;
                }
            }
            // cell = [self itemCell];
            // [self setItemCell:nil];
        }
        
        //  cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        [cellSwitch.lblLabelRight setFrame:CGRectMake(210, 11, 80, 21)];
        [cellSwitch.lblLabelRight setTextAlignment:NSTextAlignmentRight];
        
        PairKeyValue *tKey = (PairKeyValue*)[cells objectAtIndex:indexPath.row];
        [cellSwitch.lblLabelLeft setFrame:CGRectMake(14, 8, 100, 30)];
        [cellSwitch.lblLabelLeft setFont:[UIFont italicSystemFontOfSize:14.0f]];
        
        cellSwitch.lblLabelLeft.text = tKey.Key;
        //[cell.txtField setFrame:CGRectMake(110, 8, 190, 30)];
        if ([tKey.Value isEqualToString:@"YES"])
        {
            [cellSwitch.lblLabelRight setText:@"ON"];
        }
        else
        {
            [cellSwitch.lblLabelRight setText:@"OFF"];

        }
       
        cellSwitch.selectionStyle = UITableViewCellSelectionStyleNone;
        return cellSwitch;

    }
    else
    {
        
        CellLabelLabel *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            NSArray *topObject =  [[NSBundle mainBundle] loadNibNamed:@"CellLabelLabel" owner:self options:nil];
            
            for(id currentObject in topObject)
            {
                if ([currentObject isKindOfClass:[CellLabelLabel class]])
                {
                    cell = (CellLabelLabel*)currentObject;
                    break;
                }
            }
            // cell = [self itemCell];
            // [self setItemCell:nil];
        }
     
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        [cell.lblLabelRight setFrame:CGRectMake(110, 11, 180, 21)];
        [cell.lblLabelRight setTextAlignment:NSTextAlignmentLeft];
        [cell.lblLabelLeft setFrame:CGRectMake(20, 8, 100, 30)];
        [cell.lblLabelLeft setText:[(PairKeyValue*)[cells objectAtIndex:indexPath.row] Key]];
        [cell.lblLabelRight setText:[(PairKeyValue*)[cells objectAtIndex:indexPath.row] Value]];
        return cell;
        
        
    }
    
    //[cell setItemIndex:indexPath.row];
    
    
    
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    
    NSString *email = @"";
    [self updateTopLabel:@"Edit"];
    NSArray *cellsAll = [tblUser visibleCells];
    for (CellLabelText  *item in  cellsAll)
    {
        email= item.txtField.text;
        break;
    }
   
    
    if (indexPath.row ==2)
    {
        [mainDelegate.CoinUser setEmailTemp:email];
        [mainDelegate setDropdownId:DROPDOWN_SETTINGS_COUNTRY];
        [mainDelegate setDropDownKey:[mainDelegate.CoinUser countryCode]];
        [mainDelegate setDropDownValue:[mainDelegate.CoinUser currency]];
        DropDownList *vc = [[DropDownList alloc] init];
        [self presentViewController: vc animated:YES completion:nil];
        [vc release];
        
        
    }
    else if (indexPath.row ==1)
    {
       Password *vc = [[Password alloc] init];
       [self presentViewController: vc animated:YES completion:nil];
       [vc release];

    }
    else if (indexPath.row ==3)
    {
        [mainDelegate.CoinUser setEmailTemp:email];
        
        [mainDelegate setDropdownId:DROPDOWN_SETTINGS_CURRENCY];
        [mainDelegate setDropDownKey:[mainDelegate.CoinUser currencyCode]];
        [mainDelegate setDropDownValue:[mainDelegate.CoinUser currency]];
        DropDownList *vc = [[DropDownList alloc] init];
        [self presentViewController: vc animated:YES completion:nil];
        [vc release];
    }
    else
    {
        
    }
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section==0)
    {
        return @"Default";
    }
    else if(section==1)
    {
        return @"Silver";
    }
    else if(section==2)
    {
        return @"Gold";
    }
    else if(section==3)
    {
        return @"Platinum";
    }
    else
    {
        return @"";
    }
}

    @end
