//
//  DropDownList.m
//  CoinBook
//
//  Created by Vadim Re on 2013-01-18.
//  Copyright (c) 2013 Vadim Re. All rights reserved.
//

#import "DropDownList.h"
#import "AppDelegate.h"
#import "Lookup.h"

#import "AddBookViewController.h"
#import "ItemViewController.h"
#import "UserInfo.h"

#define DROPDOWN_BOOK_COUNTRY 1
#define DROPDOWN_BOOK_CURRENCY 2
#define DROPDOWN_COIN_COUNTRY 3
#define DROPDOWN_COIN_CURRENCY 4
#define DROPDOWN_SETTINGS_COUNTRY 5
#define DROPDOWN_SETTINGS_CURRENCY 6


@interface DropDownList ()

@end


@interface State : NSObject



@property(nonatomic,copy) NSString *name;

@property(nonatomic,copy) NSString *capitol;

@property(nonatomic,copy) NSString *population;

@property NSInteger sectionNumber;

@end


@implementation DropDownList





- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
        self.modalPresentationStyle = UIModalPresentationFullScreen;
    }
    selectedRowIndex = 0;
    selectedSectionIndex = 0;
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    callScroll=TRUE;
    Lookup *lookup = [[Lookup alloc] init];
    dropDown = [[NSMutableArray alloc]init];
    if(mainDelegate.DropdownId==DROPDOWN_BOOK_COUNTRY || mainDelegate.DropdownId==DROPDOWN_COIN_COUNTRY || mainDelegate.DropdownId==DROPDOWN_SETTINGS_COUNTRY)
    {
            topNav.title = @"Country";
            [lookup populateLookup: @"country"  Content:dropDown];
    }
    else if(mainDelegate.DropdownId==DROPDOWN_BOOK_CURRENCY || mainDelegate.DropdownId==DROPDOWN_COIN_CURRENCY || mainDelegate.DropdownId==DROPDOWN_SETTINGS_CURRENCY)
    {
         [lookup populateLookup: @"currency"  Content:dropDown];
        topNav.title = @"Currency";
    }
    
     indices = [[dropDown valueForKey:@"headerTitle"] retain];
   // NSMutableArray *items;
    int arrayIndex = (int)[dropDown count];
    int sectionIndex;
    
    for (int section=0; section<arrayIndex; section++)
    {
       NSMutableArray *items = [[dropDown objectAtIndex:section] objectForKey:@"item"];
        //items = [[dropDown objectAtIndex:section] objectForKey:@"item"];
        sectionIndex = (int)[items count];
        for (int ind=0; ind<sectionIndex; ind++)
        {
            
            if([[mainDelegate DropDownKey] isEqualToString:[[items objectAtIndex:ind] key]])
            {
                selectedRowIndex = ind;
                selectedSectionIndex = section;
               
                break;
            }
           
        }
       
       // [items release];
    }
    [lookup release];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onClickBack:(id)sender
{
    
    if([mainDelegate DropdownId]==DROPDOWN_BOOK_COUNTRY || [mainDelegate DropdownId]==DROPDOWN_BOOK_CURRENCY){
        
        
            AddBookViewController *vc = [[AddBookViewController alloc] init];
            [self presentViewController: vc animated:YES completion:nil];
            [vc release];
        
    }
    else  if([mainDelegate DropdownId]==DROPDOWN_COIN_COUNTRY || [mainDelegate DropdownId]==DROPDOWN_COIN_CURRENCY){
        
        ItemViewController *vc = [[ItemViewController alloc] init];
        [self presentViewController: vc animated:YES completion:nil];
        [vc release];
        
    }
    else  if([mainDelegate DropdownId]==DROPDOWN_SETTINGS_COUNTRY || [mainDelegate DropdownId]==DROPDOWN_SETTINGS_CURRENCY){
        
        UserInfo *vc = [[UserInfo alloc] init];
        [self presentViewController: vc animated:YES completion:nil];
        [vc release];
        
    }

    
}

- (IBAction)onClickSelect:(id)sender
{
    if([mainDelegate DropdownId]==DROPDOWN_BOOK_COUNTRY || [mainDelegate DropdownId]==DROPDOWN_BOOK_CURRENCY){
        
        [mainDelegate setDropdownId:0];
        AddBookViewController *vc = [[AddBookViewController alloc] init];
        [self presentViewController: vc animated:YES completion:nil];
        [vc release];
        
    }
    else  if([mainDelegate DropdownId]==DROPDOWN_COIN_COUNTRY || [mainDelegate DropdownId]==DROPDOWN_COIN_CURRENCY)
    {
        [mainDelegate setDropdownId:0];
        ItemViewController *vc = [[ItemViewController alloc] init];
        [self presentViewController: vc animated:YES completion:nil];
        [vc release];
        
    }
    else  if([mainDelegate DropdownId]==DROPDOWN_SETTINGS_COUNTRY || [mainDelegate DropdownId]==DROPDOWN_SETTINGS_CURRENCY){
        
        [mainDelegate setDropdownId:0];
        UserInfo *vc = [[UserInfo alloc] init];
        [self presentViewController: vc animated:YES completion:nil];
        [vc release];
        
    }
   

    
}
- (void)dealloc {
    [tableDropDown release];
    [cell release];
    tableDropDown = nil;
    [dropDown release];
    [super dealloc];
}
//- (void)viewDidUnload {
//    [tableDropDown release];
//    tableDropDown = nil;
//    [dropDown release];
//    [cell release];
//    [super viewDidUnload];
//    
//}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return [dropDown count];
}

/*
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    if (sectionTitle == nil) {
        return nil;
    }
    
    // Create label with section title
    UILabel *label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(20, 6, 300, 30);
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithHue:(136.0/360.0)  // Slightly bluish green
                                 saturation:1.0
                                 brightness:0.60
                                      alpha:1.0];
    label.shadowColor = [UIColor whiteColor];
    label.shadowOffset = CGSizeMake(0.0, 1.0);
    label.font = [UIFont boldSystemFontOfSize:16];
    label.text = sectionTitle;
    
    // Create header view and add label as a subview
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, SectionHeaderHeight)];
    [view autorelease];
    [view addSubview:label];
    
    return view;
}*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
      return  [[[dropDown objectAtIndex:section] objectForKey:@"item"] count];
    //
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
     cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
       
    }
    
      
   // cell.textLabel.text = [NSString stringWithFormat:@"%@",[(Lookup*)[dropDown objectAtIndex:indexPath.row] value]];
    cell.textLabel.text = [(Lookup*)[[[dropDown objectAtIndex:indexPath.section] objectForKey:@"item"] objectAtIndex:indexPath.row] value];
    cell.textLabel.font = [UIFont systemFontOfSize:12.0];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    if(callScroll)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:selectedRowIndex inSection:selectedSectionIndex];
        [tableDropDown scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
        [tableDropDown selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionMiddle];

        callScroll= false;
    }
   
    return cell;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}


- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    //NSLog(@"%@", [dropDown valueForKey:@"headerTitle"]);
    return [dropDown valueForKey:@"headerTitle"];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return [indices indexOfObject:title];
}

- (NSString *)tableView:(UITableView *)aTableView titleForHeaderInSection:(NSInteger)section
{
     //NSLog(@"%@",  [[dropDown objectAtIndex:section] objectForKey:@"headerTitle"]);
	return [[dropDown objectAtIndex:section] objectForKey:@"headerTitle"];
    
}
/*
 - (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
 {
 return UITableViewCellEditingStyleInsert;
 }
 */
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
   // [mainDelegate setDropDownKey:[(Lookup*)[dropDown objectAtIndex:indexPath.row] key]];
    //[mainDelegate setDropDownValue:[(Lookup*)[dropDown objectAtIndex:indexPath.row] value]];
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        //[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
   //     [mainDelegate setCollection:[BookList  objectAtIndex:indexPath.row] ];
   //     AddBookViewController *vc = [[AddBookViewController alloc] init];
    //    [self presentViewController: vc animated:YES completion:nil];
    //    [vc release];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    
    
    [mainDelegate setDropDownKey:[(Lookup*)[[[dropDown objectAtIndex:indexPath.section] objectForKey:@"item"] objectAtIndex:indexPath.row] key]];
    [mainDelegate setDropDownValue:[(Lookup*)[[[dropDown objectAtIndex:indexPath.section] objectForKey:@"item"] objectAtIndex:indexPath.row] value]];
        
    
   }

/*
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    
    return [[UILocalizedIndexedCollation currentCollation] sectionIndexTitles];
    
}



- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if ([[self.states objectAtIndex:section] count] > 0) {
        
        return [[[UILocalizedIndexedCollation currentCollation] sectionTitles] objectAtIndex:section];
        
    }
    
    return nil;
    
}


- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index

{
    
    return [[UILocalizedIndexedCollation currentCollation] sectionForSectionIndexTitleAtIndex:index];
    
}
*/

@end
