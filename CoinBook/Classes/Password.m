//
//  Password.m
//  CoinBook
//
//  Created by Vadim Re AppDelegate.hon 2013-06-25.
//  Copyright (c) 2013 Vadim Re. All rights reserved.
//

#import "Password.h"
#import "UserInfo.h"
#import "Utility.h"
#import "AppDelegate.h"

@interface Password ()

@end

@implementation Password




- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
        self.modalPresentationStyle = UIModalPresentationFullScreen;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([mainDelegate.CoinUser getIsPassword])
    {
        swPassword.on=TRUE;
    }
    else
    {
        swPassword.on=FALSE;
    }
    [self updateTopLabel:@"Edit"];
    // Do any additional setup after loading the view from its nib.
    //topNav.title = @"Password";
   // topNavigation.topItem.title = @"Password";
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [txtPassword release];
    [txtPasswordRetype release];
    [swPassword release];
    swPassword = nil;
    [super dealloc];
}
//- (void)viewDidUnload {
//    
//    
//    [super viewDidUnload];
//}
- (IBAction)onClickSave:(id)sender
{
    BOOL isSaved = TRUE;
    if([txtPasswordRetype.text isEqualToString: txtPassword.text])
    {
        [mainDelegate.CoinUser setPassword:txtPassword.text];
    }
    else
    {
        isSaved = NO;
        [Utility showAlertOneButton:@"Retype password mismatch to password" alertTag:128];
         
    }
    if (swPassword.on && isSaved)
    {
        [mainDelegate.CoinUser setIsPassword:YES];
    }
    else
    {
          [mainDelegate.CoinUser setIsPassword:NO];
    }
    if(isSaved)
    {
        [self updateTopLabel:@"Saved"];
       // topNav.title = @"Saved";
    }
}

- (IBAction)onClickBack:(id)sender
{
    UserInfo *vc = [[UserInfo alloc] init];
    [self presentViewController: vc animated:YES completion:nil];
    [vc release];
}

- (IBAction)onClickClear:(id)sender
{
    txtPassword.text=@"";
    txtPasswordRetype.text = @"";
}

- (IBAction)hideKeyboard:(id)sender
{
    [txtPassword resignFirstResponder];
    [txtPasswordRetype resignFirstResponder];
}

@end
