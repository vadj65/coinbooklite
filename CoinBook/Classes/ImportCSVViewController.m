//
//  ImportCSVViewController.m
//  CoinBook
//
//  Created by Vadim Re on 2012-12-06.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import "ImportCSVViewController.h"
#import "Item.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "CollectionListViewController.h"
#import "Collection.h"
#import "Utility.h"

#define ALERT_IMPORT 1

@interface ImportCSVViewController ()

@end

@implementation ImportCSVViewController


NSString *sortField;
NSString *collectionId;
BOOL callScroll;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
        BookList=[[NSMutableArray alloc] init];
        sortField = [[NSString alloc] initWithString:@"name"];
        [self populateCollection];
        // Custom initialization
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    callScroll = TRUE;
    if ([mainDelegate CSVText] != nil)
    {
       [mainDelegate setCsvArrayApp: [mainDelegate.CSVText csvRows]];
        if ([mainDelegate fileName] != nil)
        {
            
       
        if([mainDelegate fileName].length> 0)
        {
            NSString *prepFileName = mainDelegate.fileName;
            txtNewBook.text = [prepFileName stringByDeletingPathExtension];
            lblFileName.text = prepFileName;
        }
        
        }
        topNav.title = @"Import";
        collectionId =@"0";
    }

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onClickAddNewBook:(id)sender
{
    //uncomment to get the time only
    //[formatter setDateFormat:@"hh:mm a"];
    //[formatter setDateFormat:@"MMM dd, YYYY"];
    //[formatter setDateStyle:NSDateFormatterMediumStyle];
    
    Collection *newCollection = [[Collection alloc] init];
    [mainDelegate setCollection:newCollection];
    [mainDelegate.Collection setName: txtNewBook.text];
    [mainDelegate.Collection setIsUseItemName:0];
    [mainDelegate.Collection setIsYearRange:0];
    [mainDelegate.Collection setPreciousMetal:0];
    [mainDelegate.Collection setWeightUnit:0];
    
    
    [mainDelegate.Collection setDateCreated:[NSDate date]];
    [mainDelegate.Collection insertCollection];
    [self populateCollection];
    [tblCoinBooks reloadData];
    topNav.title = @"Book added";
    
   
    lblBookName.text = [mainDelegate.Collection Name];
    collectionId =[[NSString alloc]initWithFormat:@"%@" , [mainDelegate.Collection CollectionId]];
    
        

}
- (IBAction)onClickImport:(id)sender
{
    if ( mainDelegate.Collection.CollectionId >0)
    {
        [self showAlert:@"Import"];
    }
    else
    {
        [Utility showAlertOneButton:@"Select or create new collection." alertTag:128 ];
    }
}

- (IBAction)onClickBack:(id)sender
{
    
    [ mainDelegate setCSVText:@""];
    [mainDelegate setAppOptions:0];
    
    CollectionListViewController *vc = [[CollectionListViewController alloc] init];
    [self presentViewController: vc animated:YES completion:nil];
    [vc release];
 

}

#pragma mark Import CSV
/*
-(void) importToBook:(NSURL *)url {
    NSError *outError;
    NSString *fileString = [NSString stringWithContentsOfURL:url
                                                    encoding:NSUTF8StringEncoding error:&outError];
 
}
*/


-(int) csvArray2PresidentsArray:(NSArray *)csvArray {
    
    int i = 0;
   // NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
  //  [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
  //  NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
   // [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
  
   
    int inserted = 0;
    int rows = 0;
    int colNext = 3;
    for (NSArray *row in csvArray)
    {
        rows = (int)[row count];
        
     //   countRow = [row count];
        if(rows==18 || rows==19)
        {
            
        //1st row is a header - skip
            if (i > 0)
            {
            
                if(rows ==18)
                {
                    colNext = 2;
                }
                Item *item = [[Item alloc] init];
            
                item.Name =  [[row objectAtIndex:0] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
                item.Year = [[row objectAtIndex:1] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
                item.PurchaseDate = [[row objectAtIndex:2] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
                item.FaceValue = [[row objectAtIndex:colNext+1] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
                item.Description = [[row objectAtIndex:colNext+2] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
                if(item.Description== nil)
                {
                    item.Description = @"";
                }
                [item setStatus:[[[row objectAtIndex:colNext+4] stringByReplacingOccurrencesOfString:@"\"" withString:@""] intValue]];
                if(rows ==19)
                {
                    item.Price = [[row objectAtIndex:colNext]  stringByReplacingOccurrencesOfString:@"\"" withString:@""];
                }
                item.Grade = [[[row objectAtIndex:colNext+5] stringByReplacingOccurrencesOfString:@"\"" withString:@"" ] intValue];
                item.CountryCode = [[row objectAtIndex:colNext+7]  stringByReplacingOccurrencesOfString:@"\"" withString:@""];
                item.CurrencyCode = [[row objectAtIndex:colNext+9]  stringByReplacingOccurrencesOfString:@"\"" withString:@""];
                item.PreciousMetal = [[[row objectAtIndex:colNext+11] stringByReplacingOccurrencesOfString:@"\"" withString:@""] intValue];
                item.Weight = [[[row objectAtIndex:colNext+12] stringByReplacingOccurrencesOfString:@"\"" withString:@""] floatValue];
                item.WeightUnit = [[[row objectAtIndex:colNext+13] stringByReplacingOccurrencesOfString:@"\"" withString:@""] intValue];
                item.Material = [[row objectAtIndex:colNext+15] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
                item.CollectionId = [[mainDelegate.Collection CollectionId] intValue];
             
                [item insertItem];
                if([[mainDelegate.DataBase sqlStatus] isEqualToString:@"1"])
                {
                    inserted = inserted+1;
                }
                [item release];
            }
        }
        else
        {
            break;
        } 
        i++;
    }
    return inserted;
}

- (void)dealloc {
    [lblBookName release];
    [tblCoinBooks release];
    [BookList release];
    [sortField release];
    [txtNewBook release];
   
    [lblFileName release];
    
    [lblBookName release];
    lblBookName = nil;
    [tblCoinBooks release];
    tblCoinBooks = nil;
    [BookList release];
    [sortField release];
    
    [txtNewBook release];
    txtNewBook = nil;
    [lblFileName release];
    lblFileName = nil;
    [super dealloc];
}
//- (void)viewDidUnload {
//    
//       [super viewDidUnload];
//}

-(void)populateCollection
{
    [BookList removeAllObjects];
    Collection *execColl = [[Collection alloc]init];
    
    [execColl getAllCollections:@"1" sortOrder: sortField CollectionList:BookList];
    [execColl release];
    
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return [BookList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
 
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@(%@)",[(Collection*)[BookList objectAtIndex:indexPath.row] Name], [(Collection*)[BookList objectAtIndex:indexPath.row] ItemCount]];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    Collection *col = (Collection*)[BookList objectAtIndex:indexPath.row];
     cell.selectionStyle = UITableViewCellSelectionStyleGray;

    cell.textLabel.font=[UIFont fontWithName:@"Arial Rounded MT Bold" size:14.0];
    if(callScroll && [col.CollectionId isEqualToString: collectionId])
    {
        NSIndexPath *indexThisPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
        [tblCoinBooks scrollToRowAtIndexPath:indexThisPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
        [tblCoinBooks selectRowAtIndexPath:indexThisPath animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        
        callScroll= false;
    }
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 { [mainDelegate setCollection:[BookList  objectAtIndex:indexPath.row] ];

 if (isEdit)
 {
 
 AddBookViewController *vc = [[AddBookViewController alloc] init];
 [self presentViewController: vc animated:YES completion:nil];
 [vc release];
 }
 else if (isDelete)
 {
 UIAlertView *alert = [[UIAlertView alloc] initWithFrame:CGRectMake(20, 320, 280, 160)];
 
 //[alert setTitle:winnerName];
 CGAffineTransform myTransform = CGAffineTransformMakeTranslation(0, -100);
 
 [alert setTransform:myTransform];
 
 [alert setMessage: [NSString stringWithFormat:@"Delete collection %@", [mainDelegate.Collection Name]]];
 [alert setDelegate:self];
 [alert addButtonWithTitle:@"DELETE"];
 [alert addButtonWithTitle:@"CANCEL"];
 [alert setTag:ALERT_DELETE];
 [alert show];
 [alert release];
 
 }
 else
 {
 [mainDelegate setAppPath:2];
 CollectionItemsViewController *vc = [[CollectionItemsViewController alloc] init];
 [self presentViewController: vc animated:YES completion:nil];
 [vc release];
 }
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    
    [mainDelegate setCollection:[BookList  objectAtIndex:indexPath.row] ];
    lblBookName.text = [mainDelegate.Collection Name];
    collectionId =[[NSString alloc]initWithFormat:@"%@" , [mainDelegate.Collection CollectionId]];
   
   }


#pragma mark Alerts




- (void)alertView:(UIAlertController *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == 0)
	{
        if (alertView.view.tag ==ALERT_IMPORT)
        {
            if ([mainDelegate CSVText] != nil)
            {
             
              int ma =  [self csvArray2PresidentsArray:[mainDelegate csvArrayApp ]];
                if(ma == 0)
                {
                    [Utility showAlertOneButton:@"CSV file has invalid format. Cell should not have , " alertTag:22];
                }
                else if (ma <= [[mainDelegate csvArrayApp ] count]-1)
                {
                    int allItems = (int)[[mainDelegate csvArrayApp ] count]-1;
                    [Utility showAlertOneButton:[NSString stringWithFormat:@"Imported %d of total %d items.", ma, allItems] alertTag:128 ];
                    [self populateCollection ];
                    [tblCoinBooks reloadData];

                }
                [mainDelegate setCSVText:nil];
                [mainDelegate setCsvArrayApp:nil ];
            }
        }
    else if (alertView.view.tag ==23)
    {
        //[mainDelegate.CSVText release];
        //[mainDelegate.csvArrayApp release];
        
       // CollectionItemsViewController *vc = [[CollectionItemsViewController alloc] init];
       // [self presentViewController: vc animated:YES completion:nil];
        //[vc release];

    }
    
    
		
	}/////////////////////////////////////////////second button/////////////////////////////////////////////
	else if (buttonIndex == 1)
	{
		
	}
	//////////////////////////////////////////////THIRD BUTTON///////////////////////////////////
	else if (buttonIndex == 2)
	{
        
		
	}
    
    
   
}

-(void)showAlert:(NSString*)topic
{
    //UIAlertController *alert = [[UIAlertController alloc] initWithFrame:CGRectMake(20, 320, 280, 160)];
    
    //[alert setTitle:winnerName];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat: @"Import to book: %@ ", [mainDelegate.Collection Name]] message: @"" preferredStyle:UIAlertControllerStyleAlert];


    
    UIAlertAction *btnContinue = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([mainDelegate CSVText] != nil)
        {
         
          int ma =  [self csvArray2PresidentsArray:[mainDelegate csvArrayApp ]];
            if(ma == 0)
            {
                [Utility showAlertOneButton:@"CSV file has invalid format. Cell should not have , " alertTag:22];
            }
            else if (ma <= [[mainDelegate csvArrayApp ] count]-1)
            {
                int allItems = (int)[[mainDelegate csvArrayApp ] count]-1;
                [Utility showAlertOneButton:[NSString stringWithFormat:@"Imported %d of total %d items.", ma, allItems] alertTag:128 ];
                [self populateCollection ];
                [tblCoinBooks reloadData];

            }
            [mainDelegate setCSVText:nil];
            [mainDelegate setCsvArrayApp:nil ];
        };
    } ];
    [alert addAction:btnContinue];
   
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil ];
    [alert addAction:cancel];
    //[alert setTag:ALERT_UPDATE_YEAR];
    
    
   
    [alert release];
}
+(void)showAlertOneButton:(NSString*)message
{
  //  UIAlertView *alert = [[UIAlertView alloc] initWithFrame:CGRectMake(20, 320, 280, 160)];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat: @"%@ ", message] message: @"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *btnContinue = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //[mainDelegate.CSVText release];
        //[mainDelegate.csvArrayApp release];
        
       // CollectionItemsViewController *vc = [[CollectionItemsViewController alloc] init];
       // [self presentViewController: vc animated:YES completion:nil];
        //[vc release];
    } ];
    [alert addAction:btnContinue];
   
//    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil ];
//    [alert addAction:cancel];
//    //[alert setTitle:winnerName];
  
   
    //[alert setTag:23];
    
    [alert release];
}


- (IBAction)HideKeyboard:(id)sender
{
    [txtNewBook resignFirstResponder];
}
@end
