//
//  HelpAnswer.h
//  CoinBook
//
//  Created by Vadim Re on 2013-02-07.
//  Copyright (c) 2013 Vadim Re. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpAnswer : UIViewController
{
    
    IBOutlet UINavigationItem *topNav;
    IBOutlet UITextView *txtHelp;
}
- (IBAction)onClickMainMenu:(id)sender;
- (IBAction)onClickBack:(id)sender;


@end
