
//
//  ItemViewController.m
//  CoinBook
//
//  Created by Vadim Re on 2012-11-08.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import "ItemViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "Utility.h"
#import "AllItems.h"
#import "DropDownList.h"
#import "Item.h"
#import "TakePicture.h"
#import "AppDelegate.h"

#define ALERT_COPY 1
#define ALERT_DELETE 2
#define ALERT_PURCHASEDATE 3
#define ALERT_COUNTRY 4
#define ALERT_DATEOUT 5
#define TAG_COUNTRY 6
#define TAG_CURRENCY 7
#define DROPDOWN_COIN_COUNTRY 3
#define DROPDOWN_COIN_CURRENCY 4
#define REQUESTER_GRAM 1
#define REQUESTER_OZ 2
#define MESSAGE_Item_Copied Copied
#define ALERT_MESSAGE_Save_first @"Save first."



@interface ItemViewController ()

@end

@implementation ItemViewController

@synthesize birthDatePickerView;

@synthesize txtName, txtDescription;




//BOOL addDone;

- (void)onDatePickerValueChanged:(UIDatePicker *)datePicker
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:(NSString*)DateFormat];
    txtPurchaseDate.text = [formatter stringFromDate:datePicker.date];
}
- (void)onDatePickerDateOutValueChanged:(UIDatePicker *)datePicker
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:(NSString*)DateFormat];
    txtDateOut.text = [formatter stringFromDate:datePicker.date];
   // [self hideKeyboard:self];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
         mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
        self.modalPresentationStyle = UIModalPresentationFullScreen;
            }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

     
    [self.view bringSubviewToFront:uiToolBar];
  
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideNumberKeyboard)];
    
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    
    [scroller addGestureRecognizer:tapGesture];

    self.view.backgroundColor = UIColor.whiteColor;// viewFlipsideBackgroundColor];
    
    [scroller setCanCancelContentTouches:NO];
    
    scroller.indicatorStyle = UIScrollViewIndicatorStyleBlack;
    
   // scroller.clipsToBounds = YES;        // default is NO, we want to restrict drawing within our scrollview
    [self.view setMultipleTouchEnabled:YES];

    scroller.scrollEnabled = YES;
    [scroller addSubview:scrView];
    
    [scroller setContentSize:CGSizeMake(320, scrView.frame.size.height)];
    
    // POPULATE SCREEN
    
    [self populateScreen];
    
   
   
    txtCountry.delegate = self;
    // Do any additional setup after loading the view from its nib.
    
    [self setupGesture];
    
    [self registerForKeyboardNotifications];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:(NSString*)DateFormat];
    NSString *convertedString = [formatter stringFromDate:[[NSDate alloc] init]];
    NSDate *purchaseDate = nil;
    
    UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolBar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(hideKeyboard:)];
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];
   
    UIToolbar *toolBarOut=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolBarOut setTintColor:[UIColor grayColor]];
    UIBarButtonItem *doneBtnOut=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(hideKeyboard:)];
    UIBarButtonItem *spaceOut=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBarOut setItems:[NSArray arrayWithObjects:spaceOut,doneBtnOut, nil]];
    //Purchase date
    if(txtPurchaseDate.text.length > 0)
    {
        convertedString=txtPurchaseDate.text;
    }
    dobPicker = [[UIDatePicker alloc] init];
    dobPicker.frame=CGRectMake(20, 120.0, 240.0, 150.0);
    dobPicker.datePickerMode = UIDatePickerModeDate;
    purchaseDate =[formatter dateFromString:convertedString];
    if(purchaseDate== nil)
    {
        [formatter setDateFormat:(NSString*)DateFormatOld];
        purchaseDate =[formatter dateFromString:convertedString];
        if(purchaseDate== nil)
        {
            [formatter setDateFormat:(NSString*)DateFormat];
            convertedString = [formatter stringFromDate:[[NSDate alloc] init]];
            purchaseDate =[formatter dateFromString:convertedString];
        }
    }
    [dobPicker addTarget:self  action:@selector(onDatePickerValueChanged:) forControlEvents:(UIControlEventValueChanged)];
    [dobPicker addTarget:self  action:@selector(onDatePickerValueChanged:) forControlEvents:(UIControlEventEditingDidEnd)];
    [dobPicker setDate:purchaseDate];
    [txtPurchaseDate setInputAccessoryView:toolBarOut];
    txtPurchaseDate.delegate = self;
    txtPurchaseDate.inputView = dobPicker;
    
    //Date out
    convertedString = [formatter stringFromDate:[[NSDate alloc] init]];
    if(txtDateOut.text.length > 0)
    {
        convertedString=txtDateOut.text;
    }
    dobDOPicker = [[UIDatePicker alloc] init];
    dobDOPicker.frame=CGRectMake(20, 120.0, 240.0, 150.0);
    dobDOPicker.datePickerMode = UIDatePickerModeDate;
    purchaseDate =[formatter dateFromString:convertedString];
    if(purchaseDate== nil)
    {
        [formatter setDateFormat:(NSString*)DateFormatOld];
        purchaseDate =[formatter dateFromString:convertedString];
        if(purchaseDate== nil)
        {
            [formatter setDateFormat:(NSString*)DateFormat];
            convertedString = [formatter stringFromDate:[[NSDate alloc] init]];
            purchaseDate =[formatter dateFromString:convertedString];
        }
    }
    [dobDOPicker addTarget:self  action:@selector(onDatePickerDateOutValueChanged:) forControlEvents:(UIControlEventEditingDidEnd)];
    
    [dobDOPicker setDate:purchaseDate];
  

    [txtDateOut setInputAccessoryView:toolBarOut];
    
    txtDateOut.inputView = dobDOPicker;
    txtDateOut.delegate = self;
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)dealloc {
//    [txtName release];
//    [txtDescription release];
//    [_btnAdd release];
//    [_btnBack release];
//    [uiToolBar release];
//    [txtYear release];
//    [lblMessage release];
//    [selStatus release];
//    [txtFaceValue release];
//    [txtCountry release];
//    [selGrade release];
//    [tapGesture release];
//    [topToolBar release];
//    [swPreciousMetal release];
//    [lblItemCurrentPrice release];
//    [txtWeight release];
//    [txtPrice release];
//    [scrView release];
//    [txtMaterial release];
//    [msgView release];
//    [txtPurchaseDate release];
//    [txtPurchaseDate release];
//    [selWeightUnit release];
//
//    [txtCurrency release];
//
//    [txtDateOut release];
//    [super dealloc];
//}
/**/

#pragma mark - Methods
- (IBAction)onClickBack:(id)sender
{
    [self goBack];
}

- (IBAction)onClickTakePhoto:(id)sender
{
    if([[mainDelegate.Item ItemId]intValue] > 0)
    {
        TakePicture *vc = [[TakePicture alloc] init];
        [self presentViewController: vc animated:YES completion:nil];
        [vc release];
    }
    else
    {
        [Utility showAlertOneButton:@"Before take a picture save the coin." alertTag:128];
    }
}



- (IBAction)onClickAdd:(id)sender
{
}

- (IBAction)onClickSave:(id)sender
{
       [self updateItem:@""];
   
}


- (IBAction)onClickEmail:(id)sender
{
    if ([mainDelegate.Item ItemId] > 0)
    {

    
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        
        mailer.mailComposeDelegate = self;
        [mailer setBccRecipients:[NSArray  arrayWithObjects:@"" , nil]];
        [mailer setToRecipients:[NSArray arrayWithObjects: [Utility getPreference:@"coinEmail"], nil]];
        [mailer setSubject:[mainDelegate.Collection Name]];
        
        NSMutableString *emailBody = [[NSMutableString alloc] init] ;
        [emailBody appendFormat:@"Name:\t %@", txtName.text];
        [emailBody appendFormat:@"\nDescription:\t %@", txtDescription.text];
        NSString *status = @"";
        NSString *weightUnit = @"";
    
        NSString *metal=@"";
    
        switch (selStatus.selectedSegmentIndex) {
            case 0:
                status= @"New";
                break;
            case 1:
                status= @"Added";
                break;
            case 2:
                status= @"Sold";
                break;
            case 3:
                status= @"Traded";
                break;
                
            default:
                break;
        }
        
        
        switch (swPreciousMetal.selectedSegmentIndex)
        {
            case 0:
                metal= @"None";
                break;
            case 1:
                metal= @"Silver";
                break;
            case 2:
                metal= @"Gold";
                break;
            case 3:
                metal= @"Platinum";
                break;
        }
    switch (selWeightUnit.selectedSegmentIndex)
    {
        case 0:
            weightUnit= @"Gramm";
            break;
        case 1:
            weightUnit= @"Ounce";
            break;
          }

    
    [emailBody appendFormat:@"\nYear:\t %@", txtYear.text];
    [emailBody appendFormat:@"\nPurchase date:\t %@", txtPurchaseDate.text];
    [emailBody appendFormat:@"\nFace Value:\t %@", txtFaceValue.text];
    [emailBody appendFormat:@"\nStatus:\t %@", status];
    [emailBody appendFormat:@"\nPrice:\t %@", txtPrice.text];
    [emailBody appendFormat:@"\nGrade:\t %d", (int)selGrade.selectedSegmentIndex+1];
    [emailBody appendFormat:@"\nCountry:\t %@", [mainDelegate.Item Country]];
    [emailBody appendFormat:@"\nCountry Code:\t %@", [mainDelegate.Item CountryCode]];
    [emailBody appendFormat:@"\nCurrency:\t %@", [mainDelegate.Item Currency]];
    [emailBody appendFormat:@"\nCurrency Code:\t %@", [mainDelegate.Item CurrencyCode]];
    [emailBody appendFormat:@"\nPreciousMetal:\t %@", metal];
    [emailBody appendFormat:@"\nWeight:\t %@", txtWeight.text];
    [emailBody appendFormat:@"\nWeight unit:\t %@", weightUnit];
    
    
        
                 
      
        [mailer setMessageBody:emailBody isHTML:NO];
        
        
        [self presentViewController: mailer animated:YES completion: nil];
        //[item release];
        
        [mailer release];
       
        topNav.title = @"Coin is sent";
        [emailBody release];
    }
    else
    {
        [Utility showAlertOneButton:ALERT_MESSAGE_Save_first alertTag:128];
    }
}

-(void)updateItem:(NSString*)newItemId
{
    if ([self validate])
    {
        
    
    if (newItemId.length >0)
    {
        [mainDelegate.Item setItemId:@""];
    }
    [mainDelegate.Item setName:txtName.text];
    [mainDelegate.Item setYear:txtYear.text];
    [mainDelegate.Item setDescription:txtDescription.text];
    [mainDelegate.Item setFaceValue:txtFaceValue.text];
    
    [mainDelegate.Item setPrice:txtPrice.text];
    [mainDelegate.Item setPurchaseDate:txtPurchaseDate.text];
    [mainDelegate.Item setMaterial:txtMaterial.text];
    [mainDelegate.Item setStatus: (int)selStatus.selectedSegmentIndex];
    [mainDelegate.Item setGrade: (int)selGrade.selectedSegmentIndex];
    [mainDelegate.Item setWeight: [txtWeight.text floatValue]];
    [mainDelegate.Item setWeightUnit: (int)selWeightUnit.selectedSegmentIndex];
    
    [mainDelegate.Item setDateOut:txtDateOut.text];
    [mainDelegate.Item setPreciousMetal:(int)swPreciousMetal.selectedSegmentIndex];
    [mainDelegate.Item setCollectionId:[[mainDelegate.Collection CollectionId] intValue]];
    if(mainDelegate.Item.ItemId.length ==0)
    {
        [mainDelegate.Item insertItem];
        lblMessage.text = @"Item inserted.";
        
        
        topToolBar.title = @"Inserted";
        [mainDelegate.Collection setItemCount:[NSString stringWithFormat:@"%d", [mainDelegate.Collection.ItemCount intValue]+1 ]];
        
    }
    else
    {
        [mainDelegate.Item updateItem];
        lblMessage.text = @"Item updated.";
        topToolBar.title = @"Updated";
        
    }
    if([[mainDelegate.DataBase sqlStatus] isEqualToString:@"0"])
    {
        lblMessage.text = @"Item is not updated";
    }
    else
    {
        lblMessage.text=[mainDelegate.DataBase sqlStatus];
    }
    [self populateCoinWeightPrice];
    [self.view bringSubviewToFront:msgView];
    [self hideKeyboard:self];
        }
    else
    {
        [Utility showAlertOneButton:@"Name is requiered" alertTag:128];
    }
}

-(IBAction)hideKeyboard:(id)sender
{
	[txtName  resignFirstResponder];
	[txtYear  resignFirstResponder];
	[txtDescription  resignFirstResponder];
	[txtFaceValue  resignFirstResponder];
    [txtCountry resignFirstResponder];
    [txtPrice resignFirstResponder];
    [txtMaterial resignFirstResponder];
    [txtWeight resignFirstResponder];
    [txtPurchaseDate resignFirstResponder];
    [txtDateOut resignFirstResponder];
    
}


- (IBAction)onClickDelete:(id)sender
{
    [self showAlert:@"Delete item?" alertTag:ALERT_DELETE];
    
}

- (IBAction)onClickCopy:(id)sender
{
    if([mainDelegate.Item ItemId]> 0)
    {
    if ([mainDelegate.ItemList count] >=COINS_MAX)
    {
        [Utility showAlertOneButton:@"The maximum pockets for this book has been reached. Create another book." alertTag:128];
    }
    else
    {
        [self showAlert:@"Duplicate item?" alertTag:ALERT_COPY];
        
    }
    }
    else
    {
        [Utility showAlertOneButton:ALERT_MESSAGE_Save_first alertTag:128];
    }

}

- (IBAction)onClickClear:(id)sender
{
}

- (IBAction)onClickPurchaseDate:(id)sender
{
    dobPicker = [[UIDatePicker alloc] init];
    dobPicker.frame=CGRectMake(20, 120.0, 240.0, 150.0);
    dobPicker.datePickerMode = UIDatePickerModeDateAndTime;
    
    [dobPicker setDate:[NSDate date]];
    
    //UIAlertView *dAlert =[[UIAlertView alloc] initWithTitle:@"Select Purchase Date" message:@"nnnnnnn" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Camncel", nil ];
   
    UIAlertController *dAlert = [UIAlertController alertControllerWithTitle:@"Select Purchase Date" message: @"" preferredStyle:UIAlertControllerStyleAlert];
    //[dAlert setTransform:myTransform];
   
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil ];
    [dAlert addAction:ok];
   ;
    dAlert.view.tag= ALERT_PURCHASEDATE;

    
    
   // dAlert =
    //dAlert.delegate = self;
   // [dAlert addSubview:dobPicker];
  
    [dAlert release];
}

- (IBAction)onClickDateOut:(id)sender
{
    dobPicker = [[UIDatePicker alloc] init];
    dobPicker.frame=CGRectMake(20, 120.0, 240.0, 150.0);
    dobPicker.datePickerMode = UIDatePickerModeDateAndTime;
    
    [dobPicker setDate:[NSDate date]];
    
    //UIAlertView *dAlert =[[UIAlertView alloc] initWithTitle:@"Select Purchase Date" message:@"nnnnnnn" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Camncel", nil ];
    // CGAffineTransform myTransform = CGAffineTransformMakeTranslation(0, -5);
    
     UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Select Date " message: @"" preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancel];
    [alert addAction:ok];
    alert.view.tag = ALERT_DATEOUT;
    
    // dAlert =
    //dAlert.delegate = self;
    //[alert addSubview:dobPicker];
   
    
    
    
}

- (IBAction)onClickWeightUnitChanged:(id)sender
{
    [self populateCoinWeightPrice];
}



#pragma mark Item's methods
-(BOOL)validate
{
    BOOL valid= TRUE;
    if (txtName.text.length == 0)
    {
        valid = FALSE;
    }
    return valid;
}
-(void)populateCoinWeightPrice
{
    float metalPriceGr=0;
    float metalPriceOz=0;
    
    switch (swPreciousMetal.selectedSegmentIndex)
    {
        case 1:
            [mainDelegate.MyMarket setRequester:REQUESTER_GRAM];
            metalPriceGr = [mainDelegate.MyMarket getSilverGr];
            [mainDelegate.MyMarket setRequester:REQUESTER_OZ];
            metalPriceOz = [mainDelegate.MyMarket getSilverOz];
            break;
        case 2:
            [mainDelegate.MyMarket setRequester:REQUESTER_GRAM];
            metalPriceGr = [mainDelegate.MyMarket getGoldGr];
            [mainDelegate.MyMarket setRequester:REQUESTER_OZ];
            metalPriceOz = [mainDelegate.MyMarket getGoldOz];
            break;
        case 3:
            [mainDelegate.MyMarket setRequester:REQUESTER_GRAM];
            metalPriceGr = [mainDelegate.MyMarket getPlatinumGr];
            [mainDelegate.MyMarket setRequester:REQUESTER_OZ];
            metalPriceOz = [mainDelegate.MyMarket getPlatinumOz];
            break;
            
        default:
            break;
    }
    if (selWeightUnit.selectedSegmentIndex == 0)
    {
        lblItemCurrentPrice.text = [NSString stringWithFormat:@"%1.2f", metalPriceGr * [txtWeight.text intValue]];
        
    } else
    {
        lblItemCurrentPrice.text = [NSString stringWithFormat:@"%1.2f",  metalPriceOz* [txtWeight.text intValue]];
        
    }

}
-(void)populateScreen
{
    
    txtPurchaseDate.tag = ALERT_PURCHASEDATE;
    txtDateOut.tag = ALERT_DATEOUT;
    txtCountry.tag =TAG_COUNTRY;
    txtYear.tag = TAG_NUMBERKEYBOARD;
    txtFaceValue.tag = TAG_NUMBERKEYBOARD;
    if(mainDelegate.DropdownId > 0)
    {
        [self restoreCurrentState];
        if (mainDelegate.DropdownId == DROPDOWN_COIN_COUNTRY)
        {
            
            [mainDelegate.Item setCountry:mainDelegate.DropDownValue];
            [mainDelegate.Item setCountryCode:mainDelegate.DropDownKey];
            CGPoint scrollPoint = CGPointMake(0.0, txtCountry.frame.origin.y);
            [scroller setContentOffset:scrollPoint animated:YES];

            
        }
        else if (mainDelegate.DropdownId== DROPDOWN_COIN_CURRENCY)
        {
            
            [mainDelegate.Item setCurrency:mainDelegate.DropDownValue];
            [mainDelegate.Item setCurrencyCode:mainDelegate.DropDownKey];
            CGPoint scrollPoint = CGPointMake(0.0, txtCountry.frame.origin.y);
            [scroller setContentOffset:scrollPoint animated:YES];

            
        }
        
    }

    if ([mainDelegate.Item ItemId].length >0 || [mainDelegate DropdownId] >0)
    {
        if ([[mainDelegate.Item Year] isEqualToString:@"0"])
        {
            txtYear.text =@"";
        }
        else
        {
            txtYear.text = [mainDelegate.Item Year];
        }
        txtName.text = [mainDelegate.Item Name];
        
        txtDateOut.text =[mainDelegate.Item DateOut];
        txtFaceValue.text =[mainDelegate.Item FaceValue];
        txtDescription.text = [mainDelegate.Item Description];
        if([[mainDelegate.Item Price] intValue] == 0)
        {
            txtPrice.text = @"";
        }
        else
        {
            txtPrice.text = [NSString stringWithFormat:@"%1.2f",[mainDelegate.Item.Price floatValue]];
        }
        
        txtMaterial.text = [mainDelegate.Item Material];
        txtPurchaseDate.text = [mainDelegate.Item PurchaseDate];
        selStatus.selectedSegmentIndex = [mainDelegate.Item Status] ;
        if([mainDelegate.Item Weight] == 0)
        {
            txtWeight.text =@"";
        }
        else
        {
         txtWeight.text =[NSString stringWithFormat:@"%1.3f" ,[mainDelegate.Item Weight]];   
        }
        
        selGrade.selectedSegmentIndex= [mainDelegate.Item Grade] ;
        topToolBar.title = @"Edit";
        swPreciousMetal.selectedSegmentIndex = [mainDelegate.Item PreciousMetal];
        
        selWeightUnit.selectedSegmentIndex = [mainDelegate.Item WeightUnit];
        [self populateCoinWeightPrice];
    }
    else
    {
        // txtYear.text = [mainDelegate.Collection Year];
        txtName.text = [mainDelegate.Collection CoinName];
        [mainDelegate.Item setCountry: [mainDelegate.Collection Country]];
        [mainDelegate.Item setCurrency: [mainDelegate.Collection Currency]];
        [mainDelegate.Item setCountryCode: [mainDelegate.Collection CountryCode]];
        [mainDelegate.Item setCurrencyCode: [mainDelegate.Collection CurrencyCode]];
        
        txtFaceValue.text =[mainDelegate.Collection FaceValue];
        txtDescription.text = [mainDelegate.Collection Description];
        txtMaterial.text = [mainDelegate.Collection Material];
       
        if ([mainDelegate.Collection Weight] == 0)
        {
            txtWeight.text =@"";
        } else
        {
            txtWeight.text =[NSString stringWithFormat:@"%1.3f" ,[mainDelegate.Collection Weight]];
            
        }
        
        swPreciousMetal.selectedSegmentIndex = [mainDelegate.Collection PreciousMetal];
        selWeightUnit.selectedSegmentIndex=[mainDelegate.Collection WeightUnit];
        selStatus.selectedSegmentIndex = 0;
        selGrade.selectedSegmentIndex = 0;
        topToolBar.title = @"New";
        
        
    }

    [mainDelegate setDropdownId:0];
    txtCountry.text = [mainDelegate.Item Country];
    txtCurrency.text = [mainDelegate.Item Currency];
   


}

-(void)hideNumberKeyboard
{
	[txtYear  resignFirstResponder];
	[txtFaceValue  resignFirstResponder];
    [txtName  resignFirstResponder];
	[txtDescription  resignFirstResponder];
    [txtCountry resignFirstResponder];
	[txtPrice resignFirstResponder];
    [txtWeight resignFirstResponder];
    [txtMaterial resignFirstResponder];
}
-(void)hideTextKeyboard
{
    [txtName  resignFirstResponder];
	[txtDescription  resignFirstResponder];
    [txtCountry resignFirstResponder];
	[txtPrice resignFirstResponder];
    [txtWeight resignFirstResponder];
    [txtMaterial resignFirstResponder];
    [txtYear resignFirstResponder];

}
-(void)goBack
{
    if([mainDelegate AppPath] == 2)
    {
        CollectionItemsViewController *vc = [[CollectionItemsViewController alloc] init];
        [self presentViewController: vc animated:YES completion:nil];
        [vc release];
    }
    else
    {
        AllItems *Allvc = [[AllItems alloc] init];
        [self presentViewController: Allvc animated:YES completion:nil];
        [Allvc release];
    }

}

-(void)saveCurrentState
{
    mainDelegate.ItemTemp = [[Item alloc]init];
    [mainDelegate.ItemTemp setName: txtName.text];
    [mainDelegate.ItemTemp setYear: txtYear.text];
    [mainDelegate.ItemTemp setDescription:txtDescription.text];
    [mainDelegate.ItemTemp setDateOut:txtDateOut.text];
    
    [mainDelegate.ItemTemp setMaterial:txtMaterial.text];
    [mainDelegate.ItemTemp setFaceValue:txtFaceValue.text];
    [mainDelegate.ItemTemp setStatus:(int)selStatus.selectedSegmentIndex];
    [mainDelegate.ItemTemp setGrade:(int)selGrade.selectedSegmentIndex];
    
    [mainDelegate.ItemTemp setWeight:[txtWeight.text floatValue]];
    [mainDelegate.ItemTemp setPreciousMetal:(int)swPreciousMetal.selectedSegmentIndex];
    [mainDelegate.ItemTemp setWeightUnit:(int)selWeightUnit.selectedSegmentIndex];
    [mainDelegate.ItemTemp setCountryCode:[mainDelegate.Item CountryCode]];
    [mainDelegate.ItemTemp setCurrencyCode:[mainDelegate.Item CurrencyCode]];
    [mainDelegate.ItemTemp setPrice:txtPrice.text];
    
    
    
}
-(void)restoreCurrentState
{
    
    [mainDelegate.Item setName: [mainDelegate.ItemTemp Name]];
    [mainDelegate.Item setYear: [mainDelegate.ItemTemp Year]];
    [mainDelegate.Item setDescription:[mainDelegate.ItemTemp Description]];
    
    [mainDelegate.Item setMaterial:[mainDelegate.ItemTemp Material]];
    [mainDelegate.Item setFaceValue:[mainDelegate.ItemTemp FaceValue]];
    
    [mainDelegate.Item setStatus:[mainDelegate.ItemTemp Status]];
    [mainDelegate.Item setGrade:[mainDelegate.ItemTemp Grade]];
    [mainDelegate.Item setDateOut:[mainDelegate.ItemTemp DateOut]];
    
    
    [mainDelegate.Item setWeight:[mainDelegate.ItemTemp Weight]];
    [mainDelegate.Item setPreciousMetal:[mainDelegate.ItemTemp PreciousMetal]];
    [mainDelegate.Item setWeightUnit:[mainDelegate.ItemTemp WeightUnit]];
    [mainDelegate.Item setCountryCode:[mainDelegate.ItemTemp CountryCode]];
    [mainDelegate.Item setCurrencyCode:[mainDelegate.ItemTemp CurrencyCode]];
    [mainDelegate.Item setPrice:[mainDelegate.ItemTemp Price]];
    
    
}

- (void)dealloc {
    [txtYear release];
    txtYear = nil;
    [lblMessage release];
    lblMessage = nil;
    [selStatus release];
    selStatus = nil;
    [txtFaceValue release];
    txtFaceValue = nil;
    [txtCountry release];
    txtCountry = nil;
    [selGrade release];
    selGrade = nil;
    [topToolBar release];
    topToolBar = nil;
    [swPreciousMetal release];
    swPreciousMetal = nil;
    [lblItemCurrentPrice release];
    lblItemCurrentPrice = nil;
    [txtWeight release];
    txtWeight = nil;
    [txtPrice release];
    txtPrice = nil;
    [scrView release];
    scrView = nil;
    [txtMaterial release];
    txtMaterial = nil;
    [msgView release];
    msgView = nil;
    [txtPurchaseDate release];
    txtPurchaseDate = nil;
    [selWeightUnit release];
    selWeightUnit = nil;
    [txtCurrency release];
    txtCurrency = nil;
    [txtDateOut release];
    txtDateOut = nil;
    [super dealloc];
}


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            lblMessage.text= @"Mail cancelled: you cancelled the operation and no email message was queued.";
            break;
        case MFMailComposeResultSaved:
            lblMessage.text= (@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            lblMessage.text= (@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            lblMessage.text= (@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            lblMessage.text= (@"Mail not sent.");
            break;
    }
    [self dismissViewControllerAnimated:YES completion : nil];
}


#pragma mark Swipe Methods

-(void) swipeRight
{
    if (mainDelegate.itemIndex > 0)
    {
        [mainDelegate setItemIndex: mainDelegate.itemIndex-1];
        [mainDelegate setItem :(Item*)[mainDelegate.ItemList  objectAtIndex:mainDelegate.itemIndex ] ];
        [self populateScreen];
    }
   }
-(void) swipeLeft
{
     if (mainDelegate.itemIndex+ 1 < [mainDelegate.ItemList count])
    {
        [mainDelegate setItemIndex: mainDelegate.itemIndex+1];
        [mainDelegate setItem :(Item*)[mainDelegate.ItemList  objectAtIndex:mainDelegate.itemIndex ] ];
        [self populateScreen];
    }

    
   
}
#pragma mark Alerts

- (void)alertView:(UIAlertController *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == 0)
	{
        if (alertView.view.tag ==ALERT_DELETE)
        {
            [mainDelegate.Item deleteItem];
            [mainDelegate.Collection setItemCount:[NSString stringWithFormat:@"%d", [mainDelegate.Collection.ItemCount intValue]-1 ]];
            
            [self goBack];
        }
        else if(alertView.view.tag ==ALERT_COPY)
        {
             [self updateItem:@"1"];
            [self updateTopLabel:@"Duplicated"];
        }
		else if(alertView.view.tag == ALERT_PURCHASEDATE)
        {
            NSDate *date = [dobPicker date];
            NSDateFormatter* dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
            [dateFormatter setDateFormat:(NSString*)DateFormat];
             //[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
            txtPurchaseDate.text = [dateFormatter stringFromDate:date];
        }
        else if(alertView.view.tag == ALERT_DATEOUT)
        {
            NSDate *date = [dobPicker date];
            NSDateFormatter* dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
            [dateFormatter setDateFormat:(NSString*)DateFormat];
            txtDateOut.text = [dateFormatter stringFromDate:date];
        }
        else if(alertView.view.tag == ALERT_COUNTRY)
        {
            
        }
	}/////////////////////////////////////////////second button/////////////////////////////////////////////
}


-(void)showAlert:(NSString*)topic alertTag:(int)AlertTag
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:topic message: @"" preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if(AlertTag == ALERT_DELETE)
        {
            [mainDelegate.Item deleteItem];
            [mainDelegate.Collection setItemCount:[NSString stringWithFormat:@"%d", [mainDelegate.Collection.ItemCount intValue]-1 ]];
            
            [self goBack];
            
        }
        else if(AlertTag ==ALERT_COPY)
        {
             [self updateItem:@"1"];
            [self updateTopLabel:@"Duplicated"];
        }
        else if(AlertTag == ALERT_PURCHASEDATE)
        {
            NSDate *date = [dobPicker date];
            NSDateFormatter* dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
            [dateFormatter setDateFormat:(NSString*)DateFormat];
             //[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
            txtPurchaseDate.text = [dateFormatter stringFromDate:date];
        }
        else if(AlertTag == ALERT_DATEOUT)
        {
            NSDate *date = [dobPicker date];
            NSDateFormatter* dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
            [dateFormatter setDateFormat:(NSString*)DateFormat];
            txtDateOut.text = [dateFormatter stringFromDate:date];
        }
        
                        }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancel];
    [alert addAction:ok];
    alert.view.tag = AlertTag;
    
    [self presentViewController:alert animated:YES completion:nil];
  
     [alert release];
}

#pragma mark Keyboard movement

-(IBAction)showEdit:(id)sender
{
    topToolBar.title = @"Edit";
    if (selStatus.selectedSegmentIndex > 1)
    {
        txtDateOut.enabled = true;
    }
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    topToolBar.title = @"Edit";
    BOOL retBool = NO;
 
    if (textField.tag ==ALERT_DATEOUT && selStatus.selectedSegmentIndex > 1)
    {
        retBool = YES;
        activeField = textField;

      
    }
    else if (textField.tag ==ALERT_DATEOUT && selStatus.selectedSegmentIndex <= 1)
    {
  
        UIAlertController *dalert = [UIAlertController alertControllerWithTitle:@"" message: @"Status is not Sold or Traded."preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Continue" style:UIAlertActionStyleDefault handler:nil ];
            [dalert addAction:ok];
       
     
        dalert.view.tag=128;
        [self presentViewController:dalert animated:YES completion:nil];
        
    }

    else if (textField.tag ==TAG_COUNTRY)
    {
        [self saveCurrentState];
        [mainDelegate setDropdownId:DROPDOWN_COIN_COUNTRY];
        [mainDelegate setDropDownKey:[mainDelegate.Item CountryCode]];
        [mainDelegate setDropDownValue:[mainDelegate.Item Currency]];
        DropDownList *vc = [[DropDownList alloc] init];
        [self presentViewController: vc animated:YES completion:nil];
        [vc release];
        
    }
    else if (textField.tag ==TAG_CURRENCY)
    {
        [self saveCurrentState];
        [mainDelegate setDropdownId:DROPDOWN_COIN_CURRENCY];
        [mainDelegate setDropDownKey:[mainDelegate.Item CurrencyCode]];
        [mainDelegate setDropDownValue:[mainDelegate.Item Currency]];
        DropDownList *vc = [[DropDownList alloc] init];
        [self presentViewController: vc animated:YES completion:nil];
        [vc release];
    }
   
       else
    {
        retBool = YES;
        activeField = textField;
    }
      return retBool;
}


@end
