//
//  Summary.m
//  CoinBook
//
//  Created by Vadim Re on 2012-12-12.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import "Summary.h"
#import "AppDelegate.h"
#import "Settings.h"
#import "CellLabelLabel.h"
#import "PairKeyValue.h"
#import "ViewController.h"

@interface Summary ()

@end

@implementation Summary

@synthesize itemCell = inItemCell;


NSMutableArray *tblSummary;
NSMutableArray *tblSilver;
NSMutableArray *tblGold;
NSMutableArray *tblPlatinum;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
         self.modalPresentationStyle = UIModalPresentationFullScreen;
      

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
      [self populateScreen];
    topNav.title = @"Summary";
     [tblTableView  setFrame:CGRectMake(0, 0, 420, self.view.frame.size.height)];

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [lblBookAmount release];
    [lblItemsTotal release];
    [lblPaidTotal release];
    [lblPreciousMetal release];
    [lblSilver release];
    [lblGold release];
    [lblPlatinum release];
    [lblAddedItems release];
    [lblEmptyCells release];
    [tblTableView release];
    [btnMainMenu release];
    [lblBookAmount release];
    lblBookAmount = nil;
    //rvv [lblItemsTotal release];
    lblItemsTotal = nil;
    [lblPaidTotal release];
    lblPaidTotal = nil;
    [lblPreciousMetal release];
    lblPreciousMetal = nil;
    [lblSilver release];
    lblSilver = nil;
    [lblGold release];
    lblGold = nil;
    [lblPlatinum release];
    lblPlatinum = nil;
    [lblAddedItems release];
    lblAddedItems = nil;
    [lblEmptyCells release];
    lblEmptyCells = nil;
    [tblTableView release];
    tblTableView = nil;
    [btnMainMenu release];
    btnMainMenu = nil;
    [super dealloc];
}
//- (void)viewDidUnload {
//    
//    [super viewDidUnload];
//}

#pragma mark Methods
-(void)populateScreen
{
    tblSummary = [[NSMutableArray alloc] init];
    tblSilver = [[NSMutableArray alloc] init];
    tblGold = [[NSMutableArray alloc] init];
    tblPlatinum = [[NSMutableArray alloc] init];
    
    PairKeyValue *key = [[PairKeyValue alloc]init];
    key.Key = @"Coins pockets total";
    key.Value =  [mainDelegate.DataBase getOneValue:@" select count(*) from  items " ];
    
    [tblSummary addObject:key];
    [key release];
    key = [[PairKeyValue alloc]init];
    key.Key = @"Collections total:";
    key.Value = [mainDelegate.DataBase getOneValue:@" select count(*) from collections where status <> 0" ];
    [tblSummary addObject:key];
    [key release];
    key = [[PairKeyValue alloc]init];
    key.Key = @"Coins in collections:";
    key.Value = [mainDelegate.DataBase getOneValue:@" select count(*) from  items  where status =1" ];
    [tblSummary addObject:key];
    [key release];
    key = [[PairKeyValue alloc]init];
    
    key.Key = @"Total payied value:";
    
    key.Value = [NSString stringWithFormat:@"%1.2f" ,[[mainDelegate.DataBase getOneValue:@" select sum(price) from  items  where status =1 " ]floatValue]];
    if(key.Value.length == 0)
    {
        key.Value =@"0.0";
    }
    [tblSummary addObject:key];
    [key release];
    
    key = [[PairKeyValue alloc]init];
    key.Key = @"Precious metal coins:";
    key.Value = [mainDelegate.DataBase getOneValue:@" select count(*) from  items  where items.status = 1 and precious_metal  > 0" ];
    [tblSummary addObject:key];
    [key release];
    
    key = [[PairKeyValue alloc]init];
    key.Key = @"Precious metal coins value:";
    key.Value = [NSString stringWithFormat:@"%1.2f" ,[[mainDelegate.DataBase getOneValue:@" select sum(price) from  items  where items.status = 1 and precious_metal  > 0" ] floatValue]];
    [tblSummary addObject:key];
    [key release];
    ////////////////////////////////    Silver
    key = [[PairKeyValue alloc]init];
    key.Key = @"Coins:";
    key.Value = [mainDelegate.DataBase getOneValue:@" select count(*) from  items  where items.status = 1 and precious_metal  =1" ];
    [tblSilver addObject:key];
    [key release];
    
    key = [[PairKeyValue alloc]init];
    key.Key = @"Value:";
    key.Value = [NSString stringWithFormat:@"%1.2f" ,[[mainDelegate.DataBase getOneValue:@" select sum(price) from  items  where items.status = 1 and precious_metal  =1" ] floatValue]];
    [tblSilver addObject:key];
    [key release];
    
    key = [[PairKeyValue alloc]init];
    key.Key = @"Coins:";
    key.Value = [mainDelegate.DataBase getOneValue:@" select count(*) from  items  where items.status = 1 and precious_metal  =2" ];
    [tblGold addObject:key];
    [key release];
    
    key = [[PairKeyValue alloc]init];
    key.Key = @"Value:";
    key.Value = [NSString stringWithFormat:@"%1.2f" ,[[mainDelegate.DataBase getOneValue:@" select sum(price) from  items  where items.status = 1 and precious_metal  =2" ] floatValue]];
    [tblGold addObject:key];
    [key release];
    
    key = [[PairKeyValue alloc]init];
    key.Key = @"Coins:";
    key.Value = [mainDelegate.DataBase getOneValue:@" select count(*) from  items  where items.status = 1 and precious_metal  =3" ];
    [tblPlatinum addObject:key];
    [key release];
    
    
    key = [[PairKeyValue alloc]init];
    key.Key = @"Value:";
    key.Value = [NSString stringWithFormat:@"%1.2f" ,[[mainDelegate.DataBase getOneValue:@" select sum(price) from  items  where items.status = 1 and precious_metal  =3" ] floatValue]];
    [tblPlatinum addObject:key];
    [key release];
    
    key = [[PairKeyValue alloc]init];
    
    key.Key = @"Coins value:";
    key.Value =[NSString stringWithFormat:@"%d", [lblItemsTotal.text intValue] - [lblAddedItems.text intValue]];
    // [tblSummary addObject:key];
    [key release];
    


}


- (IBAction)onClickBack:(id)sender
{
    Settings *vc = [[Settings alloc] init];
    [self presentViewController: vc animated:YES completion:nil];
    [vc release];
}

- (IBAction)onClickMainMenu:(id)sender
{
    ViewController *vc = [[ViewController alloc] init];
    [self presentViewController: vc animated:YES completion:nil];
    [vc release];

}
#pragma mark Table

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 4;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)] autorelease];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, tableView.bounds.size.width, 30)];
    
    if(section==0)
    {
        [label setText:@"Summary"];
    }
    else if(section==1)
    {
        label.text =  @"Silver";
    }
    else if(section==2)
    {
        label.text =   @"Gold";
    }
    else if(section==3)
    {
        label.text =   @"Platinum";
    }
    else
    {
        label.text =   @"";
    }
    
    label.textColor = [UIColor blackColor];
    label.backgroundColor =[UIColor clearColor];
    [headerView addSubview:label];
    //[headerView setBackgroundColor:[Utility greyColor]];
    [label release];
    return headerView;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    if(section==0)
    {
        return [tblSummary count];
        
    }
    else if(section==1)
    {
        return [tblSilver count];
        
    }
    else if(section==2)
    {
        return [tblGold count];
        
    }
    else if(section==3)
    {
        return [tblPlatinum count];
        
    }
    else
    {
        return 0;
    }
    
}


- (CellLabelLabel *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CustomCellIdentifier";
    CellLabelLabel *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *topObject =  [[NSBundle mainBundle] loadNibNamed:@"CellLabelLabel" owner:self options:nil];
        
        for(id currentObject in topObject)
        {
            if ([currentObject isKindOfClass:[CellLabelLabel class]])
            {
                cell = (CellLabelLabel*)currentObject;
                break;
            }
        }
        // cell = [self itemCell];
        // [self setItemCell:nil];
    }
      
    if(indexPath.section== 0)
    {
        [cell.lblLabelLeft setText:[(PairKeyValue*)[tblSummary objectAtIndex:indexPath.row] Key]];
        [cell.lblLabelRight setText:[(PairKeyValue*)[tblSummary objectAtIndex:indexPath.row] Value]];
    }
    else if(indexPath.section== 1)
    {
        [cell.lblLabelLeft setText:[(PairKeyValue*)[tblSilver objectAtIndex:indexPath.row] Key]];
        [cell.lblLabelRight setText:[(PairKeyValue*)[tblSilver objectAtIndex:indexPath.row] Value]];
    }
    else if(indexPath.section== 2)
    {
        [cell.lblLabelLeft setText:[(PairKeyValue*)[tblGold objectAtIndex:indexPath.row] Key]];
        [cell.lblLabelRight setText:[(PairKeyValue*)[tblGold objectAtIndex:indexPath.row] Value]];
    }
    else if(indexPath.section== 3)
    {
        [cell.lblLabelLeft setText:[(PairKeyValue*)[tblPlatinum objectAtIndex:indexPath.row] Key]];
        [cell.lblLabelRight setText:[(PairKeyValue*)[tblPlatinum objectAtIndex:indexPath.row] Value]];
    }
    [cell.lblLabelRight setFrame:CGRectMake(110, 11, 180, 21)];
    //[cell setItemIndex:indexPath.row];
    // UIView *selectionColor = [[UIView alloc] init];
    // selectionColor.backgroundColor = [UIColor  grayColor];//colorWithRed:(245/255.0) green:(245/255.0) blue:(245/255.0) alpha:1
    // cell.selectedBackgroundView = selectionColor;
    
    
    return cell;
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section==0)
    {
        return @"Summary";
    }
    else if(section==1)
    {
        return @"Silver";
    }
    else if(section==2)
    {
        return @"Gold";
    }
    else if(section==3)
    {
        return @"Platinum";
    }
    else
    {
        return @"";
    }
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}


@end

