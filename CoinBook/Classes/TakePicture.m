//
//  TakePicture.m
//  CoinBook
//
//  Created by Vadim Re on 2013-06-21.
//  Copyright (c) 2013 Vadim Re. All rights reserved.
//

#import "TakePicture.h"
#import "ItemViewController.h"
#import "AppDelegate.h"


#import "Item.h"

#define ALERT_DELETE 1

@interface TakePicture ()

@end

@implementation TakePicture
@synthesize pictureImageView, btnDelete,topTitle;


UIImage *image;

NSString *imageFilePath;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
         self.modalPresentationStyle = UIModalPresentationFullScreen;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    Item *execItem= [[Item alloc]init];
    imageFilePath = [execItem getItemImagePath:[mainDelegate.Item ItemId]];
    if(imageFilePath.length >0)
    {
        UIImage *initialImage = [self loadImage:imageFilePath];
        NSData *data = UIImagePNGRepresentation([self loadImage:imageFilePath]);
        
        UIImage *tempImage = [UIImage imageWithData:data];
        UIImage *fixedOrientationImage = [UIImage imageWithCGImage:tempImage.CGImage scale:initialImage.scale orientation:initialImage.imageOrientation];
        initialImage = fixedOrientationImage;
        
        
        [pictureImageView setImage:fixedOrientationImage];//[self loadImage:imageFilePath]
    }
    [execItem release];
    topTitle.title =@"Image";
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Photo
-(IBAction)takePhoto :(id)sender

{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
    }
    
    // image picker needs a delegate,
    [imagePickerController setDelegate:self];
    
    // Place image picker on the screen
    [self presentViewController:imagePickerController animated:YES completion : nil];
}

- (IBAction)onClickBack:(id)sender
{
    ItemViewController *vc = [[ItemViewController alloc] init];
    [self presentViewController: vc animated:YES completion:nil];
    [vc release];
}

- (IBAction)onClickSaveImage:(id)sender
{
    [self saveImage:image forItem:[mainDelegate.Item ItemId]];
    topTitle.title = @"Saved";
}

- (IBAction)onClickDelete:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Delete picture" message:@"Delete picture" preferredStyle:UIAlertControllerStyleAlert];// initWithFrame:CGRectMake(20, 320, 280, 160)];
    UIAlertAction* MyAlert = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:MyAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
    //[alert setTitle:winnerName];
   
    
   
//    [alert setDelegate:self];
//    [alert addButtonWithTitle:@"OK"];
//    [alert addButtonWithTitle:@"Cancel"];
    alert.view.tag = ALERT_DELETE;
//    [alert show];
    [alert release];

}



-(IBAction)chooseFromLibrary:(id)sender
{
    
    UIImagePickerController *imagePickerController= [[UIImagePickerController alloc]init];
    [imagePickerController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    
    // image picker needs a delegate so we can respond to its messages
    [imagePickerController setDelegate:self];
    
    // Place image picker on the screen
    [self presentViewController:imagePickerController animated:YES completion: nil];
    
}

//delegate methode will be called after picking photo either from camera or library
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self dismissViewControllerAnimated:YES completion: nil];
    image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    [pictureImageView setImage:image];
    topTitle.title =@"New Image";
}


- (void)saveImage:(UIImage *)image forItem:(NSString *)itemId
{
    
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleMedium];
    spinner.center =CGPointMake(160, 240);
    spinner.tag= 128;
    [self.view addSubview:spinner];
    
    [spinner startAnimating];
    [spinner release];

    
    
    //  Make file name first
    NSString *filename = [NSString stringWithFormat:@"image%@.png", itemId]; // or .jpg
    
    //  Get the path of the app documents directory
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //  Append the filename and get the full image path
    imageFilePath = [documentsDirectory stringByAppendingPathComponent:filename];
    
    //  Now convert the image to PNG/JPEG and write it to the image path
    NSData *imageData = UIImagePNGRepresentation(image);
    [imageData writeToFile:imageFilePath atomically:NO];
    
    //  Here you save the savedImagePath to your DB
    Item *execDS = [[Item alloc] init];
    [execDS updateItemImagePath:itemId ImagePath:imageFilePath];
    [execDS release];
    
    [spinner stopAnimating];

}

- (UIImage *)loadImage:(NSString *)filePath  {
    return [UIImage imageWithContentsOfFile:filePath];
}

-(void)deleteImageFromDisk
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:imageFilePath])
    {
        NSError *error;
        if (![fileManager removeItemAtPath:imageFilePath error:&error])
        {
            //NSLog(@"Error removing file: %@", error);
        };
    }

}
#pragma mark Memeory
- (void)dealloc
{
    [pictureImageView release];
    [btnDelete release];
    [topTitle release];
    [self setPictureImageView:nil];
    [self setBtnDelete:nil];
    [self setTopTitle:nil];
    [super dealloc];
}
//- (void)viewDidUnload {
//    
//    [super viewDidUnload];
//}


- (void)alertView:(UIAlertController  *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == 0)
	{
        if (alertView.view.tag ==ALERT_DELETE)
        {
            [self deleteImageFromDisk];
            Item *execDS = [[Item alloc] init];
            [execDS updateItemImagePath:[mainDelegate.Item ItemId] ImagePath:@""];
            [execDS release];
                          topTitle.title =@"Deleted";                                     }
    }
}
@end
