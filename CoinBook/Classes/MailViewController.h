//
//  MailViewController.h
//  CoinBook
//
//  Created by Vadim Re on 2012-11-14.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

#import "Item.h"
@class AppDelegate;

@interface MailViewController : UIViewController
<MFMailComposeViewControllerDelegate>
{
    AppDelegate *mainDelegate ;
    IBOutlet UIButton *btnSendEmail;
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextView *lblMessage;
    IBOutlet UITextField *txtFileName;
    IBOutlet UINavigationItem *topNav;
    IBOutlet UILabel *lblCollectionName;
    IBOutlet UISwitch *swPrice;
    IBOutlet UILabel *lblIncludePrice;
}
- (IBAction)onClickBtnSendEmail:(id)sender;
- (IBAction)onClickBack:(id)sender;
- (IBAction)hideKeyboard:(id)sender;

@end
