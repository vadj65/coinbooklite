//
//  Metals.h
//  CoinBook
//
//  Created by Vadim Re on 2012-11-27.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utility.h"
@class AppDelegate;

@interface Metals : UIViewController
<UITableViewDelegate, UITableViewDataSource>
{
    UITapGestureRecognizer *tapGesture;
    NSUserDefaults *prefs;
    
    
    IBOutlet UINavigationItem *topNav;
    IBOutlet UITableView *tblMetals;
   AppDelegate *mainDelegate;
}

- (IBAction)onClickBack:(id)sender;
- (IBAction)onClickRefresh:(id)sender;
- (void)hideKeyboard;
- (IBAction)onClickSave:(id)sender;
- (IBAction)onClickClear:(id)sender;
- (IBAction)onClickMainMenu:(id)sender;

@end
