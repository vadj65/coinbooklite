//
//  Settings.m
//  CoinBook
//
//  Created by Vadim Re on 2012-11-27.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import "Settings.h"
#import "Metals.h"
#import "ViewController.h"
#import "Utility.h"
#import "Summary.h"
#import "UserInfo.h"
#import "Reachability.h"
#import "AppDelegate.h"

@interface Settings ()

@end

@implementation Settings

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.modalPresentationStyle = UIModalPresentationFullScreen;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    topNav.title = @"Settings";
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    
    internetReachable = [[Reachability reachabilityForInternetConnection] retain];
    [internetReachable startNotifier];
    
    // check if a pathway to a random host exists
    hostReachable = [[Reachability reachabilityWithHostName: @"http://www.nincha.ca"] retain];
    [hostReachable startNotifier];
    
    
    
    Utility *ut =[[Utility alloc]init];
   //[ut executeCommand];
    [ut menuButton:btnAppSettings];
    [ut menuButton:btnMetals];
    [ut menuButton:btnSummary];
    [ut release];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onClickMetals:(id)sender
{
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.center =CGPointMake(160, 240);
    spinner.tag= 128;
    [self.view addSubview:spinner];
    
    [spinner startAnimating];
    [spinner release];
    Metals *vc = [[Metals alloc] init];
    [self presentViewController: vc animated:YES completion:nil];
    [vc release];
    
}
- (IBAction)onClickBack:(id)sender
    {
        ViewController *vc = [[ViewController alloc] init];
        [self presentViewController: vc animated:YES completion:nil];
        [vc release];
}

- (IBAction)onClickSecond:(id)sender
{
    Summary *vc = [[Summary alloc] init];
    [self presentViewController: vc animated:YES completion:nil];
    [vc release];
    /*ut = [[Utility alloc]init];
    [ut runCommand:@"" body:@""];
    [ut release];*/
}

- (IBAction)onClickAppSettings:(id)sender
{
    UserInfo *vc = [[UserInfo alloc] init];
    [self presentViewController: vc animated:YES completion:nil];
    [vc release];

}
- (void)dealloc {
    [btnAppSettings release];
    [btnSummary release];
    [btnMetals release];
    [btnAppSettings release];
    btnAppSettings = nil;
    [btnSummary release];
    btnSummary = nil;
    [btnMetals release];
    btnMetals = nil;
    [super dealloc];
}
//- (void)viewDidUnload {
//    
//    [super viewDidUnload];
//}


#pragma mark Network
- (void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    
    //  Utility *ut= [[Utility alloc] init];
    AppDelegate *mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    NetworkStatus internetStatus = [internetReachable currentReachabilityStatus];
    mainDelegate.AppSettings.Connection =1;
    switch (internetStatus)
    
    {
        case NotReachable:
        {
            NSLog(@"The internet is down.");
            mainDelegate.AppSettings.Connection =0;
            //  [ut alertMessage:@"The internet is down."];
            
            break;
            
        }
        case ReachableViaWiFi:
        {
            //NSLog(@"The internet is working via WIFI.");
            // self.internetActive = YES;
            
            break;
            
        }
        case ReachableViaWWAN:
        {
            //NSLog(@"The internet is working via WWAN.");
            
            break;
            
        }
    }
    
    NetworkStatus hostStatus = [hostReachable currentReachabilityStatus];
    switch (hostStatus)
    
    {
        case NotReachable:
        {
            //NSLog(@"A gateway to the host server is down.");
            //mainDelegate.AppSettings.Connection =0;
            //    [ut alertMessage:@"A gateway to the host server is down."];
          //  mainDelegate.AppSettings.connection =0;
            break;
            
        }
        case ReachableViaWiFi:
        {
            //        NSLog(@"A gateway to the host server is working via WIFI.");
            
            
            break;
            
        }
        case ReachableViaWWAN:
        {
            // NSLog(@"A gateway to the host server is working via WWAN.");
            
            break;
            
        }
    }
    
}

@end
