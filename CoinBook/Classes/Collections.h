//
//  Collections.h
//  CoinBook
//
//  Created by Vadim Re on 2012-11-07.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DS.h"
#import "sqlite3.h"


@interface Collections : NSObject
{
NSInteger _collectionId;
NSString *_name;
NSString *_description;
NSDate *_dateCreated;
}
@property (nonatomic, readonly) NSInteger CollectionId;
@property (nonatomic, copy) NSString *Name;
@property (nonatomic, copy) NSString *Description;
@property (nonatomic, copy) NSDate *DateCreated;

//Instance methods.
- (id) initWithPrimaryKey:(NSInteger)pk;
- (NSMutableArray*)getAllCollections;

@end
