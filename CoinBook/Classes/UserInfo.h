//
//  UserInfo.h
//  CoinBook
//
//  Created by Vadim Re on 2012-12-15.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseView.h"
@class CellLabelText;
@class ItemCell;
@class AppDelegate;

@interface UserInfo : BaseView
{
   
    
    //rvv AppDelegate *mainDelegate;

    IBOutlet UINavigationItem *navTop;
    //CellLabelText *intItemCell;
    IBOutlet UITableView *tblUser;
}
@property(nonatomic, retain) IBOutlet ItemCell *itemCell;
//@property(nonatomic, retain) IBOutlet CellLabelText *itemCell;


- (IBAction)onCLickCLear:(id)sender;
- (IBAction)onClickSave:(id)sender;
- (IBAction)onClickBack:(id)sender;
-( IBAction)hideKeyboard:(id)sender;


@end
