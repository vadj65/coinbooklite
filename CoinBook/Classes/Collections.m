//
//  Collections.m
//  CoinBook
//
//  Created by Vadim Re on 2012-11-07.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import "Collections.h"
#import "AppDelegate.h"


@implementation Collections
@synthesize  collectionId, Description, Name, dateCreated;

sqlite3_stmt    *_statement;
sqlite3 *_contactDB;


#pragma mark - Init
- (id) initWithPrimaryKey:(NSInteger) pk {
    
    [super init];
    _collectionId = pk;
    
    
    
    return self;
}

#pragma  mark - Property

-(NSString*)Name
{
    return _name;
}
-(void)setName:(NSString *)newBookName
{
    _name = newBookName;
}

-(void)setCollectionId:(NSInteger)newCollectionId
{
    _collectionId = newCollectionId;
}

#pragma mark - Methods
-(NSMutableArray*)getAllBooks
{
    
    AppDelegate *mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    const char *sql = [@"select collection_id, name from books" UTF8String];
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    const char *dbpath = [ mainDelegate.DataBase.DatabasePath UTF8String];
    //sqlite3_stmt    *statement;
    
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
        
        
        // const char *query_stmt = [selectSQL UTF8String];
        
        if (sqlite3_prepare_v2(_contactDB, sql, -1, &_statement, NULL) == SQLITE_OK)
        {
            
            while (sqlite3_step(_statement) == SQLITE_ROW)
            {
                
                
                
                Collections *bookObj = [[Book alloc] init];
                
                [bookObj setBookName:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(_statement, 1)]];
                [bookObj setBookId:[[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(_statement, 0)] intValue]];
                [mainDelegate.DataBase setStatus: @"Match found"];
                [result addObject:bookObj];
                [bookObj release];
                
            }
            sqlite3_finalize(_statement);
        }
        sqlite3_close(_contactDB);
    }
    
    return result;
    
    
}Id, bookDescription, bookName, dateCreated;

sqlite3_stmt    *_statement;
sqlite3 *_contactDB;


#pragma mark - Init
- (id) initWithPrimaryKey:(NSInteger) pk {
    
    [super init];
    _collectionId = pk;
    
    
    
    return self;
}

#pragma  mark - Property

-(NSString*)Name
{
    return _bookName;
}
-(void)setBookName:(NSString *)newBookName
{
    _bookName = newBookName;
}

-(void)setBookId:(NSInteger)newBookId
{
    _bookId = newBookId;
}

#pragma mark - Methods
-(NSMutableArray*)getAllBooks
{
    
    AppDelegate *mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    const char *sql = [@"select collection_id, name from books" UTF8String];
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    const char *dbpath = [ mainDelegate.DataBase.DatabasePath UTF8String];
    //sqlite3_stmt    *statement;
    
    if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
    {
        
        
        // const char *query_stmt = [selectSQL UTF8String];
        
        if (sqlite3_prepare_v2(_contactDB, sql, -1, &_statement, NULL) == SQLITE_OK)
        {
            
            while (sqlite3_step(_statement) == SQLITE_ROW)
            {
                
                
                
                Book *bookObj = [[Book alloc] init];
                
                [bookObj setBookName:[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(_statement, 1)]];
                [bookObj setBookId:[[[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(_statement, 0)] intValue]];
                [mainDelegate.DataBase setStatus: @"Match found"];
                [result addObject:bookObj];
                [bookObj release];
                
            }
            sqlite3_finalize(_statement);
        }
        sqlite3_close(_contactDB);
    }
    
    return result;
    
    
}
@end
