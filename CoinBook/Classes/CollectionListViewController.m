//
//  CollectionListViewController.m
//  CoinBook
//
//  Created by Vadim Re on 2012-11-13.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import "CollectionListViewController.h"
#import "AppDelegate.h"
#import "AddBookViewController.h"
#import "CollectionItemsViewController.h"
#define ALERT_DELETE 1
#define ALERT_SORT 2


@interface CollectionListViewController ()

@end

@implementation CollectionListViewController


#pragma marks INIT

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
       
        sortDirectionName=@" Asc ";
        sortDirectionItems = @" Asc ";
        sortDirection = @" Asc ";
        sortField = [[NSString alloc] initWithString:@"name"];
        BookList=[[NSMutableArray alloc] init];
        
        [self populateCollection];
        isEdit =FALSE;
        isDelete = FALSE;
        topNavigationBar.topItem.title = @"Collections";
        self.modalPresentationStyle = UIModalPresentationFullScreen;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view autoresizingMask];
    topNav.title = @"All Collections";
    callScroll = TRUE;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    tableView.backgroundColor =[UIColor whiteColor];
    // Return the number of rows in the section.
    return [BookList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@(%@)",[(Collection*)[BookList objectAtIndex:indexPath.row] Name], [(Collection*)[BookList objectAtIndex:indexPath.row] ItemCount]];
    cell.textLabel.font = [UIFont fontWithName:@"custom" size:14.0f];
    cell.textLabel.font=[UIFont fontWithName:@"Arial Rounded MT Bold" size:14.0];
    cell.selectionStyle =UITableViewCellSelectionStyleGray;
    int intTemp;
    if ([mainDelegate selectedRowIndexCollection]>=[BookList count])
    {
        intTemp= (int)[BookList count]-1;
    }
    else
    {
        intTemp= [mainDelegate selectedRowIndexCollection];
    }
    if(callScroll)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:intTemp inSection:0];
        [tblCollection scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
       // [tblCollection selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        
        callScroll= false;
    }
    cell.backgroundColor =[UIColor whiteColor];
    cell.textLabel.textColor =[UIColor blackColor];
    return cell;
}


 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 /*
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleInsert;
}
*/
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [mainDelegate setCollection:[BookList  objectAtIndex:indexPath.row] ];
    if (isEdit || (!isEdit && !isDelete))
    {
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            // Delete the row from the data source
            //[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            AddBookViewController *vc = [[AddBookViewController alloc] init];
            [self presentViewController: vc animated:YES completion:nil];
            [vc release];
        }
        else if (editingStyle == UITableViewCellEditingStyleInsert) {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    else if (isDelete)
    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithFrame:CGRectMake(20, 320, 280, 160)];
//
//        //[alert setTitle:winnerName];
//        CGAffineTransform myTransform = CGAffineTransformMakeTranslation(0, -100);
//
//        [alert setTransform:myTransform];
//
//        [alert setMessage: [NSString stringWithFormat:@"Delete collection %@", [mainDelegate.Collection Name]]];
//        [alert setDelegate:self];
//        [alert addButtonWithTitle:@"Delete"];
//        [alert addButtonWithTitle:@"Cancel"];
//        [alert setTag:ALERT_DELETE];
//        [alert show];
//        [alert release];
        UIAlertController *dalert = [UIAlertController alertControllerWithTitle:@"" message: [NSString stringWithFormat:@"Delete collection: %@", [mainDelegate.Collection Name]] preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [mainDelegate.Collection deleteCollection:[mainDelegate.Collection CollectionId]];
            [self populateCollection ];
            [tblCollection reloadData];
        }];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil ];
        [dalert addAction:ok];
        [dalert addAction:cancel];
       
     
        dalert.view.tag=ALERT_DELETE;
        [self presentViewController:dalert animated:YES completion:nil];
    }
    
}



/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    
   
    [mainDelegate setCollection:[BookList  objectAtIndex:indexPath.row] ];
    // NSLog(@"indexPath = %d", indexPath.row);
    if (isEdit)
    {
        [mainDelegate setSelectedRowIndexCollection:(int)indexPath.row];
        AddBookViewController *vc = [[AddBookViewController alloc] init];
        [self presentViewController: vc animated:YES completion:nil];
        [vc release];
    }
   else if (isDelete)
    {
       // UIAlertView *alert = [[UIAlertView alloc] initWithFrame:CGRectMake(20, 320, 280, 160)];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Delete" message: [NSString stringWithFormat:@"Delete collection %@", [mainDelegate.Collection Name]] preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:cancel];
        [alert addAction:ok];
        alert.view.tag = ALERT_DELETE;
//        [alert setTransform:myTransform];
//
//        [alert setMessage: [NSString stringWithFormat:@"Delete collection %@", [mainDelegate.Collection Name]]];
//        [alert setDelegate:self];
//        [alert addButtonWithTitle:@"DELETE"];
//        [alert addButtonWithTitle:@"CANCEL"];
//        [alert setTag:ALERT_DELETE];
//        [alert show];
        [alert release];

    }
    else
    {
        [mainDelegate setWhereField:@""];
        [mainDelegate setSortField:@" name "];                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
        [mainDelegate setSelectedRowIndexCollection:(int)indexPath.row];
        [mainDelegate setAppPath:2];
        CollectionItemsViewController *vc = [[CollectionItemsViewController alloc] init];
        [self presentViewController: vc animated:YES completion:nil];
        [vc release];
    }
}
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isDelete)
    {
        return @"Delete";
    }
    else 
    {
        return @"Edit";
    }
    
    
}

#pragma mark - Methods Buttons

- (IBAction)onClickBack:(id)sender {
     [mainDelegate setSelectedRowIndexCollection:0];
    ViewController *vc = [[ViewController alloc] init];
    [self presentViewController: vc animated:YES completion:nil];
    [vc release];
    
}

- (IBAction)onClickBtnEdit:(id)sender
{
    if ([BookList count] > 0)
    {
        
    
        if (isEdit)
        {
            isEdit = FALSE;
            btnEdit.title = @"Edit";
            btnEdit.tintColor = [UIColor grayColor];
            [tblCollection setEditing: NO animated:NO];
        }
        else
        {
            isEdit = TRUE;
            btnEdit.title = @"Stop Edit";
             btnEdit.tintColor = [UIColor redColor];
             [tblCollection setEditing: YES animated:YES];
        
        }
        isDelete = FALSE;
         btnDelete.title = @"Delete";
        btnDelete.tintColor = [UIColor grayColor];

        [tblCollection reloadData];
    }
}

- (IBAction)onClickDelete:(id)sender
{
    if ([BookList count] > 0)
    {
    if(isDelete)
    {
        isDelete = FALSE;
        btnDelete.title = @"Delete";
        btnDelete.tintColor = [UIColor grayColor];
        [tblCollection setEditing:false animated:NO];
    }
    else
    {
         isDelete = TRUE;
         btnDelete.title = @"Stop Delete";
         btnDelete.tintColor = [UIColor redColor];
          [tblCollection setEditing:true animated:YES];
    }
   
    isEdit = FALSE;
    btnEdit.title = @"Edit";
      btnEdit.tintColor = [UIColor grayColor];
    [tblCollection reloadData];
    }
    
   
}

- (IBAction)onClickSort:(id)sender
{
    if ([BookList count] > 0)
    {
  //  UIAlertView *alert = [[UIAlertView alloc] initWithFrame:CGRectMake(20, 320, 280, 160)];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Sort" message:@"" preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction* ok = [UIAlertAction
                           actionWithTitle:@"By Name"
                           style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action)
                           {
                                sortField = @" name ";
                                if ([sortDirectionName isEqualToString:@" Asc "])
                                {
                                    sortDirection = @" Desc ";
                                    sortDirectionName = @" Desc ";
                                }
                                else
                                {
                                    sortDirection = @" Asc ";
                                    sortDirectionName = @" Asc ";
                                }
                                [self populateCollection ];
                                [tblCollection reloadData];
                                [alert dismissViewControllerAnimated:YES completion:nil];

                           }];
                UIAlertAction* cancel = [UIAlertAction
                                actionWithTitle:@"By Collection Size"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                    sortField = @" items_count ";
                                    if ([sortDirectionItems isEqualToString:@" Asc "])
                                    {
                                        sortDirection = @" Desc ";
                                        sortDirectionItems = @" Desc ";
                                    }
                                    else
                                    {
                                        sortDirection = @" Asc ";
                                        sortDirectionItems = @" Asc ";
                                    }
                                    [self populateCollection ];
                                    [tblCollection reloadData];
                                    [alert dismissViewControllerAnimated:YES completion:nil];
                               }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
         [self presentViewController:alert animated:YES completion:nil];
//    [alert setTitle:@"winnerName"];
//    CGAffineTransform myTransform = CGAffineTransformMakeTranslation(0, -100);
//
//    [alert setTransform:myTransform];
//
//    [alert setMessage: [NSString stringWithFormat:@"Delete collection %@", [mainDelegate.Collection Name]]];
//    [alert setDelegate:self];
//    [alert addButtonWithTitle:@"by Name"];
//    [alert addButtonWithTitle:@"by Items"];
//    [alert setTag:ALERT_SORT];
//    [alert show];
//    [alert release];
    }

}

- (IBAction)onClickAddBook:(id)sender
{
    if ([BookList count] <BOOKS_MAX)
    {
        [mainDelegate setCollection:[[Collection alloc]init]];
        AddBookViewController *addBookVC = [[AddBookViewController alloc] init];
        [self presentViewController:addBookVC animated:YES completion:nil];
        [addBookVC release];
     }
    else
    {
        [Utility showAlert:@"Allowed amount of books has been achieved. Upgrade to full version." alertTag:33];
    }
}
- (void)dealloc {
    //[btnBack release];
    [tblCollection release];
    [btnEdit release];
    [topNavigationBar release];
    [topNav release];
    
    [btnDelete release];
    [btnSort release];
    [btnDelete release];
    [tblCollection release];
    tblCollection = nil;
    [btnEdit release];
    btnEdit = nil;
    [topNavigationBar release];
    topNavigationBar = nil;
    [topNav release];
    topNav = nil;
    
    [btnDelete release];
    btnDelete = nil;
    [btnSort release];
    btnSort = nil;
    [btnDelete release];
    btnDelete = nil;
    [super dealloc];
}


#pragma mark Controller methods 

-(void)populateCollection
{
    [BookList removeAllObjects];
    Collection *collectionExec = [[Collection alloc]init];
    [collectionExec getAllCollections:@"1" sortOrder: [sortField  stringByAppendingString: sortDirection] CollectionList:BookList] ;
    [collectionExec release];
    
}

-(void)showTitle:(NSString*)NewTitle
{
    UILabel *lbl= [[UILabel alloc]initWithFrame:CGRectMake(5,3,15,15)];
    lbl.textColor=[UIColor redColor];
    [lbl setText:NewTitle];
    [lbl setBackgroundColor:[UIColor  clearColor] ];
    
    [lbl sizeToFit];
    lbl.center = CGPointMake(160,25);
   //[topNavigationBar addSubview:lbl];
    topNavigationBar.topItem.title = NewTitle;
    [lbl release];

}

#pragma mark Alerts

- (void)alertView:(UIAlertController *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == 0)
	{
        if (alertView.view.tag ==ALERT_DELETE)
        {
            [mainDelegate.Collection deleteCollection:[mainDelegate.Collection CollectionId]];
        }
        else if(alertView.view.tag ==ALERT_SORT)
        {
            sortField = @" name ";
            if ([sortDirectionName isEqualToString:@" Asc "])
            {
                sortDirection = @" Desc ";
                sortDirectionName = @" Desc ";
            }
            else
            {
                sortDirection = @" Asc ";
                sortDirectionName = @" Asc ";
            }
        }
		
	}/////////////////////////////////////////////second button/////////////////////////////////////////////
	else if (buttonIndex == 1)
	{
		if(alertView.view.tag ==ALERT_SORT)
        {
            sortField = @" items_count ";
            if ([sortDirectionItems isEqualToString:@" Asc "])
            {
                sortDirection = @" Desc ";
                sortDirectionItems = @" Desc ";
            }
            else
            {
                sortDirection = @" Asc ";
                sortDirectionItems = @" Asc ";
            }
        }
	}
	//////////////////////////////////////////////THIRD BUTTON///////////////////////////////////
	else if (buttonIndex == 2)
	{
       
		
	}
    

    [self populateCollection ];
    [tblCollection reloadData];
}





@end
