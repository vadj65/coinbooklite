//
//  CollectionItemsViewController.h
//  CoinBook
//
//  Created by Vadim Re on 2012-11-08.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import "ViewController.h"
#import "CollectionListViewController.h"
#import "ItemViewController.h"

@class ItemCell;
@class AppDelegate;


@interface CollectionItemsViewController : UIViewController
<UITableViewDelegate, UITableViewDataSource>
{
     
   AppDelegate *mainDelegate ;
    NSString *sortDirectionName;
    NSString *sortDirectionYear;
    NSString *sortDirection;
    
    BOOL callScroll;
    UIView *headerView;
    
    NSString *filterType;
    
    IBOutlet UIBarButtonItem *btnFilter;
    IBOutlet UINavigationItem *topNav;
    IBOutlet UIScrollView *scroller;
    IBOutlet UITableView *tableCollection;
    IBOutlet UIBarButtonItem *btnSort;
    IBOutlet UIBarButtonItem *btnEdit;
    ItemCell *inItemCell;
    IBOutlet UIBarButtonItem *btnEmail;
    IBOutlet UIToolbar *toolBarBottom;
}
@property(nonatomic, retain) IBOutlet ItemCell *itemCell;
- (IBAction)onClickAdd:(id)sender;
- (IBAction)onClickBack:(id)sender;

- (IBAction)onClickBtnEdit:(id)sender;
- (IBAction)onClickBtnSort:(id)sender;
- (IBAction)onClickFilter:(id)sender;
- (IBAction)onClickEmail:(id)sender;

@end
