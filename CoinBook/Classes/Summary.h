//
//  Summary.h
//  CoinBook
//
//  Created by Vadim Re on 2012-12-12.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CellLabelLabel;
@class AppDelegate;

@interface Summary : UIViewController
<UITableViewDelegate, UITableViewDataSource>
{
    
    AppDelegate *mainDelegate;
    IBOutlet UILabel *lblEmptyCells;
    IBOutlet UILabel *lblAddedItems;
    IBOutlet UILabel *lblPlatinum;
    IBOutlet UILabel *lblGold;
    IBOutlet UILabel *lblSilver;
    IBOutlet UILabel *lblPaidTotal;
    IBOutlet UILabel *lblItemsTotal;
    IBOutlet UILabel *lblBookAmount;
    IBOutlet UILabel *lblPreciousMetal;
    IBOutlet UINavigationItem *topNav;
    CellLabelLabel *inItemCell;
    IBOutlet UITableView *tblTableView;
    IBOutlet UITableView *tblTable;
    IBOutlet UIBarButtonItem *btnMainMenu;
}
- (IBAction)onClickBack:(id)sender;
- (IBAction)onClickMainMenu:(id)sender;
@property(nonatomic, retain) IBOutlet CellLabelLabel *itemCell;
@end
