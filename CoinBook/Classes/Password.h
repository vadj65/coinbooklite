//
//  Password.h
//  CoinBook
//
//  Created by Vadim Re on 2013-06-25.
//  Copyright (c) 2013 Vadim Re. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseView.h"
@class AppDelegate;

@interface Password : BaseView
{
    //rvv AppDelegate *mainDelegate;
    IBOutlet UITextField *txtPassword;
    IBOutlet UITextField *txtPasswordRetype;
    IBOutlet UISwitch *swPassword;
   // IBOutlet UINavigationItem *topNav;
}
- (IBAction)onClickSave:(id)sender;
- (IBAction)onClickBack:(id)sender;
- (IBAction)onClickClear:(id)sender;

@end
