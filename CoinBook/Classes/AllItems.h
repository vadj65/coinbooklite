//
//  AllItems.h
//  CoinBook
//
//  Created by Vadim Re on 2012-11-30.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ItemCell;
@class AppDelegate;
//test for commit
@interface AllItems : UIViewController
<UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *tblAllItems;
    ItemCell *inItemCell;
    IBOutlet UINavigationItem *topNav;
    AppDelegate *mainDelegate ;
    int selectedAlert;
    int buttonIndex;
}
@property(nonatomic, retain) IBOutlet ItemCell *itemCell;

- (IBAction)onClickBack:(id)sender;
- (IBAction)onClickFilter:(id)sender;
- (IBAction)onClickSort:(id)sender;

@end
