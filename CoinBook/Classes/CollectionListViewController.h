//
//  CollectionListViewController.h
//  CoinBook
//
//  Created by Vadim Re on 2012-11-13.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AppDelegate;

@interface CollectionListViewController : UIViewController
<UITableViewDelegate, UITableViewDataSource>
{

    BOOL isEdit;
    bool isDelete;
    NSString *sortDirectionName;
    NSString *sortDirectionItems;
    NSString *sortDirection;
    
    IBOutlet UITableView *tblCollection;
    NSMutableArray *BookList;
    IBOutlet UIBarButtonItem *btnEdit;
    IBOutlet UINavigationBar *topNavigationBar;
    IBOutlet UINavigationItem *topNav;
    AppDelegate *mainDelegate ;
    IBOutlet UIBarButtonItem *btnDelete;
    IBOutlet UIBarButtonItem *btnSort;
    bool callScroll;
    NSString *sortField;
    
}

@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnBack;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnAddBook;

- (IBAction)onClickAddBook:(id)sender;
- (IBAction)onClickBack:(id)sender;

- (IBAction)onClickBtnEdit:(id)sender;
- (IBAction)onClickDelete:(id)sender;
- (IBAction)onClickSort:(id)sender;

@end
