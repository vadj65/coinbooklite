//
//  AllItems.m
//  CoinBook
//
//  Created by Vadim Re on 2012-11-30.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import "AllItems.h"
#import "ViewController.h"
#import "AppDelegate.h"
#import "ItemViewController.h"
#import "ItemCell.h"
#define ALERT_FILTER 1
#define ALERT_SORT 2


@interface AllItems ()

@end

@implementation AllItems

@synthesize itemCell = inItemCell;

NSString *sortString;
NSString *filterString;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
         mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
        // Custom initialization
        sortString =@"";
        filterString=@"";
         [self populateList];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    topNav.title=@"All items";
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return [mainDelegate.ItemList count];
}
/*
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
 {
 static NSString *CellIdentifier = @"Cell";
 UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
 if (cell == nil) {
 cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
 }
 
 // Configure the cell...
 //Get the object from the array.
 //Item *itemObj =[[Item alloc] init];
 //itemObj = [_collectionList objectAtIndex:indexPath.row];
 
 //Set .(Item*)[_collectionList objectAtIndex:indexPath.row]
 // NSLog(@"%@", [itemObj getName]);
 cell.textLabel.text = [NSString stringWithFormat:@"%@ -%@",[(Item*)[_collectionItems objectAtIndex:indexPath.row] Year], [(Item*)[_collectionItems objectAtIndex:indexPath.row] Name]];
 return cell;
 }
 
 */

- (ItemCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CustomCellIdentifier";
    ItemCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"ItemCell" owner:self options:nil];
        cell = [self itemCell];
        [self setItemCell:nil];
    }
    
    [[cell lblName] setText:[NSString stringWithFormat:@"%@ -%@",[(Item*)[mainDelegate.ItemList objectAtIndex:indexPath.row] Year], [(Item*)[mainDelegate.ItemList objectAtIndex:indexPath.row] Name]]];
    [cell setItemId:[NSString stringWithFormat:@"%@",[(Item*)[mainDelegate.ItemList objectAtIndex:indexPath.row] ItemId]]];
    if ([(Item*)[mainDelegate.ItemList objectAtIndex:indexPath.row] Status] ==0)
    {
        [cell swStatus].on = FALSE;
    } else {
        [cell swStatus].on = TRUE;
    }
    [cell setItemIndex:(int)indexPath.row];
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
       // NSLog(@"indexPath = %d", indexPath.row);
    [mainDelegate.Item getItemByItemId:[(Item*)[mainDelegate.ItemList  objectAtIndex:indexPath.row] ItemId]];
    
    [mainDelegate setItemIndex:(int)indexPath.row];
    [mainDelegate setAppPath:1];
    ItemViewController *vc = [[ItemViewController alloc] init];
    [self presentViewController: vc animated:YES completion:nil];
    [vc release];
    
}
#pragma mark - Methods Buttons
- (IBAction)onClickBack:(id)sender
{
    ViewController *vc = [[ViewController alloc] init];
    [self presentViewController: vc animated:YES completion:nil];
    [vc release];
    
}
#pragma mark -Buttons
- (IBAction)onClickFilter:(id)sender
{
    [self showAlertFilter];
}

- (IBAction)onClickSort:(id)sender
{
    [self showAlertSort];
}
#pragma mark Finish
- (void)dealloc {
    [tblAllItems release];
    [sortString release];
    [filterString release];
    [tblAllItems release];
    tblAllItems = nil;
    [sortString release];
    [filterString release];
    [super dealloc];
}
//- (void)viewDidUnload {
//
//    [super viewDidUnload];
//}

#pragma mark Methods

-(void)populateList   //:(NSString*)filter  sort:(NSString*)Sort
{
    
  //  [mainDelegate setItem:[[Item alloc] init]];
  //   [mainDelegate.Item getAllItems:filterString sort:sortString ItemList:mainDelegate.ItemList];

    
}
-(void)showAlertSort
{
 	
   
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Filter Items by" message: @"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *btnName = [UIAlertAction actionWithTitle:@"Name" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            buttonIndex =0;
            selectedAlert = ALERT_SORT;
            [self callAction];
    } ];
    [alert addAction:btnName];
   
    
    UIAlertAction *btnYear = [UIAlertAction actionWithTitle:@"Year" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            buttonIndex =1;
            selectedAlert = ALERT_SORT;
            [self callAction];
    } ];
    [alert addAction:btnYear];
   
    UIAlertAction *btnGrade = [UIAlertAction actionWithTitle:@"Grade" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            buttonIndex =2;
            selectedAlert = ALERT_SORT;
            [self callAction];
    } ];
    [alert addAction:btnGrade];
    
    UIAlertAction *btnCollection = [UIAlertAction actionWithTitle:@"In Collection" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            buttonIndex =3;
            selectedAlert = ALERT_SORT;
            [self callAction];
    } ];
    [alert addAction:btnCollection];
    UIAlertAction *btnCountry = [UIAlertAction actionWithTitle:@"Country" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            buttonIndex =4;
            selectedAlert = ALERT_SORT;
            [self callAction];
    } ];
    [alert addAction:btnCountry];
   
    
	[alert release];
}
-(void)showAlertFilter
{
//    UIAlertView *alert = [[UIAlertView alloc] initWithFrame:CGRectMake(20, 320, 280, 160)];
//
//	//[alert setTitle:winnerName];
//	CGAffineTransform myTransform = CGAffineTransformMakeTranslation(0, 0);
//
//	[alert setTransform:myTransform];
//
//	[alert setMessage: @"Filter Items by"];
//	[alert setDelegate:self];
//	[alert addButtonWithTitle:@"Year"];
//	[alert addButtonWithTitle:@"Grade"];
//	[alert addButtonWithTitle:@"In Collection"];
//	[alert addButtonWithTitle:@"Country"];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Filter Items by" message: @"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *btnYear = [UIAlertAction actionWithTitle:@"Year" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            buttonIndex =0;
            selectedAlert = ALERT_FILTER;
            [self callAction];
    } ];
    [alert addAction:btnYear];
   
    UIAlertAction *btnGrade = [UIAlertAction actionWithTitle:@"Grade" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            buttonIndex =1;
            selectedAlert = ALERT_FILTER;
            [self callAction];
    } ];
    [alert addAction:btnGrade];
    
    UIAlertAction *btnCollection = [UIAlertAction actionWithTitle:@"In Collection" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            buttonIndex =2;
            selectedAlert = ALERT_FILTER;
            [self callAction];
    } ];
    [alert addAction:btnCollection];
    UIAlertAction *btnCountry = [UIAlertAction actionWithTitle:@"Country" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            buttonIndex =3;
            selectedAlert = ALERT_FILTER;
            [self callAction];
    } ];
    [alert addAction:btnCountry];
   
	[alert release];
}
#pragma mark Alerts

- (void)callAction
//alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == 0)
	{
		if (selectedAlert == ALERT_FILTER)
		{
           
		}
        
        else if(selectedAlert == ALERT_SORT)
		{
            if([sortString isEqualToString:@" name ASC "])
            {
                sortString = @" name Desc ";
            }
            else
            {
                sortString = @" name ASC ";
            }
        }
	}/////////////////////////////////////////////second button/////////////////////////////////////////////
	else if (buttonIndex == 1)
	{
		if (selectedAlert == ALERT_FILTER)
		{
			
		}
		else if (selectedAlert == ALERT_SORT)
		{
            if([sortString isEqualToString:@" year ASC "])
            {
                sortString = @" year Desc ";
            }
            else
            {
                sortString = @" year ASC ";
            }
            
		}
		
	}
	//////////////////////////////////////////////THIRD BUTTON///////////////////////////////////
	else if (buttonIndex == 2)
	{
        if (selectedAlert == ALERT_FILTER)
		{
           
		}
        else if (selectedAlert == ALERT_SORT)
        {
            if([sortString isEqualToString:@" grade ASC "])
            {
                sortString = @" grade Desc ";
            }
            else
            {
                sortString = @" grade ASC ";
            }

            sortString = @" grade ";
        }
		
	}
    //////////////////////////////////////////////THIRD BUTTON///////////////////////////////////
	else if (buttonIndex == 3)
	{
        if (selectedAlert == ALERT_FILTER)
		{
            
		}
        else if (selectedAlert == ALERT_SORT)
        {
            if([sortString isEqualToString:@" status ASC "])
            {
                sortString = @" status Desc ";
            }
            else
            {
                sortString = @" status ASC ";
            }

            
        }
		
	}
    //////////////////////////////////////////////THIRD BUTTON///////////////////////////////////
	else if (buttonIndex == 3)
	{
        if (selectedAlert == ALERT_FILTER)
		{
            
		}
        else if (selectedAlert == ALERT_SORT)
        {
            if([sortString isEqualToString:@" country ASC "])
            {
                sortString = @" country Desc ";
            }
            else
            {
                sortString = @" country ASC ";
            }

            
        }
		
	}
     [self populateList];
    [tblAllItems reloadData];
}



@end
