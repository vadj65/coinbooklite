//
//  Settings.h
//  CoinBook
//
//  Created by Vadim Re on 2012-11-27.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Reachability;
@class AppDelegate;

@interface Settings : UIViewController
{
       IBOutlet UINavigationItem *topNav;
    IBOutlet UIButton *btnAppSettings;
    IBOutlet UIButton *btnMetals;
    
    IBOutlet UIButton *btnSummary;
    
    
    Reachability* internetReachable;
    Reachability* hostReachable;
    
}
- (IBAction)onClickBack:(id)sender;
- (IBAction)onClickSecond:(id)sender;
- (IBAction)onClickAppSettings:(id)sender;
- (IBAction)onClickMetals:(id)sender;
@end
