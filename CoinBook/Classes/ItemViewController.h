//
//  ItemViewController.h
//  CoinBook
//
//  Created by Vadim Re on 2012-11-08.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import "ViewController.h"
#import "CollectionItemsViewController.h"
#import "DatePickerView.h"
#import "Item.h"

#import "BaseView.h"
#import <MessageUI/MessageUI.h>

@interface ItemViewController : BaseView
<MFMailComposeViewControllerDelegate>
{
    //rvv AppDelegate *mainDelegate;
    UITapGestureRecognizer *tapGesture;
    Item *localItem;
    
    IBOutlet UIView *msgView;
    IBOutlet UITextField *txtMaterial;
     IBOutlet UIView *viewProfileHolder;
   
    IBOutlet UIToolbar *uiToolBar;
    IBOutlet UITextField *txtYear;
    IBOutlet UILabel *lblMessage;
    IBOutlet UISegmentedControl *selStatus;
    IBOutlet UITextField *txtFaceValue;
   
    IBOutlet UISegmentedControl *selGrade;
    IBOutlet UITextField *txtDateOut;
   
    IBOutlet UINavigationItem *topToolBar;
     CGPoint gestureStartPoint;
    IBOutlet UISegmentedControl *swPreciousMetal;
    IBOutlet UITextField *txtWeight;
    
    IBOutlet UITextField *txtPurchaseDate;
    IBOutlet UITextField *txtPrice;
    IBOutlet UILabel *lblItemCurrentPrice;
    
    IBOutlet UISegmentedControl *selWeightUnit;
     IBOutlet UINavigationItem *topNav;
    IBOutlet UIView *scrView;
    IBOutlet UIDatePicker *dobPicker;
    IBOutlet UIDatePicker *dobDOPicker;
  
    
}

@property (retain, nonatomic) IBOutlet UITextField *txtName;
@property (retain, nonatomic) IBOutlet UITextField *txtDescription;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *btnAdd;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *btnBack;

@property(nonatomic, retain) DatePickerView *birthDatePickerView;

- (IBAction)onClickBack:(id)sender;
- (IBAction)onClickTakePhoto:(id)sender;

- (IBAction)onClickAdd:(id)sender;
- (IBAction)onClickSave:(id)sender;

- (IBAction)onClickEmail:(id)sender;

- (IBAction)onClickDelete:(id)sender;
- (IBAction)onClickCopy:(id)sender;
- (IBAction)onClickClear:(id)sender;
- (IBAction)onClickPurchaseDate:(id)sender;
- (IBAction)onClickDateOut:(id)sender;
- (IBAction)onClickWeightUnitChanged:(id)sender;

-( IBAction)hideKeyboard:(id)sender;
-(IBAction)showEdit:(id)sender;

@end
