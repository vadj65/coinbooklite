//
//  DropDownList.h
//  CoinBook
//
//  Created by Vadim Re on 2013-01-18.
//  Copyright (c) 2013 Vadim Re. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AppDelegate;

@interface DropDownList : UIViewController
<UITextFieldDelegate>

{
    IBOutlet UITableView *tableDropDown;
    NSMutableArray *dropDown;
    IBOutlet UINavigationItem *topNav;
    AppDelegate *mainDelegate ;
    NSIndexPath *setupIndex;
    int selectedRowIndex;
    int selectedSectionIndex;
    BOOL callScroll;
    NSArray *indices;
    UITableViewCell *cell;
}
@property(nonatomic, retain) NSMutableArray *states;
- (IBAction)onClickBack:(id)sender;
- (IBAction)onClickSelect:(id)sender;

@end
