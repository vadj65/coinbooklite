//
//  MailViewController.m
//  CoinBook
//
//  Created by Vadim Re on 2012-11-14.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import "MailViewController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "AddBookViewController.h"
#import "Utility.h"
#import "CollectionItemsViewController.h"
#import "Help.h"
#import "AppDelegate.h"

#define EMAIL_RETURN_COLLECTION 1
#define EMAIL_RETURN_ITEMS 2

@interface MailViewController ()

@end

@implementation MailViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
        // Custom initialization
        self.modalPresentationStyle = UIModalPresentationFullScreen;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    Utility *ut =[[Utility alloc]init];
   
    [ut menuButton:btnSendEmail];
    [ut release];

    
    if ([mainDelegate emailReturn] == RETURN_TO_ALL_COLLECTION)
    {
        lblCollectionName.text = @"AllCollections";
        txtFileName.text =@"AllCollections";

    }
    else if ([mainDelegate emailReturn] == RETURN_TO_HELP)
    {
        swPrice.enabled = false;
        swPrice.alpha=0.0;
        lblIncludePrice.alpha=0.0;
        lblCollectionName.text = @"CSV template";
        txtFileName.text =@"CSVTemplate";

    }
    else
    {
        lblCollectionName.text =  [mainDelegate.Collection Name];
        txtFileName.text =[ [mainDelegate.Collection Name] stringByReplacingOccurrencesOfString:@"" withString:@"_"];

    }
    txtEmail.text = [Utility getPreference:@"coinEmail"];
    topNav.title = @"Send Email";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onClickBtnSendEmail:(id)sender
{
   [self hKeyboard];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    if ([Utility validateEmail:txtEmail.text]==1 && txtEmail.text.length > 0  && txtFileName.text.length > 0 && [MFMailComposeViewController canSendMail])
    {
        /*UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        spinner.center =CGPointMake(160, 240);
        spinner.tag= 128;
        [self.view addSubview:spinner];
        
        [spinner startAnimating];
        [spinner release];

        */
        
        Item *item = [[Item alloc] init];

        if ([mainDelegate emailReturn] == RETURN_TO_ALL_COLLECTION)
        {
            [item createCVSAllItems:txtFileName.text PriceIncluded:swPrice.on];
        }
        else if ([mainDelegate emailReturn] == RETURN_TO_HELP)
        {
            [item createCVSTemplate:txtFileName.text ];
        }
        else
        {
            [item createCVS:txtFileName.text  PriceIncluded:swPrice.on];
        }
       // [[self.view viewWithTag:128] stopAnimating];
        
        //create NSString object, that holds our exact path to the documents directory
        NSString *documentsDirectory = [paths objectAtIndex:0];
       
        
        NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.csv", txtFileName.text]]; //add
        NSData *myData = [NSData dataWithContentsOfFile:fullPath];
        
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        
        mailer.mailComposeDelegate = self;
        [mailer setBccRecipients:[NSArray  arrayWithObjects: @"", nil]];
        [mailer setToRecipients:[NSArray arrayWithObjects: txtEmail.text, nil]];
        [mailer setSubject:@"Collection from Coin Collection App"];
        
        NSString *emailBody = @"";
        [mailer setMessageBody:emailBody isHTML:NO];
        
        [mailer addAttachmentData:myData mimeType:@"csv" fileName:[NSString stringWithFormat:@"%@.csv", txtFileName.text]];
        
        [self presentViewController:mailer animated:NO completion:nil];
        //[item release];
        
        [mailer release];
        [item release];
        //topNav.title = @"Colllection is sent";
    }
    else
    {
        NSString *messageEMAIL =@"";
        if (txtEmail.text.length == 0) {
            messageEMAIL = [messageEMAIL stringByAppendingString:@" EMail is empty."];
        }
        if (txtFileName.text.length == 0) {
            messageEMAIL = [messageEMAIL stringByAppendingString:@" Fale name is empty."];
        }
        if ([Utility validateEmail:txtEmail.text]==0) {
             messageEMAIL = [messageEMAIL stringByAppendingString:@" Email is invalid."];
        }
        
        
     
        
        //[alert setMessage: [NSString stringWithFormat:@"Delete collection %@", [mainDelegate.Collection Name]]];
      
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message: messageEMAIL preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:cancel];
        [alert addAction:ok];
        

              
      
    }

}

- (IBAction)onClickBack:(id)sender
{
    if([mainDelegate emailReturn] == EMAIL_RETURN_COLLECTION)
    {
        [mainDelegate setEmailReturn:0];
        AddBookViewController *vc = [[AddBookViewController alloc] init];
        [self presentViewController: vc animated:YES completion:nil];
        [vc release];
    }
    else if([mainDelegate emailReturn] == EMAIL_RETURN_ITEMS)
    {
        [mainDelegate setEmailReturn:0];
        CollectionItemsViewController *vc = [[CollectionItemsViewController alloc] init];
        [self presentViewController: vc animated:YES completion:nil];
        [vc release];
    }
    else if([mainDelegate emailReturn] == RETURN_TO_ALL_COLLECTION)
    {
        [mainDelegate setEmailReturn:0];
        Help *vc = [[Help alloc] init];
        [self presentViewController: vc animated:YES completion:nil];
        [vc release];
    }
    else if([mainDelegate emailReturn] == RETURN_TO_HELP)
    {
        [mainDelegate setEmailReturn:0];
        Help *vc = [[Help alloc] init];
        [self presentViewController: vc animated:YES completion:nil];
        [vc release];
    }

//
}

- (IBAction)hideKeyboard:(id)sender
{
    [self hKeyboard];
}

-(void)hKeyboard
{
    [txtEmail resignFirstResponder];
    [txtFileName resignFirstResponder];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            lblMessage.text= @"Mail cancelled: you cancelled the operation and no email message was queued.";
            break;
        case MFMailComposeResultSaved:
            lblMessage.text= (@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            lblMessage.text= (@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            lblMessage.text= (@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            lblMessage.text= (@"Mail not sent.");
            break;
    }
    [self dismissViewControllerAnimated:NO completion:nil];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
   
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.csv", txtFileName.text]]; //add
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:fullPath])
    {
        NSError *error;
        if (![fileManager removeItemAtPath:fullPath error:&error])
        {
            //NSLog(@"Error removing file: %@", error);
        };
    }
}
- (void)dealloc {
    [lblMessage release];
    lblMessage = nil;
    [lblCollectionName release];
    lblCollectionName = nil;
    [txtEmail release];
    txtEmail = nil;
    [lblMessage release];
    lblMessage = nil;
    [txtFileName release];
    txtFileName = nil;
    [btnSendEmail release];
    btnSendEmail = nil;
    [swPrice release];
    swPrice = nil;
    [lblIncludePrice release];
    lblIncludePrice = nil;
    [super dealloc];
}

@end
