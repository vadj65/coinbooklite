//
//  HelpView.h
//  CoinBook
//
//  Created by Vadim Re on 2013-04-09.
//  Copyright (c) 2013 Vadim Re. All rights reserved.
//

#import "ViewController.h"
#import <WebKit/WebKit.h>

@interface HelpView : ViewController
{
    IBOutlet WKWebView *helpWebView;
}
- (IBAction)onClickBack:(id)sender;
@end
