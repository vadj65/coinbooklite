//
//  Help.m
//  CoinBook
//
//  Created by Vadim Re on 2013-01-28.
//  Copyright (c) 2013 Vadim Re. All rights reserved.
//

#import "Help.h"
#import "ViewController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "Utility.h"
#import "HelpView.h"
#import "MailViewController.h"
#import "AppDelegate.h"

@interface Help ()

@end

@implementation Help


UITableViewCell *cell;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
  
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)onClickMainMenu:(id)sender
{
    ViewController *vc = [[ViewController alloc] init];
    [self presentViewController: vc animated:YES completion:nil];
    [vc release];
 
}
-(IBAction)onClickBack:(id)sender
{
    ViewController *vc = [[ViewController alloc] init];
    [self presentViewController: vc animated:YES completion:nil];
    [vc release];

}

- (IBAction)onClickLink:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.nincha.ca"] options:@{} completionHandler:nil];
}

- (IBAction)onClickSendEmail:(id)sender
{
    MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
    
    mailer.mailComposeDelegate = self;
    [mailer setBccRecipients:[NSArray  arrayWithObjects: @"", nil]];
    [mailer setToRecipients:[NSArray arrayWithObjects: @"info@nincha.ca", nil]];
    [mailer setSubject:@"From Coin Collection App"];
    
    NSString *emailBody = @"";
    [mailer setMessageBody:emailBody isHTML:NO];
    
     
    [self presentViewController:mailer animated:YES completion: nil];
    //[item release];
    
    [mailer release];
  
    
}

- (IBAction)onClickHelp:(id)sender
{
    HelpView *vc = [[HelpView alloc] init];
    [self presentViewController: vc animated:YES completion:nil];
    [vc release];

}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            lblMessage.text= @"Mail cancelled: you cancelled the operation and no email message was queued.";
            break;
        case MFMailComposeResultSaved:
            lblMessage.text= (@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            lblMessage.text= (@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            lblMessage.text= (@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            lblMessage.text= (@"Mail not sent.");
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark Table methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
        return 1;
   
    
    
}


- (id)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
      
        
        if (cell == nil)
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        // cell = [self itemCell];
        // [self setItemCell:nil];
    }
    if (indexPath.section == 0)
    {
        [cell.textLabel setText:@"www.nincha.ca/help"];
    }
    else if (indexPath.section == 1)
    {
          [cell.textLabel setText:@"send email to info@nincha.ca"];
    }
    else  if (indexPath.section == 2)
    {
          [cell.textLabel setText:@"Help"];
    }
    else  if (indexPath.section == 3)
    {
        [cell.textLabel setText:@"Create CSV backup"];
    }
    else  if (indexPath.section == 4)
    {
        [cell.textLabel setText:@"Create CSV template."];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
     return cell;
    
    
    //[cell setItemIndex:indexPath.row];
    
    
    
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 5;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0)
    {
         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.nincha.ca"] options:@{} completionHandler:nil];
    }
    else if(indexPath.section==1)
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        
        mailer.mailComposeDelegate = self;
        [mailer setBccRecipients:[NSArray  arrayWithObjects: @"", nil]];
        [mailer setToRecipients:[NSArray arrayWithObjects: @"info@nincha.ca", nil]];
        [mailer setSubject:@"From Coin Collection App"];
        
        NSString *emailBody = @"";
        [mailer setMessageBody:emailBody isHTML:NO];
        
        
        [self presentViewController:mailer animated:YES completion:nil];
        //[item release];
        
        [mailer release];

    }
    else if(indexPath.section==2)
    {
        HelpView *vc = [[HelpView alloc] init];
        [self presentViewController: vc animated:YES completion:nil];
        [vc release];
        
    }
    else if(indexPath.section==3)
    {
        [mainDelegate setEmailReturn:RETURN_TO_ALL_COLLECTION];
        MailViewController *vc = [[MailViewController alloc] init];
        [self presentViewController: vc animated:YES completion:nil];
        [vc release];
        
    }
    else if(indexPath.section==4)
    {
        [mainDelegate setEmailReturn:RETURN_TO_HELP];
        MailViewController *vc = [[MailViewController alloc] init];
        [self presentViewController: vc animated:YES completion:nil];
        [vc release];
        
    }
    // Navigation logic may go here. Create and push another view controller.
    
    
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section==0)
    {
        return @"Help on website";
    }
    else if(section==1)
    {
        return @"Send email";
    }
    else if(section==2)
    {
        return @"Help";
    }
    else if(section==3)
    {
        return @"Create Excel Backup";
    }
    else if(section==4)
    {
        return @"Create CSV template.";
    }
    else
    {
        return @"";
    }
}



- (void)dealloc {
    
   
    [lblMessage release];
    [btnEmail release];
    [tblHelp release];
    [lblMessage release];
    lblMessage = nil;
    [btnEmail release];
    btnEmail = nil;
    [tblHelp release];
    tblHelp = nil;
    [super dealloc];
}
//- (void)viewDidUnload {
//
//
//
//    [super viewDidUnload];
//}
@end
