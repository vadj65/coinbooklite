//
//  ViewController.m
//  CoinBook
//
//  Created by Vadim Re on 2012-10-22.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import "ViewController.h"
//#import  "ScrollViewController.h"
#import "MailViewController.h"
#import "Reachability.h"
#import "AppDelegate.h"
#import "Settings.h"
#import "AllItems.h"
#import "ImportCSVViewController.h"
#import "Help.h"
#import "Utility.h"
#import <QuartzCore/QuartzCore.h>

@interface ViewController ()

@end

@implementation ViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
       // mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
       
        self.modalPresentationStyle = UIModalPresentationFullScreen;
        
    }
    return self;
}

-(void)viewDidLoad
{
    Utility *ut =[[Utility alloc]init];
      // [ut executeCommand];
       [ut menuButton:btnHelp];
       [ut menuButton:btnCoinBook];
       [ut menuButton:btnSettings];
       [ut release];
}
- (void)viewDidAppear
{
    
         
    [super viewDidLoad];
    //[self.view autoresizingMask];
    
    AppDelegate  *mainDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
   
	    navTop.title = @"Collections Book";
    
        [self.navigationController setNavigationBarHidden:YES];
    
    
        if([mainDelegate AppOptions] == 1)
        {
            ImportCSVViewController *coinBook = [[ImportCSVViewController alloc] init];
            [self presentViewController:coinBook animated:YES completion: nil ];
            [coinBook release];

        }
   //ViewController *vc = [[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
    //vc.modalPresentationStyle=UIModalPresentationFullScreen;
    
    self.modalPresentationStyle=UIModalPresentationFullScreen;
    self.navigationController.modalPresentationStyle=UIModalPresentationFullScreen;
    
    Utility *ut =[[Utility alloc]init];
   // [ut executeCommand];
    [ut menuButton:btnHelp];
    [ut menuButton:btnCoinBook];
    [ut menuButton:btnSettings];
    [ut release];
   
    
     
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (IBAction)onClickCoinBook:(id)sender {
    CollectionListViewController *coiBook = [[CollectionListViewController alloc] init];
   
    [self presentViewController:coiBook animated:YES completion: nil ];
    [coiBook release];
}

- (IBAction)onClickAllCoins:(id)sender
{
    AllItems *coiBook = [[AllItems alloc] init];
    [self presentViewController:coiBook animated:YES completion: nil ];
    [coiBook release];

}

- (IBAction)onClickHelp:(id)sender
{
    Help *coiBook = [[Help alloc] init];
    [self presentViewController:coiBook animated:YES completion: nil ];
    [coiBook release];
}

- (IBAction)onClickSettings:(id)sender
{
    Settings *coiBook = [[Settings alloc] init];
    [self presentViewController:coiBook animated:YES completion: nil ];
    [coiBook release];

}

- (IBAction)onClickImports:(id)sender
{
    ImportCSVViewController *coiBook = [[ImportCSVViewController alloc] init];
    [self presentViewController:coiBook animated:YES completion: nil ];
    [coiBook release];
}

- (void)dealloc {
    
    [navTop release];
    navTop = nil;
    [btnSettings release];
    btnSettings = nil;
    [btnHelp release];
    btnHelp = nil;
    [txtKeyboard release];
    txtKeyboard = nil;
    [super dealloc];
}




#pragma mark Import CSV
-(void) handleOpenURL:(NSURL *)url {
    //NSError *outError;
    //NSString *fileString = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&outError];
    //NSLog(@"%@",fileString);
}






@end
