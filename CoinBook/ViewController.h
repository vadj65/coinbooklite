//
//  ViewController.h
//  CoinBook
//
//  Created by Vadim Re on 2012-10-22.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sqlite3.h"
#import "CollectionListViewController.h"

@class Reachability;
@class AppDelegate;

@interface ViewController : UIViewController
{
    IBOutlet UIButton *btnCoinBook;
    IBOutlet UIButton *btnAllCoins;
   UIButton *btnAddBook;
    IBOutlet UIButton *btnSettings;
    UILabel *status;
    IBOutlet UIButton *btnHelp;
    
    IBOutlet UINavigationItem *navTop;
    AppDelegate *mainDelegate ;
    IBOutlet UITextField *txtKeyboard;
    sqlite3 *contactDB;
    NSString        *databasePath;
   
}



- (IBAction)onClickCoinBook:(id)sender;
- (IBAction)onClickAllCoins:(id)sender;
- (IBAction)onClickHelp:(id)sender;
- (IBAction)onClickSettings:(id)sender;
- (IBAction)onClickImports:(id)sender;
-(void) handleOpenURL:(NSURL *)url;

@end
