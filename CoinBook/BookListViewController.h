//
//  BookListViewController.h
//  CoinBook
//
//  Created by Vadim Re on 2012-10-31.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DS.h"

@interface BookListViewController : UITableViewController
{
    NSMutableArray *_bookList;
}

@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnBack;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnAddBook;

- (IBAction)onClickAddBook:(id)sender;
- (IBAction)onClickBack:(id)sender;





@end
