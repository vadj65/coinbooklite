//
//  AppDelegate.h
//  CoinBook
//
//  Created by Vadim Re on 2012-10-22.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DS.h"
#import "Collection.h"
#import "AppSettings.h"
#import "Market.h"
#import "User.h"
@class Item;


@class CheckPassword;
@class ImportCSVViewController;
@class Password;
@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    DS *_dataBase;
    Collection *_collection;
    Item *newItem;
    Item *_item;
    
  

}
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) Collection *Collection;
@property (strong, nonatomic) Collection *CollectionTemp;
@property (strong, nonatomic) User *CoinUser;

@property (strong, nonatomic) AppSettings *AppSettings;
@property (strong, nonatomic) Market *MyMarket;
@property (strong, retain) Item *Item;
@property (strong, retain) Item *ItemTemp ;
@property (strong, nonatomic) CheckPassword *checkPassword;
@property (strong, nonatomic) ImportCSVViewController *importCSV;
@property (strong, nonatomic) UINavigationController *navigationController;
@property (nonatomic , retain) DS *DataBase;
@property (nonatomic , retain) NSMutableArray *ItemList;
@property (nonatomic , assign) int itemIndex;
@property (nonatomic , assign) int itemId;
@property (nonatomic , assign) int AppPath;
@property (nonatomic , assign) int AppOptions;
@property (nonatomic , retain) NSString *CSVText;
@property (nonatomic , retain) NSArray *csvArrayApp;
@property (nonatomic , assign) int selectedRowIndexCollection;
@property (nonatomic , assign) int selectedRowIndexItem;
@property (nonatomic , assign) int DropdownId;
@property (nonatomic , retain) NSString *DropDownKey;
@property (nonatomic , retain) NSString *DropDownValue;
@property (nonatomic , assign) int emailReturn;
@property (nonatomic , retain) NSString *sortField;
@property (nonatomic , retain) NSString *whereField;
@property (nonatomic , retain) NSString *fileName;
@property (nonatomic , assign) Boolean isPassword;


extern const int DROPDOWN_SETTINGS_COUNTRY;
extern const int DROPDOWN_SETTINGS_CURRENCY;
extern const int RETURN_TO_ALL_COLLECTION;
extern const int RETURN_TO_ONE_COLLECTION;
extern const int RETURN_TO_HELP;
extern const NSString *DateFormat;
extern const NSString *DateFormatOld;
extern const int TAG_NUMBERKEYBOARD;
extern const int COINS_MAX;
extern const int BOOKS_MAX;
extern const int TAG_Settings;
extern const NSString *Message_Max_Coins;
@end
