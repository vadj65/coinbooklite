//
//  Lookup.h
//  CoinBook
//
//  Created by Vadim Re on 2013-01-21.
//  Copyright (c) 2013 Vadim Re. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"

@interface Lookup : NSObject
{
    sqlite3_stmt    *_statement;
    sqlite3 *_contactDB;
}
@property (nonatomic, retain) NSString *key;
@property (nonatomic, retain) NSString *value;
@property (nonatomic, retain) NSString *ind;


//-(NSMutableArray*)populateLookup:(NSString*)lookupName;
-(void)populateLookup:(NSString*)lookupName Content:(NSMutableArray*)content;
@end
