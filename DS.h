//  Vadim Reznikov 2022 02 24
//  DS.h
//  CoinBook
//
//  Created by Vadim Re on 2012-11-03.
//  Copyright (c) 2012 Vadim Re. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"


@interface DS : NSObject
{
   NSString *_status;
   
    NSString  *_databasePath;
    NSString *_docsDir;
    NSArray *_dirPaths;
    sqlite3_stmt    *_statement;
     sqlite3 *_contactDB;
}


@property (nonatomic, copy) NSString *sqlStatus;
//@property(nonatomic, retain)  sqlite3 *ContactDB;
@property(nonatomic, retain)  NSString *DatabasePath;
@property(nonatomic, retain)  NSString *DocsDir;
@property(nonatomic, retain)  NSArray *DirPath;
//@property(copy, nonatomic)  sqlite3 *ContactDB;


-(id)init;

-(void)commandSQL:(NSString*)SQLStatement;
-(NSString*)getLastSeq:(NSString *)tableName;
-(void)createTables;
-(NSString*)getOneValue:(NSString*)sqlStatement;
-(void)checkDB;
-(void)updateTables;
//-(NSString*)getSqlStatus;

@end
